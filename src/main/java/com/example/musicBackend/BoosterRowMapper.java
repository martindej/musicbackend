package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.musicBackend.model.Booster;

public class BoosterRowMapper implements RowMapper<Booster> {
	
	public Booster mapRow(ResultSet rs, int arg1) throws SQLException {
		Booster boo = new Booster();
		boo.setId(rs.getLong("id"));
		boo.setName(rs.getString("name"));
		boo.setNumberMusics(rs.getInt("number_musics"));
		boo.setPrice(rs.getDouble("price"));
		boo.setLogo(rs.getBytes("logo"));
		boo.setGeneralSettings(rs.getBoolean("general_settings"));
		boo.setAdvancedSettings(rs.getBoolean("advanced_settings"));
		boo.setMelodySettings(rs.getBoolean("melody_settings"));

  	  	return boo;
	}
}