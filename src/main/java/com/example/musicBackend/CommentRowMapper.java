package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;

import com.example.musicBackend.model.Comment;

public class CommentRowMapper implements RowMapper<Comment> {
	
	private LobHandler lobHandler = new DefaultLobHandler();
	
	public Comment mapRow(ResultSet rs, int arg1) throws SQLException {
		Comment com = new Comment();
		com.setId(rs.getLong("id"));
		com.setUser_id(rs.getLong("user_id"));
		com.setUser_name(rs.getString("user_name"));
		com.setMusic_id(rs.getLong("music_id"));
  	  	byte[] requestData = lobHandler.getBlobAsBytes(rs,"message");
  	  	if(requestData != null && requestData.length > 0) {
  	  		com.setMessage(new String(requestData));
  	  	}
  	  	else {
  	  		com.setMessage("");
  	  	}
  	  	com.setDate(rs.getLong("date"));
  	  	return com;
	}

}
