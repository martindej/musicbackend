package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.musicBackend.model.MesureTempsFort;

public class MesureTempsFortRowMapper  implements RowMapper<MesureTempsFort> {
	
	public MesureTempsFort mapRow(ResultSet rs, int arg1) throws SQLException {
		
		MesureTempsFort p = new MesureTempsFort();
		
  	  	p.setId(rs.getLong("id"));
  	  	p.setStyleId(rs.getLong("style_id"));
  	  	p.setMesure(rs.getInt("mesure"));
  	  	p.setEtOu(rs.getBoolean("et_ou"));
  	  	
  	  	return p;
    }

}
