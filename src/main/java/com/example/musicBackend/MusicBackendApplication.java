package com.example.musicBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.example")
public class MusicBackendApplication  extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(MusicBackendApplication.class);
	}

//	@Autowired
//    private EmailService emailService;

	public static void main(String[] args) throws Exception {
		// System.setProperty("server.servlet.context-path", "/api");
		SpringApplication.run(MusicBackendApplication.class, args);
	}
	
	
//	@Override
//    public void run(ApplicationArguments applicationArguments) throws Exception {
//
//        Mail mail = new Mail();
//        mail.setFrom("murmure.codemure@gmail.com");
//        mail.setTo("martin.dejax@gmail.com");
//        mail.setSubject("Sending Email with Thymeleaf HTML Template Example");
//        
//        Map<String, String> model = new HashMap<String, String>();
//        model.put("name", "Memorynotfound.com");
//        model.put("location", "Belgium");
//        model.put("signature", "https://memorynotfound.com");
//        mail.setModel(model);
//
//
//        emailService.sendSimpleMessage(mail);
//    }
}
