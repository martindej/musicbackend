package com.example.musicBackend;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.Callable;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.example.musicBackend.model.MusicFile;
import com.example.musicBackend.service.ServiceMusic;

import enums.EnumDurationChord;
import enums.EnumInstruments;
import enums.EnumTonalite;
import it.sauronsoftware.jave.EncoderException;
import it.sauronsoftware.jave.InputFormatException;
import midi.MidiToWav;
import midi.MidiWhitoutJavax;
import midi.WavToMp3;
import musicRandom.Music;
import musicXml.WriteMusicXml;
import styles.Style;

public class MusicFileCallable  implements Callable<MusicFile> {

	private String name;
	private long user_id;
	private String user_name;
	private Style style;
	private long creationDate;
	private int idImage;
	private String tonality;
	private List<EnumInstruments> instruments;
	private boolean percussions;
	private int swing;
	private String soundFont;
	
	private ServiceMusic serviceMusic;
	
	public MusicFileCallable(Style st, long user_id, String user_name, long creationDate, String title, int idImage, String tonality, List<EnumInstruments> instruments, boolean percussions, String soundFont, ServiceMusic serviceMusic2, int swing){
		this.style = st;
		this.user_id = user_id;
		this.user_name = user_name;
		this.creationDate = creationDate;
		this.name = title;
		this.serviceMusic = serviceMusic2;
		this.idImage = idImage;
		this.tonality = tonality;
		this.instruments = instruments;
		this.percussions = percussions;
		this.soundFont = soundFont;
		this.swing = swing;
	}
	
	public MusicFile call() throws Exception {
		
		EnumTonalite tonality = EnumTonalite.getByEngName(this.tonality);
		EnumDurationChord dChord = null;
		
		int nbre_bat;
		
		if(this.percussions) {
			nbre_bat = 1;
		}
		else {
			nbre_bat = 0;
		}

		Music music = new Music(tonality, -1, dChord, null, this.instruments, nbre_bat, null, style, swing);

		MidiWhitoutJavax midi = new MidiWhitoutJavax(music);
		byte[] midiFile = midi.getMidiBytes();
		
		WriteMusicXml musicXml = new WriteMusicXml(music, this.name);
		String musicXmlFile = musicXml.getMusicXml();
		String musicXmlFileModified = "";
		
		String videoLink = "";
		String instruments = music.getListInstrumentsAsString();
		int tempo = music.getRythme();
		String styleMusic = style.getStyleName();
		long user_id_origin = user_id;
		int user_id_modified = 0;
		int user_id_video = 0;
		String user_name_origin = user_name;
		String description = "";
		boolean visible_origin = false;
		String tonalityMusic = music.getTonality().getEngName();
		int mother_id = 0;
		
		Resource resource = new ClassPathResource("/soundfonts/" + this.soundFont);
		
		MusicFile musicFile = null;
		
	    try {
	        File dbAsStream = resource.getFile();
	        MidiToWav wav = new MidiToWav(midiFile, dbAsStream);
	        File mp3 = WavToMp3.encodeToMp3(wav.getWavFile());
	        wav.deleteWavFile();
	        
	        Path path = mp3.toPath();
	        byte[] mp3File = Files.readAllBytes(path);
	        mp3.delete();
	        
	        musicFile = new MusicFile(midiFile, musicXmlFile, musicXmlFileModified, videoLink, instruments, tempo, styleMusic, user_id, user_name, user_id_origin, user_id_modified, user_id_video, user_name_origin, name, description, creationDate, visible_origin, tonalityMusic, mother_id, mp3File, this.idImage, this.soundFont); 
			
	    } catch (IOException e) {
	        e.printStackTrace();
	    } catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InputFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EncoderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
//		MusicFile music = new MusicFile(this.style,this.user_id, this.user_name, this.creationDate, this.name);  
		if(music != null && musicFile.getMusicXmlFile() != null) {
			serviceMusic.addNew(musicFile);
			musicFile = serviceMusic.getByUserIdAndDate(this.user_id, this.creationDate);
		}
		
		return musicFile;
    }

	public int getIdImage() {
		return idImage;
	}

	public void setIdImage(int idImage) {
		this.idImage = idImage;
	}

	public String getTonality() {
		return tonality;
	}

	public void setTonality(String tonality) {
		this.tonality = tonality;
	}

	public boolean isPercussions() {
		return percussions;
	}

	public void setPercussions(boolean percussions) {
		this.percussions = percussions;
	}
}