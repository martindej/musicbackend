package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.musicBackend.model.MusicFileLight;

public class MusicFileLightRowMapper implements RowMapper<MusicFileLight> {
	
	public MusicFileLight mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		MusicFileLight music = new MusicFileLight();
        music.setId(rs.getLong("id"));
        music.setUser_id(rs.getLong("user_id"));
        music.setUser_name(rs.getString("user_name"));
        music.setUser_id_origin(rs.getLong("user_id_origin"));
        music.setUser_name_origin(rs.getString("user_name_origin"));
        music.setVisible_origin(rs.getBoolean("visible_origin"));
        music.setUser_id_modified(rs.getLong("user_id_modified"));
        music.setUser_name_modified(rs.getString("user_name_modified"));
        music.setVisible_modified(rs.getBoolean("visible_modified"));
        music.setUser_id_video(rs.getLong("user_id_video"));
        music.setUser_name_video(rs.getString("user_name_video"));
        music.setVisible_video(rs.getBoolean("visible_video"));
        music.setStyle(rs.getString("style"));
        music.setInstruments(rs.getString("instruments"));
        music.setVideoId(rs.getString("video_id"));
        music.setName(rs.getString("name"));
        music.setTempo(rs.getInt("tempo"));
        music.setCreationDate(rs.getLong("creation_date"));
        music.setTonality(rs.getString("tonality"));
        music.setMother_id(rs.getLong("mother_id"));
        music.setDescription(rs.getString("description"));
        
        music.setPictureNumber(rs.getInt("picture_number"));

		return music;
	}

}
