package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.lob.LobHandler;

import com.example.musicBackend.model.MusicFile;

public class MusicRowMapper implements RowMapper<MusicFile> {
	
	@Autowired @Qualifier("myDefaultLobHandler")
	private LobHandler lobHandler;
	
	public MusicFile mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		MusicFile music = new MusicFile();
        music.setId(rs.getLong("id"));
        music.setUser_id(rs.getLong("user_id"));
        music.setUser_name(rs.getString("user_name"));
        music.setUser_id_origin(rs.getLong("user_id_origin"));
        music.setUser_name_origin(rs.getString("user_name_origin"));
        music.setVisible_origin(rs.getBoolean("visible_origin"));
        music.setUser_id_modified(rs.getLong("user_id_modified"));
        music.setUser_name_modified(rs.getString("user_name_modified"));
        music.setVisible_modified(rs.getBoolean("visible_modified"));
        music.setUser_id_video(rs.getLong("user_id_video"));
        music.setUser_name_video(rs.getString("user_name_video"));
        music.setVisible_video(rs.getBoolean("visible_video"));
//        music.setMidiFile(lobHandler.getBlobAsBytes(rs,"midiFile"));
        music.setMusicXmlFile(rs.getString("music_xml_file"));
        music.setMusicXmlFileModified(rs.getString("music_xml_file_modified"));
        music.setMidiFile(rs.getBytes("midi_file"));
        music.setStyle(rs.getString("style"));
        music.setInstruments(rs.getString("instruments"));
        music.setVideoId(rs.getString("video_id"));
        music.setName(rs.getString("name"));
        music.setTempo(rs.getInt("tempo"));
        music.setMp3File(rs.getBytes("mp3file"));
        music.setCreationDate(rs.getLong("creation_date"));
        music.setTonality(rs.getString("tonality"));
        music.setMother_id(rs.getLong("mother_id"));
        music.setDescription(rs.getString("description"));
        music.setSoundFont(rs.getString("sound_font"));
        
        music.setPictureNumber(rs.getInt("picture_number"));

		return music;
	}

}
