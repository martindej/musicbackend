package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.example.musicBackend.model.MusicSubscription;

public class MusicSubscriptionRowMapper implements RowMapper<MusicSubscription> {
	
	public MusicSubscription mapRow(ResultSet rs, int arg1) throws SQLException {
		MusicSubscription m = new MusicSubscription();
		m.setId(rs.getLong("id"));
		m.setDate(rs.getLong("date"));
		m.setUser_id(rs.getLong("user_id"));
		m.setOrigin(rs.getString("origin"));

  	  	return m;
	}
}
