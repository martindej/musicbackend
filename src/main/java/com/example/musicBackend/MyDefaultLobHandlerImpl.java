package com.example.musicBackend;

import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.stereotype.Service;

@Service("myDefaultLobHandler")
public class MyDefaultLobHandlerImpl extends DefaultLobHandler {

	public MyDefaultLobHandlerImpl() {
		super();
		this.setWrapAsLob(true);
		this.setStreamAsLob(true);
	}
}

