package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.musicBackend.model.ProfileImage;

public class ProfileImageRowMapper implements RowMapper<ProfileImage> {
	
	public ProfileImage mapRow(ResultSet rs, int arg1) throws SQLException {
		ProfileImage p = new ProfileImage();
		p.setId(rs.getLong("id"));
  	  	p.setUser_id(rs.getLong("user_id"));
  	  	p.setName(rs.getString("name"));
  	  	p.setAvatar(rs.getBytes("avatar"));
  	  	return p;
    }

}

