//package com.example.musicBackend;
//
//import java.io.*;
//import java.sql.*;
//import java.util.*;
//
//import javax.sql.DataSource;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.support.lob.LobHandler;
//import org.springframework.jdbc.support.lob.LobCreator;
//import org.springframework.stereotype.Service;
//import org.slf4j.*;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.example.musicBackend.model.MusicFile;
//
//@Service("service")
//@Transactional
//public class ServiceImpl {
//	final static Logger logger = LoggerFactory.getLogger(ServiceImpl.class);
//   
//	private JdbcTemplate jdbcTemplate;
//	
//	@Autowired
//	public void setDataSource(DataSource dataSource) {
//		jdbcTemplate = new JdbcTemplate(dataSource);
//	}
//
//	@Autowired @Qualifier("myDefaultLobHandler")
//	private LobHandler lobHandler;
//
//	public boolean musicExistById(long id) {
////		
////		String sql = "SELECT * FROM MUSICFILE WHERE ID = ?";
////
////		Customer customer = (Customer)getJdbcTemplate().queryForObject(
////				sql, new Object[] { custId }, new CustomerRowMapper());
//
////		return customer;
////		try {
////		    return jdbcTemplate.queryForObject(EXIST_MUSICFILE_BY_ID,Boolean.class,id);
////		} catch(DataAccessException e){
////		    return false;
////		}
//		return false;
//	}
//	
//	public void insertXlob(final MusicFile music) throws Exception {
//	        
//	  final File clobFileIn = new File("musicXml.xml");
//	  
//	  if(!clobFileIn.exists()){
//	    logger.error("Fichier '"+clobFileIn.getPath()+ "' introuvable !");
//	    return;
//	  }
//	  	  
////	  String stm = "INSERT INTO mesa(tag, modelo, menor_complemento, peso_min, "
////	            + "peso_max, som, rotina, address64bits) "
////	            + "VALUES(?,?,?,?,?,?,?,?)";
////	    try {
////	        pst = con.prepareStatement(stm);
////	        pst.setString(1, mesa.getTag());
////	        pst.setString(2, mesa.getModelo());
////	        pst.setInt(3, mesa.getMenorComplemento());
////	        pst.setInt(4, mesa.getPesoMin());
////	        pst.setInt(5, mesa.getPesoMax());
////	        pst.setByte(6, mesa.getSom());
////	        pst.setByte(7, mesa.getRotina());
////	        pst.setBytes(8, mesa.getAddress64Bits());
////	        pst.executeUpdate();
////	    }
//	  
//	  try (final InputStream clobFileIs = new FileInputStream(clobFileIn)) 
//	  {        
//	    int retour = jdbcTemplate.execute("INSERT INTO MUSIC_FILE (music_xml_file) VALUES (?)", (PreparedStatement ps)-> /* type necessaire car ambiguite*/
//	    { 
//	      try(LobCreator lobCreator = lobHandler.getLobCreator()) {
////	        ps.setLong(1,music.getId());
////	        ps.setBytes(1, music.getMusicXmlFile());
////	        lobCreator.setBlobAsBytes(ps, 2, music.getMusicXmlFile());
////	        lobCreator.setBlobAsBinaryStream(ps, 2, clobFileIs, clobFileIs.available());
//	        return ps.executeUpdate();
//	      } catch (Exception e) { /*catch force par le compilo*/
//	        logger.warn(e.getMessage());
//	        return 0;
//	      }
//	    });
//	    if(retour==1) logger.info("Ligne insérée dans");
//	  }    
//	}
//	
//	public void updateXlob(String fullPathFileClob, long idMail) throws Exception {
//		// TODO Auto-generated method stub
//		
//	}
//	
//	public List<MusicFile> getMusicMidiById(final long id) {
////		return jdbcTemplate.query(
////			MUSICFILE_CORPS_SQL_BY_ID, new Object[] { id },
////			        (rs,num) -> { 
////			          final MusicFile music = new MusicFile();
////			          music.setId(id);
////			          music.setMidiFile(lobHandler.getClobAsString(rs,"midiFile"));
////			          music.setMusicXmlFile(lobHandler.getClobAsString(rs,"musicXmlFile"));
////			          return email;
////			        });    
//		return null;
//	}
//
//	public List<MusicFile> getMusicXmlById(long id) {
//		// TODO Auto-generated method stub
//		return null;
//	}
//}
