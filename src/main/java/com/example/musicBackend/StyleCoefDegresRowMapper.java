package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.musicBackend.model.StyleCoefDegres;

public class StyleCoefDegresRowMapper implements RowMapper<StyleCoefDegres> {
	
	public StyleCoefDegres mapRow(ResultSet rs, int arg1) throws SQLException {
		
		StyleCoefDegres p = new StyleCoefDegres();
		
  	  	p.setId(rs.getLong("id"));
  	  	p.setStyleId(rs.getLong("style_id"));
  	  	p.setCoefDegre(rs.getString("coef_degre"));
  	  	
  	  	p.setC11(rs.getInt("c11"));
  	  	p.setC12(rs.getInt("c12"));
  	  	p.setC13(rs.getInt("c13"));
  	  	p.setC14(rs.getInt("c14"));
  	  	p.setC15(rs.getInt("c15"));
  	  	p.setC16(rs.getInt("c16"));
  	  	p.setC17(rs.getInt("c17"));
  	  	
  	  	p.setC21(rs.getInt("c21"));
	  	p.setC22(rs.getInt("c22"));
	  	p.setC23(rs.getInt("c23"));
	  	p.setC24(rs.getInt("c24"));
	  	p.setC25(rs.getInt("c25"));
	  	p.setC26(rs.getInt("c26"));
	  	p.setC27(rs.getInt("c27"));
	  	
	  	p.setC31(rs.getInt("c31"));
  	  	p.setC32(rs.getInt("c32"));
  	  	p.setC33(rs.getInt("c33"));
  	  	p.setC34(rs.getInt("c34"));
  	  	p.setC35(rs.getInt("c35"));
  	  	p.setC36(rs.getInt("c36"));
  	  	p.setC37(rs.getInt("c37"));
  	  	
  	  	p.setC41(rs.getInt("c41"));
	  	p.setC42(rs.getInt("c42"));
	  	p.setC43(rs.getInt("c43"));
	  	p.setC44(rs.getInt("c44"));
	  	p.setC45(rs.getInt("c45"));
	  	p.setC46(rs.getInt("c46"));
	  	p.setC47(rs.getInt("c47"));
	  	
	  	p.setC51(rs.getInt("c51"));
  	  	p.setC52(rs.getInt("c52"));
  	  	p.setC53(rs.getInt("c53"));
  	  	p.setC54(rs.getInt("c54"));
  	  	p.setC55(rs.getInt("c55"));
  	  	p.setC56(rs.getInt("c56"));
  	  	p.setC57(rs.getInt("c57"));
  	  	
  	  	p.setC61(rs.getInt("c61"));
	  	p.setC62(rs.getInt("c62"));
	  	p.setC63(rs.getInt("c63"));
	  	p.setC64(rs.getInt("c64"));
	  	p.setC65(rs.getInt("c65"));
	  	p.setC66(rs.getInt("c66"));
	  	p.setC67(rs.getInt("c67"));
	  	
	  	p.setC71(rs.getInt("c71"));
  	  	p.setC72(rs.getInt("c72"));
  	  	p.setC73(rs.getInt("c73"));
  	  	p.setC74(rs.getInt("c74"));
  	  	p.setC75(rs.getInt("c75"));
  	  	p.setC76(rs.getInt("c76"));
  	  	p.setC77(rs.getInt("c77"));
  	  	
  	  	return p;
    }
}

