package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.musicBackend.model.StyleGroupDrumsIns;

public class StyleGroupDrumsInsRowMapper  implements RowMapper<StyleGroupDrumsIns> {
	
	public StyleGroupDrumsIns mapRow(ResultSet rs, int arg1) throws SQLException {
		
		StyleGroupDrumsIns p = new StyleGroupDrumsIns();
		
  	  	p.setId(rs.getLong("id"));
  	  	p.setStyleId(rs.getLong("style_id"));
  	  	p.setGroupDrumsId(rs.getLong("group_drums_id"));
  	  	p.setName(rs.getString("name"));
  	  	p.setRhythm(rs.getInt("rhythm"));
  	  	
  	  	p.setPourcentagePlayingTempsFortMin(rs.getInt("pourcentage_playing_temps_fort_min"));
	  	p.setPourcentagePlayingTempsFortMax(rs.getInt("pourcentage_playing_temps_fort_max"));
  	  	
  	  	p.setPourcentagePlayingTempsFaibleMin(rs.getInt("pourcentage_playing_temps_faible_min"));
  	  	p.setPourcentagePlayingTempsFaibleMax(rs.getInt("pourcentage_playing_temps_faible_max"));
  	  	
  	  	p.setPourcentagePlayingTemps1Min(rs.getInt("pourcentage_playing_temps1min"));
  	  	p.setPourcentagePlayingTemps1Max(rs.getInt("pourcentage_playing_temps1max"));
  	  	
  	  	p.setPourcentagePlayingTemps2Min(rs.getInt("pourcentage_playing_temps2min"));
	  	p.setPourcentagePlayingTemps2Max(rs.getInt("pourcentage_playing_temps2max"));
	  	
	  	p.setPourcentagePlayingTemps3Min(rs.getInt("pourcentage_playing_temps3min"));
  	  	p.setPourcentagePlayingTemps3Max(rs.getInt("pourcentage_playing_temps3max"));
  	  	
  	  	p.setPourcentagePlayingTemps4Min(rs.getInt("pourcentage_playing_temps4min"));
	  	p.setPourcentagePlayingTemps4Max(rs.getInt("pourcentage_playing_temps4max"));

  	  	return p;
    }

}

