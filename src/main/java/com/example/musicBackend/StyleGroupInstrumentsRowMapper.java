package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.musicBackend.model.StyleGroupInstruments;

public class StyleGroupInstrumentsRowMapper  implements RowMapper<StyleGroupInstruments> {
	
	public StyleGroupInstruments mapRow(ResultSet rs, int arg1) throws SQLException {
		
		StyleGroupInstruments p = new StyleGroupInstruments();
		
  	  	p.setId(rs.getLong("id"));
  	  	p.setStyleId(rs.getLong("style_id"));
  	  	p.setMotherId(rs.getLong("mother_id"));
  	  	p.setName(rs.getString("name"));

  	  	p.setPourcentagePlayingMin(rs.getInt("pourcentage_playing_min"));
		p.setPourcentagePlayingMax(rs.getInt("pourcentage_playing_max")); 
		p.setBalanceBassMelodyMin(rs.getInt("balance_bass_melody_min")); 
		p.setBalanceBassMelodyMax(rs.getInt("balance_bass_melody_max")); 
		p.setBalanceMelodyMelodyBisMin(rs.getInt("balance_melody_melody_bis_min")); 
		p.setBalanceMelodyMelodyBisMax(rs.getInt("balance_melody_melody_bis_max")); 
  	  	p.setgBetweenNotesMax(rs.getInt("g_between_notes_max"));
  	  	p.setgBetweenNotesMin(rs.getInt("g_between_notes_min"));
  	  	p.setDispersionNotesMin(rs.getInt("dispersion_notes_min"));
  	  	p.setDispersionNotesMax(rs.getInt("dispersion_notes_max"));
  	  	p.setPourcentageSilMin(rs.getInt("pourcentage_sil_min"));
  	  	p.setPourcentageSilMax(rs.getInt("pourcentage_sil_max"));
  	  	p.setSpeedMax(rs.getInt("speed_max"));
  	  	p.setSpeedMin(rs.getInt("speed_min"));
  	  	p.setpSyncopesMin(rs.getInt("p_syncopes_min"));
  	  	p.setpSyncopesMax(rs.getInt("p_syncopes_max"));
  	  	p.setpSyncopettesMin(rs.getInt("p_syncopettes_min"));
  	  	p.setpSyncopettesMax(rs.getInt("p_syncopettes_max"));
  	  	p.setpSyncopesTempsFortsMin(rs.getInt("p_syncopes_temps_forts_min"));
  	  	p.setpSyncopesTempsFortsMax(rs.getInt("p_syncopes_temps_forts_max"));
  	  	p.setpContreTempsMin(rs.getInt("p_contre_temps_min"));
  	  	p.setpContreTempsMax(rs.getInt("p_contre_temps_max"));
  	  	p.setpDissonanceMin(rs.getInt("p_dissonance_min"));
  	  	p.setpDissonanceMax(rs.getInt("p_dissonance_max"));
  	  	p.setpIrregularitesMin(rs.getInt("p_irregularites_min"));
  	  	p.setpIrregularitesMax(rs.getInt("p_irregularites_max"));
  	  	p.setbBinaireTernaireMin(rs.getInt("b_binaire_ternaire_min"));
  	  	p.setbBinaireTernaireMax(rs.getInt("b_binaire_ternaire_max"));
  	  	p.setpChanceSupSameRythmMin(rs.getInt("p_chance_sup_same_rythm_min"));
  	  	p.setpChanceSupSameRythmMax(rs.getInt("p_chance_sup_same_rythm_max"));
  	  	p.setNumberNotesSimultaneouslyMin(rs.getInt("number_notes_simultaneously_min"));
  	  	p.setNumberNotesSimultaneouslyMax(rs.getInt("number_notes_simultaneously_max"));
  	  	p.setProbabilityRythmMin(rs.getInt("probability_rythm_min"));
  	  	p.setProbabilityRythmMax(rs.getInt("probability_rythm_max"));
  	  	p.setNumberRythmMin(rs.getInt("number_rythm_min"));
  	  	p.setNumberRythmMax(rs.getInt("number_rythm_max"));
  	  	p.setWeightRythmMin(rs.getInt("weight_rythm_min"));
  	  	p.setWeightRythmMax(rs.getInt("weight_rythm_max"));
  	  	
  	  	p.setpChanceSameNoteInMotifMin(rs.getInt("p_chance_same_note_in_motif_min"));
  		p.setpChanceSameNoteInMotifMax(rs.getInt("p_chance_same_note_in_motif_max"));
  		p.setpChanceStartByTonaleAsBassMin(rs.getInt("p_chance_start_by_tonale_as_bass_min"));
  		p.setpChanceStartByTonaleAsBassMax(rs.getInt("p_chance_start_by_tonale_as_bass_max"));
  		p.setpChanceStayAroundNoteRefMin(rs.getInt("p_chance_stay_around_note_ref_min"));
  		p.setpChanceStayAroundNoteRefMax(rs.getInt("p_chance_stay_around_note_ref_max"));
  		p.setpChancePlayChordMin(rs.getInt("p_chance_play_chord_min"));
  		p.setpChancePlayChordMax(rs.getInt("p_chance_play_chord_max"));

  	  	return p;
    }

}


