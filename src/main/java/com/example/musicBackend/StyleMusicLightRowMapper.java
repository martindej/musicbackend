package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.musicBackend.model.StyleMusic;

public class StyleMusicLightRowMapper  implements RowMapper<StyleMusic> {
	
	public StyleMusic mapRow(ResultSet rs, int arg1) throws SQLException {
		
		StyleMusic p = new StyleMusic();
		
  	  	p.setId(rs.getLong("id"));
  	  	p.setStyleName(rs.getString("style_name"));
  	  	
		return p;
	}

}
