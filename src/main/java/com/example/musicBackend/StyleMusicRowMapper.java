package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.musicBackend.model.StyleMusic;

public class StyleMusicRowMapper  implements RowMapper<StyleMusic> {
	
	public StyleMusic mapRow(ResultSet rs, int arg1) throws SQLException {
		
		StyleMusic p = new StyleMusic();
		
  	  	p.setId(rs.getLong("id"));
  	  	p.setPublish(rs.getBoolean("publish"));
  	  	p.setStyleName(rs.getString("style_name"));
  	  	p.setRythmeMax(rs.getInt("rythme_max"));
  	  	p.setRythmeMin(rs.getInt("rythme_min"));
  	  	p.setnInstrumentMin(rs.getInt("n_instrument_min"));
	  	p.setnInstrumentMax(rs.getInt("n_instrument_max"));
	  	p.setnPercussionMin(rs.getInt("n_percussion_min"));
	  	p.setnPercussionMax(rs.getInt("n_percussion_max"));
	  	p.setComplexityChordMin(rs.getInt("complexity_chord_min"));
	  	p.setComplexityChordMax(rs.getInt("complexity_chord_max"));
	  	p.setNumberChordMin(rs.getInt("number_chord_min"));
	  	p.setNumberChordMax(rs.getInt("number_chord_max"));
	  	p.setSoundFont(rs.getString("sound_font"));

  	  	p.setImage(rs.getBytes("image"));

  	  	return p;
    }

}

