package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.example.musicBackend.model.Subscription;

public class SubscriptionRowMapper implements RowMapper<Subscription> {
	
	public Subscription mapRow(ResultSet rs, int arg1) throws SQLException {
		Subscription boo = new Subscription();
		boo.setId(rs.getLong("id"));
		boo.setName(rs.getString("name"));
		boo.setMusicsPerDay(rs.getInt("musics_per_day"));
		boo.setPricePerMonth(rs.getDouble("price_per_month"));
		boo.setPricePerYear(rs.getDouble("price_per_year"));
		boo.setLogo(rs.getBytes("logo"));
		boo.setGeneralSettings(rs.getBoolean("general_settings"));
		boo.setAdvancedSettings(rs.getBoolean("advanced_settings"));
		boo.setMelodySettings(rs.getBoolean("melody_settings"));
		boo.setDurationInDays(rs.getInt("duration_in_days"));

  	  	return boo;
	}
}