package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import com.example.musicBackend.model.SubscriptionUser;

public class SubscriptionUserRowMapper implements RowMapper<SubscriptionUser> {
	
	public SubscriptionUser mapRow(ResultSet rs, int arg1) throws SQLException {
		SubscriptionUser boo = new SubscriptionUser();
		boo.setId(rs.getLong("id"));
		boo.setDuration_days(rs.getInt("duration_days"));
		boo.setStart_date(rs.getLong("start_date"));
		boo.setEnd_date(rs.getLong("end_date"));
		boo.setMusicsPerDay(rs.getInt("musics_per_day"));
		boo.setAdvancedSettings(rs.getBoolean("advanced_settings"));
		boo.setMelodySettings(rs.getBoolean("melody_settings"));
		boo.setSubscriptionId(rs.getLong("subscription_id"));
		boo.setUserId(rs.getLong("user_id"));

  	  	return boo;
	}
}
