package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.musicBackend.model.UserFollowing;

public class UserFollowingRowMapper implements RowMapper<UserFollowing> {

	
	public UserFollowing mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		UserFollowing userFollowing = new UserFollowing();
		userFollowing.setId(rs.getLong("id"));
		userFollowing.setUser_id(rs.getLong("user_id"));
		userFollowing.setFollowing_user_id(rs.getLong("following_user_id"));

		return userFollowing;
	}

}
