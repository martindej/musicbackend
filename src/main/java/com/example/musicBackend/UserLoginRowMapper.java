package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.example.musicBackend.model.UserLogin;

public class UserLoginRowMapper implements RowMapper<UserLogin> {
	
	public UserLogin mapRow(ResultSet rs, int arg1) throws SQLException {
		UserLogin p = new UserLogin();
  	  	p.setUsername(rs.getString("username"));
  	  	p.setPassword(rs.getString("password"));
  	  	return p;
    }

}