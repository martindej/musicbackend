package com.example.musicBackend;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.lob.DefaultLobHandler;
import org.springframework.jdbc.support.lob.LobHandler;

import com.example.musicBackend.model.User;

public class UserRowMapper implements RowMapper<User> {
	
//	@Autowired @Qualifier("myDefaultLobHandler")
	private LobHandler lobHandler = new DefaultLobHandler();
	
	public User mapRow(ResultSet rs, int arg1) throws SQLException {
		User p = new User();
  	  	p.setId(rs.getLong("id"));
  	  	p.setUsername(rs.getString("username"));
//  	  	p.setDescription(rs.getString("description"));
  	  	byte[] requestData = lobHandler.getBlobAsBytes(rs,"description");
  	  	if(requestData != null && requestData.length > 0) {
  	  		p.setDescription(new String(requestData));
  	  	}
  	  	else {
  	  	p.setDescription("");
  	  	}
  	  	
  	  	p.setPassword(rs.getString("password"));
  	  	p.setEmail(rs.getString("email"));
  	  	p.setEmailChecked(rs.getBoolean("email_checked"));
  	  	p.setNewsletter(rs.getBoolean("newsletter"));
  	  	p.setIdentifier(rs.getString("identifier"));
  	  	p.setComposer(rs.getBoolean("composer"));
  	  	p.setPlayer(rs.getBoolean("player"));
  	  	p.setInstruments(rs.getString("instruments"));
  	  	p.setAvatar(rs.getBytes("avatar"));
	  	p.setNameAvatar(rs.getString("name_avatar"));
	  	p.setSuperUser(rs.getBoolean("super_user"));
	  	p.setSubscriptionId(rs.getLong("subscription_id"));
	  	p.setBoosterId(rs.getLong("booster_id"));
  	  	return p;
    }

}

