package com.example.musicBackend.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
 
@Configuration
@EnableScheduling
public class SchedulerConfig {
 
	// permet d'autoriser l'utilisation de Scheduling
    // Declaring the beans are related to the schedule here if necessary.
}
