package com.example.musicBackend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.musicBackend.model.Comment;
import com.example.musicBackend.service.ServiceComment;

@RestController
@RequestMapping(value = "/comment")
public class CommentController {
	
	@Autowired
	ServiceComment serviceComment;
	
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<Comment> saveComment(@RequestBody Comment comment) {
		this.serviceComment.addNew(comment);
		return ResponseEntity.ok(comment);
	}
	
	@RequestMapping(value = "/signalComment/{idComment}/{idUser}/{idOwner}", method = RequestMethod.POST)
	public ResponseEntity<Boolean> signalComment(@PathVariable("idComment") long idComment, @PathVariable("idUser") long idUser, @PathVariable("idOwner") long idOwner) {
		boolean result = this.serviceComment.signalComment(idComment, idUser, idOwner) > 0;
		return ResponseEntity.ok(result);
	}
	
	@RequestMapping(value = "/getCommentsByMusicId/{idMusic}/{page}", method = RequestMethod.GET)
	public List<Comment> getCommentsByMusicId(@PathVariable("idMusic") long idMusic, @PathVariable("page") int page) {
		return serviceComment.getAllComments(idMusic, page);
	}
	
	@RequestMapping(path="/getNumberCommentsByMusicId/{music_id}", method = RequestMethod.GET)
	public int getNumberCommentsByMusicId(@PathVariable("music_id") long music_id) {
		return serviceComment.getNumberCommentsByMusicId(music_id);
	}
	
	@RequestMapping(path="/getNumberLikesByCommentId/{comment_id}", method = RequestMethod.GET)
	public int getNumberLikesByMusicId(@PathVariable("comment_id") long comment_id) {
		return serviceComment.numberLikesByCommentId(comment_id);
	}
	
	@RequestMapping(path="/getNumberDislikesByCommentId/{comment_id}", method = RequestMethod.GET)
	public int getNumberDislikesByCommentId(@PathVariable("comment_id") long comment_id) {
		return serviceComment.numberDislikesByCommentId(comment_id);
	}
	
	@RequestMapping(path = "/like/{idComment}/{idUser}/{idOwner}", method = RequestMethod.GET)
	public boolean like(@PathVariable("idComment") long idComment, @PathVariable("idUser") long idUser, @PathVariable("idOwner") long idOwner) {
		
		int ok = 0;
		
		ok = serviceComment.likeCommentById(idComment, idUser, idOwner);
		
		if(ok > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	@RequestMapping(path = "/dislike/{idComment}/{idUser}/{idOwner}", method = RequestMethod.GET)
	public boolean dislike(@PathVariable("idComment") long idComment, @PathVariable("idUser") long idUser, @PathVariable("idOwner") long idOwner) {
		
		int ok = 0;
		
		ok = serviceComment.dislikeCommentById(idComment, idUser, idOwner);
		
		if(ok > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	@RequestMapping(path = "/removeEntryLike/{idComment}/{idUser}", method = RequestMethod.GET)
	public boolean removeEntryLike(@PathVariable("idComment") long idComment, @PathVariable("idUser") long idUser) {
		
		int ok = 0;
		
		ok = serviceComment.removeEntryLikeCommentById(idComment, idUser);
		
		if(ok > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	@RequestMapping(path = "/hasLike/{idComment}/{idUser}", method = RequestMethod.GET)
	public boolean hasLike(@PathVariable("idComment") long idComment, @PathVariable("idUser") long idUser) {
		return serviceComment.hasLike(idComment, idUser);
	}
	
	@RequestMapping(path = "/hasDislike/{idComment}/{idUser}", method = RequestMethod.GET)
	public boolean hasDislike(@PathVariable("idComment") long idComment, @PathVariable("idUser") long idUser) {
		return serviceComment.hasDislike(idComment, idUser);
	}
	
	@RequestMapping(path = "/deleteComment/{idComment}/{id}", method = RequestMethod.GET)
	public int deleteComment( @PathVariable("idComment") long idComment, @PathVariable("id") long id) {
		return serviceComment.deleteCommentById(idComment, id);
	}
}
