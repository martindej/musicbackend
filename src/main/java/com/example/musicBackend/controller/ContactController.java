package com.example.musicBackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.musicBackend.model.MessageMail;
import com.example.musicBackend.service.EmailService;

@RestController
@RequestMapping(value = "/contact")
public class ContactController {
	
	@Autowired
	EmailService emailService;
	
	@RequestMapping(value = "/send", method = RequestMethod.POST)
	public ResponseEntity<Boolean> send(@RequestBody MessageMail messageMail) {

		this.emailService.sendSimpleMessage(messageMail.getEmail(), "message from user "+ messageMail.getName(), messageMail.getMessage());
		return ResponseEntity.ok(true);
	}

}
