package com.example.musicBackend.controller;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.musicBackend.model.Subscription;
import com.example.musicBackend.model.SubscriptionUser;
import com.example.musicBackend.service.ServiceSubscription;
import com.example.musicBackend.service.ServiceUser;
import com.example.musicBackend.service.Sha;



/**
 * Empfängt IPN Events von Zahlungsdienstleistern wie z.B. PayPal. Der Zahlungsanbieter sendet 4 Tage lang IPN Events bis eine 
 * Bestätigung an ihn zurückgesendet wird. IPN ist nicht synchron. Dieser IPN Endpunkt muss per HTTPS erreichbar sein.
 * @link https://developer.paypal.com/docs/classic/products/instant-payment-notification/
 * @link https://developer.paypal.com/docs/classic/ipn/integration-guide/IPNImplementation/
 *
 */
@RestController
@RequestMapping(value = "/payment")
public class IPNController {

	@Autowired
	ServiceSubscription serviceSubscription;
	
	@Autowired
	ServiceUser serviceUser;

//	private final SimpMessagingTemplate template;
	private static final String USER_AGENT = "ME IPN Responder";
	private static Logger LOG = LoggerFactory.getLogger(IPNController.class);
	
	// use that for websockets
//	@Autowired
//	IPNController(SimpMessagingTemplate template) {
//		this.template = template;
//	}

	private static final String urlSystemPay = "https://paiement.systempay.fr/vads-payment/";
	private static final boolean sandboxmode = true;

	/**
	 * IPN Listener
	 * @param request
	 * @param response
	 */
	@RequestMapping(value="/ipn", method = RequestMethod.POST)
	public void handleIpn(HttpServletRequest request, HttpServletResponse response) {
		
		String vads_trans_status = "", vads_cust_email = "", vads_cust_id = "", vads_order_id = "", vads_product_amountN = "", signature = "";
//		, 
//		@RequestParam("vads_trans_status") String vads_trans_status,
//		@RequestParam("vads_cust_email") String vads_cust_email,
//		@RequestParam("vads_cust_id") String vads_cust_id,
//		@RequestParam("vads_order_id") String vads_order_id, 
//		@RequestParam("vads_product_amountN") String vads_product_amountN,
//		@RequestParam("signature") String signature
		
		SortedSet<String> vadsFields = new TreeSet<String>();
		Enumeration<String> paramNames = request.getParameterNames(); 
		String certificate = "TwgiPDelEzrcdLYh";

		// Recupere et trie les noms des champs vads_* par ordre alphabetique
		while (paramNames.hasMoreElements()) {
			String paramName = paramNames.nextElement();
			if (paramName.startsWith( "vads_" )) {
				vadsFields.add(paramName);
			}
			else if(paramName.equals("signature")) {
				String vadsParamValue = request.getParameter(paramName);
				signature = vadsParamValue;
			}
		}
		// Calcule la signature
		String sep = Sha.SEPARATOR;
		StringBuilder sb = new StringBuilder();
		for (String vadsParamName : vadsFields) {
			
			String vadsParamValue = request.getParameter(vadsParamName);
			
//			System.out.println(vadsParamName + " : "+vadsParamValue);
			
			if(vadsParamName.equals("vads_trans_status")) {
				vads_trans_status = vadsParamValue;
			}
			else if(vadsParamName.equals("vads_cust_email")) {
				vads_cust_email = vadsParamValue;
			}
			else if(vadsParamName.equals("vads_cust_id")) {
				vads_cust_id = vadsParamValue;
			}
			else if(vadsParamName.equals("vads_order_id")) {
				vads_order_id = vadsParamValue;
			}
			else if(vadsParamName.equals("vads_product_amountN")) {
				vads_product_amountN = vadsParamValue;
			}
			if (vadsParamValue != null) {
				sb.append(vadsParamValue);
			}
			sb.append(sep);
		}
		
		sb.append( certificate );
		String c_sign = Sha.encode(sb.toString());
		
		String reqUri = "";
		if (request != null) {
			reqUri = request.getRequestURI();
		}

		LOG.info("[ uri : {} ] - IPN Callback wird aufgerufen", reqUri);
		if(LOG.isDebugEnabled()) {
			logRequestHeaders(request);
		}
		// write an ipn flag to bestellung or do some other clever things
		LOG.debug("Invoice: "+request.getParameter("invoice"));

//		System.out.println(signature);
//		System.out.println(c_sign);
		System.out.println(vads_trans_status);
		if(c_sign.equals(signature)) {
			// TODO : create a response for each cases
			if(vads_trans_status.equals("AUTHORISED")) {
				Subscription subscription = serviceSubscription.getSubscriptionById(Long.parseLong(vads_order_id));
				SubscriptionUser subscriptionUser = new SubscriptionUser(Long.parseLong(vads_cust_id), Long.parseLong(vads_order_id), new Date().getTime(), 365, subscription.getMusicsPerDay(), subscription.isAdvancedSettings(), subscription.isMelodySettings());
				long subscriptionUserId = serviceSubscription.addSubscriptionUser(subscriptionUser);
				serviceUser.setSubscriptionId(subscriptionUser.getUserId(), subscriptionUserId);
				this.serviceSubscription.incrementOrderId();
				// use that for websockets !!  
//				this.template.convertAndSend("/sub", vads_cust_id);
			}
		}
	}



	private void logRequestHeaders(HttpServletRequest request) {
		Enumeration<String> h = request.getHeaderNames();
		while (h.hasMoreElements()) {
			String s = (String) h.nextElement();
			LOG.debug("Header: "+s+" - "+request.getHeader(s));
		}
	}



	String buildResponseData(Enumeration<String> n, Map<String, String[]> map) throws UnsupportedEncodingException {
		StringBuffer buffer = new StringBuffer("cmd=_notify-validate");
		while (n.hasMoreElements()) {
			buffer.append("&");
			String s = (String) n.nextElement();
			buffer.append(s);
			buffer.append("=");
			buffer.append(URLEncoder.encode(map.get(s)[0], "UTF-8"));
		}
		return buffer.toString();
	}


	
	/**
	 * Return the unchanged IPN Message to Paypal only with notify validated.
	 * 
	 * @param ipnMessage IPN Message
	 * @throws Exception in case of an Error or Paypal send INVLAID back
	 */
	private void sendIpnMessageToPaypal(String url, String ipnReturnMessage) throws Exception {
		// TODO do this in a new thread
		LOG.debug("Test");
		LOG.info("Send IPN Message 'verified' to Paypal: "+url+" with IPN: "+ipnReturnMessage);

		HttpClient client = HttpClientBuilder.create().build();
		// TODO use right paypal url based in env
		HttpPost post = new HttpPost(url);

		// TODO ask for user agent
		post.setHeader("User-Agent", USER_AGENT);
		post.setHeader("content-type", "application/x-www-form-urlencoded");
		post.setHeader("host", "www.paypal.com");

		
		post.setEntity(new StringEntity(ipnReturnMessage) );
		
		HttpResponse response = client.execute(post);
		LOG.debug("Response Code : "  + response.getStatusLine().getStatusCode()+" "+response.getStatusLine().getReasonPhrase());
		Header[] h = response.getAllHeaders();
		for (int i = 0; i < h.length; i++) {
			LOG.debug("Header: "+h[i].getName()+ " "+h[i].getValue());
		}
		// validate response
		InputStream r = response.getEntity().getContent();
		String reponseMessage = IOUtils.toString( r, Charset.defaultCharset());
		if (reponseMessage.equalsIgnoreCase("VERIFIED")) {
			LOG.info("IPN Message verified by Paypal successfully");
		} else {
			throw new Exception("IPN Message not verified by Paypal: "+reponseMessage);
		}
	}


	private void sendIpnMessageToPaypal2(String url, String ipnReturnMessage) throws Exception {
		// TODO do this in a new thread
		// Don't write live customer data to logs
		if(sandboxmode) {
			LOG.debug("IPN: "+ipnReturnMessage);
		}
		LOG.info("Send IPN Message 'verified' to Paypal: "+url);
		
		URL u = new URL(url);
		HttpURLConnection conn = (HttpURLConnection) u.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		OutputStream os = conn.getOutputStream();
		os.write(ipnReturnMessage.getBytes());
		conn.connect();
		
		InputStream in = conn.getInputStream();
		String ins = IOUtils.toString(in, "UTF-8");
		String m = conn.getResponseMessage();
		LOG.debug("Response Code : "  + conn.getResponseCode()+" "+m+" - "+ins);
		if (ins.equalsIgnoreCase("VERIFIED")) {
			LOG.info("IPN Message verified by Paypal successfully");
		} else {
			throw new Exception("IPN Message not verified by Paypal: "+ins);
		}
	}

}