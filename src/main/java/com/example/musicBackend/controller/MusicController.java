package com.example.musicBackend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.example.musicBackend.MusicFileCallable;
import com.example.musicBackend.model.FilterMusic;
import com.example.musicBackend.model.MesureTempsFortFull;
import com.example.musicBackend.model.MusicDetail;
import com.example.musicBackend.model.MusicFile;
import com.example.musicBackend.model.MusicFileLight;
import com.example.musicBackend.model.MusicParameters;
import com.example.musicBackend.model.MusicParametersLight;
import com.example.musicBackend.model.StyleGroupDrumsFull;
import com.example.musicBackend.model.StyleGroupDrumsInsFull;
import com.example.musicBackend.model.StyleGroupInstruments;
import com.example.musicBackend.model.StyleGroupInstrumentsFull;
import com.example.musicBackend.model.StyleMusicFull;
import com.example.musicBackend.model.User;
import com.example.musicBackend.randomNameGenerator.RandomNameGenerator;
import com.example.musicBackend.service.ServiceMusic;
import com.example.musicBackend.service.ServiceStyle;
import com.example.musicBackend.service.ServiceSubscription;
import com.example.musicBackend.service.ServiceUser;

import analyse.Analyse;
import enums.EnumInstruments;
import enums.EnumMusicStyle;
import styles.MesureTempsFort;
import styles.Style;
import styles.StyleCoefDegres;
import styles.StyleGroupDrums;
import styles.StyleGroupDrumsIns;
import styles.StyleGroupIns;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@RestController
@RequestMapping(value = "/music")
public class MusicController {
	
//	@Autowired
//	ServiceImpl repository;
	
	@Autowired
	ServiceMusic serviceMusic;
	
	@Autowired
	ServiceUser serviceUser;
	
	@Autowired
	ServiceStyle serviceStyle;
	
	@Autowired
	ServiceSubscription serviceSubscription;
	
	private static Random RANDOM = new Random();
	
	@RequestMapping(path = "/getWav/{id}", method = RequestMethod.GET)
	public ResponseEntity<Resource> getWav(@PathVariable("id") long id) throws IOException {

		MusicFile m = serviceMusic.getMusicById(id);
		File musicWav = serviceMusic.getWav(m.getMidiFile(), m.getSoundFont());
		musicWav.deleteOnExit();
//		try {
//			musicWav = WavToMp3.encodeToMp3(musicWav);
//		} catch (IllegalArgumentException | EncoderException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}


	    Path path = Paths.get(musicWav.getAbsolutePath());
	    ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
	    
//	    ByteArrayResource resource = new ByteArrayResource(musicWav);
	    
	    HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

	    return ResponseEntity.ok()
	            .headers(headers)
	            .contentLength(musicWav.length())
	            .contentType(MediaType.parseMediaType("application/octet-stream"))
	            .body(resource);
	}
	
	@RequestMapping(value = "/uploadModifiedScore/{musicId}/{id}", method = RequestMethod.POST) ///{username}/{id}
	public ResponseEntity<String> uploadModifiedScore(@PathVariable("musicId") long musicId, @PathVariable("id") long id, @RequestParam("file") MultipartFile file) throws IOException { //@PathVariable("username") String username, @PathVariable("id") long id, 
		
		String message = "";
		
		try {
			this.serviceMusic.uploadModifiedScore(file, id, musicId);
 
			message = "You successfully uploaded " + file.getOriginalFilename() + "!";
			return ResponseEntity.status(HttpStatus.OK).body(message);
		} catch (Exception e) {
			message = "FAIL to upload " + file.getOriginalFilename() + "!";
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
		}
	}
	
	@RequestMapping(path = "/getMp3/{id}", method = RequestMethod.GET)
	public ResponseEntity<Resource> getMp3(@PathVariable("id") long id) throws IOException {

		MusicFile m = serviceMusic.getMusicById(id);

	    ByteArrayResource resource = new ByteArrayResource(m.getMp3File());
	    
	    HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

	    return ResponseEntity.ok()
	            .headers(headers)
	            .contentLength(m.getMp3File().length)
	            .contentType(MediaType.parseMediaType("application/octet-stream"))
	            .body(resource);
	}
	
	@RequestMapping(path = "/getDemoMp3/{random}", method = RequestMethod.GET)
	public ResponseEntity<Resource> getDemoMp3(@PathVariable("random") int random) throws IOException {

		long musicId = 0;
		List<Long> idsTopMusics = serviceMusic.getIdsTopMusics();
		
		if(idsTopMusics.size() > 0 && random < idsTopMusics.size()) {
			musicId = idsTopMusics.get(random);
		}
		else if(idsTopMusics.size() > 0) {
			musicId = idsTopMusics.get(RANDOM.nextInt(idsTopMusics.size()));
		}
		
		if(musicId != 0) {
			MusicFile m = serviceMusic.getMusicById(musicId);

		    ByteArrayResource resource = new ByteArrayResource(m.getMp3File());
		    
		    HttpHeaders headers = new HttpHeaders();
	        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
	        headers.add("Pragma", "no-cache");
	        headers.add("Expires", "0");

		    return ResponseEntity.ok()
		            .headers(headers)
		            .contentLength(m.getMp3File().length)
		            .contentType(MediaType.parseMediaType("application/octet-stream"))
		            .body(resource);
		}
		else {
			return null;
		}
		
	}
	
	@RequestMapping(path = "/getXml/{id}/{original}", method = RequestMethod.GET)
	public ResponseEntity<String> getXml(@PathVariable("id") long id, @PathVariable("original") boolean original) throws IOException {

		MusicFile m = serviceMusic.getMusicById(id);
		String result = null;

	    if(original) {
	    	result = m.getMusicXmlFile();
	    }
	    else {
	    	result = m.getMusicXmlFileModified();
	    }

	    return ResponseEntity.ok().body(result);
	}
	
	@RequestMapping(path = "/getDemoXml/{random}", method = RequestMethod.GET)
	public ResponseEntity<String> getDemoXml(@PathVariable("random") int random) throws IOException {
		
		
		long musicId = 0;
		String result = null;
		List<Long> idsTopMusics = serviceMusic.getIdsTopMusics();
		
		if(idsTopMusics.size() > 0 && random < idsTopMusics.size()) {
			musicId = idsTopMusics.get(random);
		}
		else if(idsTopMusics.size() > 0) {
			musicId = idsTopMusics.get(RANDOM.nextInt(idsTopMusics.size()));
		}
		
		if(musicId != 0) {
			MusicFile m = serviceMusic.getMusicById(musicId);
			result = m.getMusicXmlFile();
			return ResponseEntity.ok().body(result);
		}
		else {
			return null;
		}
	}
	
	@RequestMapping(path = "/getMidi/{id}", method = RequestMethod.GET)
	public ResponseEntity<Resource> getMidi(@PathVariable("id") long id) throws IOException {

		MusicFile m = serviceMusic.getMusicById(id);

	    ByteArrayResource resource = new ByteArrayResource(m.getMidiFile());
	    
	    HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

	    return ResponseEntity.ok()
	            .headers(headers)
	            .contentLength(m.getMidiFile().length)
	            .contentType(MediaType.parseMediaType("application/octet-stream"))
	            .body(resource);
	}
//	
//	@RequestMapping("/xml/{id}")
//	public ResponseEntity<?> getAttachment(@PathVariable("id") long id) throws IOException {
//
//		MusicFile music = serviceMusic.getMusicById(id);
//		return ResponseEntity.ok().contentType(MediaType.APPLICATION_XML).body(music.getMusicXmlFile());
//	}
	
	@RequestMapping(path = "/compose/{id}", method = RequestMethod.POST)
	public ResponseEntity<MusicFileLight> compose(@PathVariable("id") long id, @RequestBody MusicParametersLight musicParametersLight) throws IOException, InterruptedException, ExecutionException {

		MusicFile m = null;
		MusicFileLight mLight = null;
		User user = serviceUser.getUserById(id);
		int numberSub = serviceSubscription.getNumberMusicsSubscriptionByUserId(id);
		int numberBooster = serviceSubscription.getNumberMusicsBoosterByUserId(id);
		int swing = musicParametersLight.getSwing();
		
		if( numberSub > 0 || numberBooster > 0) {
			
			long creationDate = new Date().getTime();
			
			StyleMusicFull styleMusicFull = this.serviceStyle.getByIdFull(musicParametersLight.getStyle());
			
			List<StyleGroupIns> styleGroupIns = new ArrayList<StyleGroupIns>();
			List<StyleGroupDrums> styleGroupDrums = new ArrayList<StyleGroupDrums>();
			List<StyleGroupDrumsIns> styleGroupDrumsIns = new ArrayList<StyleGroupDrumsIns>();
			List<MesureTempsFort> mesureTempsForts = new ArrayList<MesureTempsFort>();
			List<StyleCoefDegres> styleCoefDegres = new ArrayList<StyleCoefDegres>();
			List<EnumInstruments> instruments = new ArrayList<EnumInstruments>();
			
			for(Integer i:musicParametersLight.getListInstruments()) {
				instruments.add(EnumInstruments.getByIndex(i));
			}
			
			for(StyleGroupInstrumentsFull sgIns:styleMusicFull.getStyleGroupIns()) {
				
				styleGroupIns.add(new StyleGroupIns(
						sgIns.getId(),
						sgIns.getName(),
						sgIns.getMotherId(),
						sgIns.getInstruments(),
						sgIns.getPourcentagePlayingMin(),
						sgIns.getPourcentagePlayingMax(), 
						sgIns.getBalanceBassMelodyMin(), 
						sgIns.getBalanceBassMelodyMax(), 
						sgIns.getBalanceMelodyMelodyBisMin(), 
						sgIns.getBalanceMelodyMelodyBisMax(), 
						sgIns.getgBetweenNotesMin(),
						sgIns.getgBetweenNotesMax(),
						sgIns.getDispersionNotesMin(),
						sgIns.getDispersionNotesMax(),
						sgIns.getPourcentageSilMin(),
						sgIns.getPourcentageSilMax(),
						sgIns.getSpeedMax(),
						sgIns.getSpeedMin(),
						sgIns.getpSyncopesMin(),
						sgIns.getpSyncopesMax(),
						sgIns.getpSyncopettesMin(),
						sgIns.getpSyncopettesMax(),
						sgIns.getpSyncopesTempsFortsMin(),
						sgIns.getpSyncopesTempsFortsMax(),
						sgIns.getpContreTempsMin(),
						sgIns.getpContreTempsMax(),
						sgIns.getpDissonanceMin(),
						sgIns.getpDissonanceMax(),
						sgIns.getpIrregularitesMin(),
						sgIns.getpIrregularitesMax(),
						sgIns.getbBinaireTernaireMin(),
						sgIns.getbBinaireTernaireMax(),
						sgIns.getpChanceSupSameRythmMin(),
						sgIns.getpChanceSupSameRythmMax(),
						sgIns.getNumberNotesSimultaneouslyMin(),
						sgIns.getNumberNotesSimultaneouslyMax(),
						sgIns.getProbabilityRythmMin(),
						sgIns.getProbabilityRythmMax(),
						sgIns.getNumberRythmMin(),
						sgIns.getNumberRythmMax(),
						sgIns.getWeightRythmMin(),
						sgIns.getWeightRythmMax(),
						sgIns.getpChanceSameNoteInMotifMin(),
						sgIns.getpChanceSameNoteInMotifMax(),
						sgIns.getpChanceStartByTonaleAsBassMin(),
						sgIns.getpChanceStartByTonaleAsBassMax(),
						sgIns.getpChanceStayAroundNoteRefMin(),
						sgIns.getpChanceStayAroundNoteRefMax(),
						sgIns.getpChancePlayChordMin(),
						sgIns.getpChancePlayChordMax()));
			}
			
			for(StyleGroupDrumsFull sgIns:styleMusicFull.getStyleGroupDrums()) {
				
				styleGroupDrumsIns = new ArrayList<StyleGroupDrumsIns>();
				
				for(StyleGroupDrumsInsFull sgDIs:sgIns.getDrumsIns()) {
					styleGroupDrumsIns.add(new StyleGroupDrumsIns(
							sgDIs.getId(),
							sgDIs.getStyleId(),
							sgDIs.getGroupDrumsId(),
							sgDIs.getName(),
							sgDIs.getRhythm(),
							sgDIs.getPourcentagePlayingTempsFortMin(),
							sgDIs.getPourcentagePlayingTempsFortMax(),
							sgDIs.getPourcentagePlayingTempsFaibleMin(),
							sgDIs.getPourcentagePlayingTempsFaibleMax(),
							sgDIs.getPourcentagePlayingTemps1Min(),
							sgDIs.getPourcentagePlayingTemps1Max(),
							sgDIs.getPourcentagePlayingTemps2Min(),
							sgDIs.getPourcentagePlayingTemps2Max(),
							sgDIs.getPourcentagePlayingTemps3Min(),
							sgDIs.getPourcentagePlayingTemps3Max(),
							sgDIs.getPourcentagePlayingTemps4Min(),
							sgDIs.getPourcentagePlayingTemps4Max(),
							sgDIs.getInstruments()));
				}
				
				styleGroupDrums.add(new StyleGroupDrums(
						sgIns.getId(),
						sgIns.getStyleId(),
						sgIns.getName(),
						sgIns.getRhythm(),
						sgIns.getPourcentagePlayingTempsFortMin(),
						sgIns.getPourcentagePlayingTempsFortMax(),
						sgIns.getPourcentagePlayingTempsFaibleMin(),
						sgIns.getPourcentagePlayingTempsFaibleMax(),
						sgIns.getPourcentagePlayingTemps1Min(),
						sgIns.getPourcentagePlayingTemps1Max(),
						sgIns.getPourcentagePlayingTemps2Min(),
						sgIns.getPourcentagePlayingTemps2Max(),
						sgIns.getPourcentagePlayingTemps3Min(),
						sgIns.getPourcentagePlayingTemps3Max(),
						sgIns.getPourcentagePlayingTemps4Min(),
						sgIns.getPourcentagePlayingTemps4Max(),
						styleGroupDrumsIns));
			}
			
			for(MesureTempsFortFull mTF:styleMusicFull.getMesureTempsFort()) {
				
				mesureTempsForts.add(new MesureTempsFort(
						mTF.getId(),
						mTF.getStyleId(),
						mTF.getMesure(),
						mTF.getTempsFort(),
						mTF.isEtOu()));

			}
			
			for(com.example.musicBackend.model.StyleCoefDegres sCD:styleMusicFull.getStyleCoefDegres()) {
				
				styleCoefDegres.add(new StyleCoefDegres(
						sCD.getC11(),
						sCD.getC12(),
						sCD.getC13(),
						sCD.getC14(),
						sCD.getC15(),
						sCD.getC16(),
						sCD.getC17(),
						
						sCD.getC21(),
						sCD.getC22(),
						sCD.getC23(),
						sCD.getC24(),
						sCD.getC25(),
						sCD.getC26(),
						sCD.getC27(),
						
						sCD.getC31(),
						sCD.getC32(),
						sCD.getC33(),
						sCD.getC34(),
						sCD.getC35(),
						sCD.getC36(),
						sCD.getC37(),
						
						sCD.getC41(),
						sCD.getC42(),
						sCD.getC43(),
						sCD.getC44(),
						sCD.getC45(),
						sCD.getC46(),
						sCD.getC47(),
						
						sCD.getC51(),
						sCD.getC52(),
						sCD.getC53(),
						sCD.getC54(),
						sCD.getC55(),
						sCD.getC56(),
						sCD.getC57(),
						
						sCD.getC61(),
						sCD.getC62(),
						sCD.getC63(),
						sCD.getC64(),
						sCD.getC65(),
						sCD.getC66(),
						sCD.getC67(),
						
						sCD.getC71(),
						sCD.getC72(),
						sCD.getC73(),
						sCD.getC74(),
						sCD.getC75(),
						sCD.getC76(),
						sCD.getC77(),
						sCD.getStyleId(),
						sCD.getCoefDegre()
						));

			}

			Style st = new Style(styleMusicFull.getStyleName(),
					styleMusicFull.getAvailableTon(),
					musicParametersLight.getTempo(),
					styleMusicFull.getRythmeMax(),
					styleMusicFull.getRythmeMin(), 
					styleMusicFull.getnInstrumentMin(), 
					styleMusicFull.getnInstrumentMax(),
					styleMusicFull.getnPercussionMin(), 
					styleMusicFull.getnPercussionMax(),
					styleMusicFull.getComplexityChordMin(),
					styleMusicFull.getComplexityChordMax(),
					styleMusicFull.getNumberChordMin(),
					styleMusicFull.getNumberChordMax(),
					styleCoefDegres,
					mesureTempsForts,
					styleGroupIns,
					styleGroupDrums);
			
			ExecutorService executor = Executors.newCachedThreadPool();
			Future<MusicFile> futureCall = executor.submit(new MusicFileCallable(st,id, user.getUsername(), creationDate, musicParametersLight.getMusicTitle(), musicParametersLight.getIdImage(), musicParametersLight.getTonality(), instruments, musicParametersLight.isPercussions(), styleMusicFull.getSoundFont(), serviceMusic, swing));
			m = futureCall.get(); // Here the thread will be blocked
			
			if(m != null) {
				if(numberSub > 0) {
					serviceSubscription.addNewMusicSubscription(id, creationDate, "subscription");
				}
				else {
					serviceSubscription.addNewMusicSubscription(id, creationDate, "booster");
					serviceSubscription.decrementBoosterByUserId(id);
				}
			}
			mLight = new MusicFileLight(m);
			return ResponseEntity.status(HttpStatus.OK).body(mLight);
		}
		else {
			mLight = new MusicFileLight(m);
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(mLight);
		}

	}
	
	@RequestMapping(path = "/getAllAnalyseParameters/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<String>> getAllAnalyseParameters(@PathVariable("id") long id) {
		
		User user = serviceUser.getUserById(id);
		
		if( user.isSuperUser()) {		
			return ResponseEntity.status(HttpStatus.OK).body(this.serviceMusic.findParamList(new StyleGroupInstruments()));
		}
		else {
			return ResponseEntity.status(HttpStatus.OK).body(new ArrayList<String>());
		}
	}
	
	@RequestMapping(path = "/analyse/{id}/{parameter}", method = RequestMethod.POST)
	public ResponseEntity<Analyse> analyse(@PathVariable("id") long id, @PathVariable("parameter") String parameter, @RequestBody MusicParametersLight musicParametersLight) throws IOException, InterruptedException, ExecutionException, NoSuchFieldException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {

		User user = serviceUser.getUserById(id);
		
		if( user.isSuperUser()) {
						
			StyleMusicFull styleMusicFull = this.serviceStyle.getByIdFull(musicParametersLight.getStyle());
			
			List<StyleGroupIns> styleGroupIns = new ArrayList<StyleGroupIns>();
			List<StyleGroupDrums> styleGroupDrums = new ArrayList<StyleGroupDrums>();
			List<StyleGroupDrumsIns> styleGroupDrumsIns = new ArrayList<StyleGroupDrumsIns>();
			List<MesureTempsFort> mesureTempsForts = new ArrayList<MesureTempsFort>();
			List<StyleCoefDegres> styleCoefDegres = new ArrayList<StyleCoefDegres>();
//			List<EnumInstruments> instruments = new ArrayList<EnumInstruments>();
			
//			for(Integer i:musicParametersLight.getListInstruments()) {
//				instruments.add(EnumInstruments.getByIndex(i));
//			}
			
			for(StyleGroupInstrumentsFull sgIns:styleMusicFull.getStyleGroupIns()) {
				
				styleGroupIns.add(new StyleGroupIns(
						sgIns.getId(),
						sgIns.getName(),
						sgIns.getMotherId(),
						sgIns.getInstruments(),
						sgIns.getPourcentagePlayingMin(),
						sgIns.getPourcentagePlayingMax(), 
						sgIns.getBalanceBassMelodyMin(), 
						sgIns.getBalanceBassMelodyMax(), 
						sgIns.getBalanceMelodyMelodyBisMin(), 
						sgIns.getBalanceMelodyMelodyBisMax(), 
						sgIns.getgBetweenNotesMin(),
						sgIns.getgBetweenNotesMax(),
						sgIns.getDispersionNotesMin(),
						sgIns.getDispersionNotesMax(),
						sgIns.getPourcentageSilMin(),
						sgIns.getPourcentageSilMax(),
						sgIns.getSpeedMax(),
						sgIns.getSpeedMin(),
						sgIns.getpSyncopesMin(),
						sgIns.getpSyncopesMax(),
						sgIns.getpSyncopettesMin(),
						sgIns.getpSyncopettesMax(),
						sgIns.getpSyncopesTempsFortsMin(),
						sgIns.getpSyncopesTempsFortsMax(),
						sgIns.getpContreTempsMin(),
						sgIns.getpContreTempsMax(),
						sgIns.getpDissonanceMin(),
						sgIns.getpDissonanceMax(),
						sgIns.getpIrregularitesMin(),
						sgIns.getpIrregularitesMax(),
						sgIns.getbBinaireTernaireMin(),
						sgIns.getbBinaireTernaireMax(),
						sgIns.getpChanceSupSameRythmMin(),
						sgIns.getpChanceSupSameRythmMax(),
						sgIns.getNumberNotesSimultaneouslyMin(),
						sgIns.getNumberNotesSimultaneouslyMax(),
						sgIns.getProbabilityRythmMin(),
						sgIns.getProbabilityRythmMax(),
						sgIns.getNumberRythmMin(),
						sgIns.getNumberRythmMax(),
						sgIns.getWeightRythmMin(),
						sgIns.getWeightRythmMax(),
						sgIns.getpChanceSameNoteInMotifMin(),
						sgIns.getpChanceSameNoteInMotifMax(),
						sgIns.getpChanceStartByTonaleAsBassMin(),
						sgIns.getpChanceStartByTonaleAsBassMax(),
						sgIns.getpChanceStayAroundNoteRefMin(),
						sgIns.getpChanceStayAroundNoteRefMax(),
						sgIns.getpChancePlayChordMin(),
						sgIns.getpChancePlayChordMax()));
			}
			
			for(StyleGroupDrumsFull sgIns:styleMusicFull.getStyleGroupDrums()) {
				
				styleGroupDrumsIns = new ArrayList<StyleGroupDrumsIns>();
				
				for(StyleGroupDrumsInsFull sgDIs:sgIns.getDrumsIns()) {
					styleGroupDrumsIns.add(new StyleGroupDrumsIns(
							sgDIs.getId(),
							sgDIs.getStyleId(),
							sgDIs.getGroupDrumsId(),
							sgDIs.getName(),
							sgDIs.getRhythm(),
							sgDIs.getPourcentagePlayingTempsFortMin(),
							sgDIs.getPourcentagePlayingTempsFortMax(),
							sgDIs.getPourcentagePlayingTempsFaibleMin(),
							sgDIs.getPourcentagePlayingTempsFaibleMax(),
							sgDIs.getPourcentagePlayingTemps1Min(),
							sgDIs.getPourcentagePlayingTemps1Max(),
							sgDIs.getPourcentagePlayingTemps2Min(),
							sgDIs.getPourcentagePlayingTemps2Max(),
							sgDIs.getPourcentagePlayingTemps3Min(),
							sgDIs.getPourcentagePlayingTemps3Max(),
							sgDIs.getPourcentagePlayingTemps4Min(),
							sgDIs.getPourcentagePlayingTemps4Max(),
							sgDIs.getInstruments()));
				}
				
				styleGroupDrums.add(new StyleGroupDrums(
						sgIns.getId(),
						sgIns.getStyleId(),
						sgIns.getName(),
						sgIns.getRhythm(),
						sgIns.getPourcentagePlayingTempsFortMin(),
						sgIns.getPourcentagePlayingTempsFortMax(),
						sgIns.getPourcentagePlayingTempsFaibleMin(),
						sgIns.getPourcentagePlayingTempsFaibleMax(),
						sgIns.getPourcentagePlayingTemps1Min(),
						sgIns.getPourcentagePlayingTemps1Max(),
						sgIns.getPourcentagePlayingTemps2Min(),
						sgIns.getPourcentagePlayingTemps2Max(),
						sgIns.getPourcentagePlayingTemps3Min(),
						sgIns.getPourcentagePlayingTemps3Max(),
						sgIns.getPourcentagePlayingTemps4Min(),
						sgIns.getPourcentagePlayingTemps4Max(),
						styleGroupDrumsIns));
			}
			
			for(MesureTempsFortFull mTF:styleMusicFull.getMesureTempsFort()) {
				
				mesureTempsForts.add(new MesureTempsFort(
						mTF.getId(),
						mTF.getStyleId(),
						mTF.getMesure(),
						mTF.getTempsFort(),
						mTF.isEtOu()));

			}
			
			for(com.example.musicBackend.model.StyleCoefDegres sCD:styleMusicFull.getStyleCoefDegres()) {
				
				styleCoefDegres.add(new StyleCoefDegres(
						sCD.getC11(),
						sCD.getC12(),
						sCD.getC13(),
						sCD.getC14(),
						sCD.getC15(),
						sCD.getC16(),
						sCD.getC17(),
						
						sCD.getC21(),
						sCD.getC22(),
						sCD.getC23(),
						sCD.getC24(),
						sCD.getC25(),
						sCD.getC26(),
						sCD.getC27(),
						
						sCD.getC31(),
						sCD.getC32(),
						sCD.getC33(),
						sCD.getC34(),
						sCD.getC35(),
						sCD.getC36(),
						sCD.getC37(),
						
						sCD.getC41(),
						sCD.getC42(),
						sCD.getC43(),
						sCD.getC44(),
						sCD.getC45(),
						sCD.getC46(),
						sCD.getC47(),
						
						sCD.getC51(),
						sCD.getC52(),
						sCD.getC53(),
						sCD.getC54(),
						sCD.getC55(),
						sCD.getC56(),
						sCD.getC57(),
						
						sCD.getC61(),
						sCD.getC62(),
						sCD.getC63(),
						sCD.getC64(),
						sCD.getC65(),
						sCD.getC66(),
						sCD.getC67(),
						
						sCD.getC71(),
						sCD.getC72(),
						sCD.getC73(),
						sCD.getC74(),
						sCD.getC75(),
						sCD.getC76(),
						sCD.getC77(),
						sCD.getStyleId(),
						sCD.getCoefDegre()
						));

			}

			Style st = new Style(styleMusicFull.getStyleName(),
					styleMusicFull.getAvailableTon(),
					0,
					styleMusicFull.getRythmeMax(),
					styleMusicFull.getRythmeMin(), 
					styleMusicFull.getnInstrumentMin(), 
					styleMusicFull.getnInstrumentMax(),
					styleMusicFull.getnPercussionMin(), 
					styleMusicFull.getnPercussionMax(),
					styleMusicFull.getComplexityChordMin(),
					styleMusicFull.getComplexityChordMax(),
					styleMusicFull.getNumberChordMin(),
					styleMusicFull.getNumberChordMax(),
					styleCoefDegres,
					mesureTempsForts,
					styleGroupIns,
					styleGroupDrums);
			
			Analyse analyse = new Analyse(st, parameter);
			
			return ResponseEntity.status(HttpStatus.OK).body(analyse);
		}
		else {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(null);
		}

	}
		
	@RequestMapping(path = "/take/{user_id}/{user_name}/{music_id}", method = RequestMethod.GET)
	public ResponseEntity<MusicFileLight> take(@PathVariable("user_id") long user_id, @PathVariable("user_name") String user_name, @PathVariable("music_id") long music_id) {

		int numberSub = serviceSubscription.getNumberMusicsSubscriptionByUserId(user_id);
		int numberBooster = serviceSubscription.getNumberMusicsBoosterByUserId(user_id);
		
		MusicFile musicCopy;
		MusicFileLight music = null;
		
		if( numberSub > 0 || numberBooster > 0) {
			
			long creationDate = new Date().getTime();
			musicCopy = serviceMusic.getCopyMusicById(music_id, user_id, user_name, creationDate);
			
			serviceMusic.addNew(musicCopy);
			
			music = serviceMusic.getByUserIdAndDateLight(user_id, creationDate);
			
			if(numberSub > 0) {
				serviceSubscription.addNewMusicSubscription(user_id, creationDate, "subscription");
			}
			else {
				serviceSubscription.addNewMusicSubscription(user_id, creationDate, "booster");
				serviceSubscription.decrementBoosterByUserId(user_id);
			}
			
		}

		return ResponseEntity.status(HttpStatus.OK).body(music);
	}
	
	@RequestMapping(path = "/getFirstParametersMusic/{style_id}", method = RequestMethod.GET)
	public ResponseEntity<MusicParameters> getFirstParametersMusic( @PathVariable("style_id") long style_id) {
		
//		EnumMusicStyle st = EnumMusicStyle.getByName(style);
		StyleMusicFull styleMusicFull = this.serviceStyle.getByIdFull(style_id);
		RandomNameGenerator rnd = new RandomNameGenerator();
		MusicParameters m = new MusicParameters(styleMusicFull, rnd.next());
		
		return ResponseEntity.status(HttpStatus.OK).body(m);
	}
	
	@RequestMapping(path = "/changeMusicDetails", method = RequestMethod.POST)
	public boolean changeMusicDetails(@RequestBody MusicDetail musicDetail) {
		
		int ok = 0;
		
		ok = serviceMusic.changeMusicDetails(musicDetail);
		
		if(ok > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	@RequestMapping(path = "/like/{idMusic}/{idUser}/{idOwner}", method = RequestMethod.GET)
	public boolean like(@PathVariable("idMusic") long idMusic, @PathVariable("idUser") long idUser, @PathVariable("idOwner") long idOwner) {
		
		int ok = 0;
		
		ok = serviceMusic.likeMusicById(idMusic, idUser, idOwner);
		
		if(ok > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	@RequestMapping(path = "/dislike/{idMusic}/{idUser}/{idOwner}", method = RequestMethod.GET)
	public boolean dislike(@PathVariable("idMusic") long idMusic, @PathVariable("idUser") long idUser, @PathVariable("idOwner") long idOwner) {
		
		int ok = 0;
		
		ok = serviceMusic.dislikeMusicById(idMusic, idUser);
		
		if(ok > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	@RequestMapping(path = "/toggleVisible/{idMusic}/{visible}/{idUser}", method = RequestMethod.GET)
	public boolean toggleVisible(@PathVariable("idMusic") long idMusic, @PathVariable("visible") boolean visible, @PathVariable("idUser") long idUser) {
		
		int ok = 0;
		ok = serviceMusic.toggleVisibleMusicById(idMusic, visible);
		
		if(ok > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	@RequestMapping(path = "/toggleVisibleOrigin/{idMusic}/{visible}/{idUser}", method = RequestMethod.GET)
	public boolean toggleVisibleOrigin(@PathVariable("idMusic") long idMusic, @PathVariable("visible") boolean visible, @PathVariable("idUser") long idUser) {
		
		int ok = 0;
		ok = serviceMusic.toggleVisibleOriginMusicById(idMusic, visible);
		
		if(ok > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	@RequestMapping(path = "/toggleVisibleModified/{idMusic}/{visible}/{idUser}", method = RequestMethod.GET)
	public boolean toggleVisibleModified(@PathVariable("idMusic") long idMusic, @PathVariable("visible") boolean visible, @PathVariable("idUser") long idUser) {
		
		int ok = 0;
		ok = serviceMusic.toggleVisibleModifiedMusicById(idMusic, visible);
		
		if(ok > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	@RequestMapping(path = "/toggleVisibleVideo/{idMusic}/{visible}/{idUser}", method = RequestMethod.GET)
	public boolean toggleVisibleVideo(@PathVariable("idMusic") long idMusic, @PathVariable("visible") boolean visible, @PathVariable("idUser") long idUser) {
		
		int ok = 0;
		ok = serviceMusic.toggleVisibleVideoMusicById(idMusic, visible);
		
		if(ok > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	@RequestMapping(path = "/setVideoId/{idMusic}/{videoId}/{idUser}", method = RequestMethod.GET)
	public boolean setVideoId(@PathVariable("idMusic") long idMusic, @PathVariable("videoId") String videoId, @PathVariable("idUser") long idUser) {
		
		int ok = 0;
		ok = serviceMusic.setVideoIdMusicById(idMusic, videoId);
		
		if(ok > 0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	@RequestMapping(path = "/hasLike/{idMusic}/{idUser}", method = RequestMethod.GET)
	public boolean hasLike(@PathVariable("idMusic") long idMusic, @PathVariable("idUser") long idUser) {
		
		return serviceMusic.hasLike(idMusic, idUser);
	}
	
	@RequestMapping(path = "/deleteMusic/{idMusic}/{id}", method = RequestMethod.GET)
	public int deleteMusic( @PathVariable("idMusic") long idMusic, @PathVariable("id") long id) {
		
		int result = 0;

		result = serviceMusic.deleteMusicById(idMusic);
		return result;
	}
	
	@RequestMapping(path = "/getStyles", method = RequestMethod.GET)
	public ResponseEntity<List<String>> getStyles() {
		
		List<String> styles = EnumMusicStyle.getList();
		return ResponseEntity.status(HttpStatus.OK).body(styles);
	}
	
//	@RequestMapping(path = "/getImageOfStyle/{name}/{random}", method = RequestMethod.GET)
//	public String getImageOfStyle( @PathVariable("name") String name, @PathVariable("random") int random) throws IOException {
//		return Base64Utils.encodeToString(serviceMusic.getImageOfStyleAsBytes(name));
//	}

	@RequestMapping(path = "/getAllMusics/{id}/{page}", method = RequestMethod.POST)
	public List<MusicFileLight> getAllMusics(@PathVariable("id") long id, @PathVariable("page") int page, @RequestBody FilterMusic filterMusic) {
		return serviceMusic.getAllMusics(id, filterMusic, page);
	}
	
	@RequestMapping(path = "/getTopMusics", method = RequestMethod.GET)
	public List<MusicFileLight> getTopMusics() {
		return serviceMusic.getTopMusics();
	}
	
	@RequestMapping(path = "/getPublicMusics/{page}", method = RequestMethod.POST)
	public List<MusicFileLight> getPublicMusics(@PathVariable("page") int page, @RequestBody FilterMusic filterMusic) {
		return serviceMusic.getPublicMusics(filterMusic, page);
	}
	
	@RequestMapping(path = "/getPublicMusicsByUserId/{id}", method = RequestMethod.GET)
	public List<MusicFileLight> getPublicMusicsByUserId(@PathVariable("id") long id) {
		return serviceMusic.getPublicMusicsByUserId(id);
	}

	@RequestMapping(path = "/getMusicById/{id}", method = RequestMethod.GET)
	public MusicFile getMusicById(@PathVariable("id") long id) {
		return serviceMusic.getMusicById(id);
	}
	
	@RequestMapping(path="/getNumberLikesByMusicId/{music_id}", method = RequestMethod.GET)
	public int getNumberLikesByMusicId(@PathVariable("music_id") long music_id) {
		return serviceMusic.numberLikesByMusicId(music_id);
	}
}
