package com.example.musicBackend.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.musicBackend.model.MesureTempsFortFull;
import com.example.musicBackend.model.StyleCoefDegres;
import com.example.musicBackend.model.StyleGroupDrumsFull;
import com.example.musicBackend.model.StyleGroupDrumsInsFull;
import com.example.musicBackend.model.StyleGroupInstrumentsFull;
import com.example.musicBackend.model.StyleMusic;
import com.example.musicBackend.model.StyleMusicFull;
import com.example.musicBackend.service.ServiceStyle;
import com.example.musicBackend.service.ServiceUser;

@RestController
@RequestMapping(value = "/style")
public class StyleController {
	
	@Autowired
	ServiceStyle serviceStyle;
	
	@Autowired
	ServiceUser serviceUser;
	
	@RequestMapping(path = "/getAll/{published}", method = RequestMethod.GET)
	public ResponseEntity<List<StyleMusicFull>> getAll(@PathVariable("published") boolean published) {
		
		List<StyleMusicFull> allStyles = new ArrayList<StyleMusicFull>();
		allStyles = this.serviceStyle.getAllStylesMusic(published);
		return ResponseEntity.status(HttpStatus.OK).body(allStyles);
	}
	
	@RequestMapping(path = "/getListSoundFont", method = RequestMethod.GET)
	public ResponseEntity<List<String>> getListSoundFont() {
		
		List<String> allSoundFont = new ArrayList<String>();
		try {
			allSoundFont = this.serviceStyle.getListSoundFont();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.status(HttpStatus.OK).body(allSoundFont);
	}
	
	@RequestMapping(value = "/upload/{user_id}/{id}", method = RequestMethod.POST)
	public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file, @PathVariable("user_id") long user_id, @PathVariable("id") long id) throws IOException { //@PathVariable("username") String username, @PathVariable("id") long id, 
		
		String message = "";
		
		if(serviceUser.isAdmin(user_id)) {
			try {
				this.serviceStyle.upload(file.getBytes(), id);
	 
				message = "You successfully uploaded " + file.getOriginalFilename() + "!";
				return ResponseEntity.status(HttpStatus.OK).body(message);
			} catch (Exception e) {
				message = "FAIL to upload " + file.getOriginalFilename() + "!";
				return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
			}
		}
		else {
			message = "FAIL to upload " + file.getOriginalFilename() + "!";
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
		}

	}
	
	@RequestMapping(value = "/download/{id}/{random}", method = RequestMethod.GET)
	public String download(@PathVariable("id") long id, @PathVariable("random") int random)
	{
		StyleMusic style = serviceStyle.downloadAvatar(id);
	    return Base64Utils.encodeToString(style.getImage());
	}
	
	@RequestMapping(path = "/delete/{name}", method = RequestMethod.GET)
	public ResponseEntity<Integer> delete(@PathVariable("name") String name) {
		
		int result = 0;
		long id = this.serviceStyle.getStyleIdByName(name).getId();
		
		result = this.serviceStyle.removeStyleById(id);
		
		this.serviceStyle.removeAllStyleGroupInstrumentsByStyleId(id);
		this.serviceStyle.removeAllStyleGroupDrumsByStyleId(id);
		this.serviceStyle.removeAllStyleGroupDrumsInsByStyleId(id);
		this.serviceStyle.removeAllDrumsByStyleId(id);
		this.serviceStyle.removeAllInstrumentsByStyleId(id);
		this.serviceStyle.removeAllMesureTempsFortByStyleId(id);
		this.serviceStyle.removeAllStyleCoefDegresByStyleId(id);
		this.serviceStyle.removeAllChordDurationsByStyleId(id);
		this.serviceStyle.removeAllTonalitiesByStyleId(id);
		
		return ResponseEntity.status(HttpStatus.OK).body(result);
	}
	
	@RequestMapping(path = "/addStyle", method = RequestMethod.POST)
	public ResponseEntity<?> addStyle(@RequestBody StyleMusicFull styleMusicFull ) {
		
		long styleId = styleMusicFull.getId();
//		long styleName = this.serviceStyle.getStyleIdByName(styleMusicFull.getStyleName());
				
		if( styleId == 0) {
			styleId = this.serviceStyle.addNew(styleMusicFull);
			
			if(styleId>0) {
				this.serviceStyle.setAllTonalitiesByStyleId(styleMusicFull, styleId);
			}
		}
		else {
			this.serviceStyle.updateStyleMusic(styleMusicFull);
			this.serviceStyle.removeAllTonalitiesByStyleId(styleId);
			this.serviceStyle.setAllTonalitiesByStyleId(styleMusicFull, styleId);
		}
		
		if(styleId >0) {
			return ResponseEntity.status(HttpStatus.OK).body(true);
		}
		else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);
		}
		
	}
	
	@RequestMapping(path = "/addGroupInstruments", method = RequestMethod.POST)
	public ResponseEntity<Boolean> addGroupInstruments(@RequestBody StyleGroupInstrumentsFull styleGroupInstrumentsFull ) {
		
		long styleGroupId = styleGroupInstrumentsFull.getId();
				
		if(styleGroupId == 0) {
			styleGroupId = this.serviceStyle.addNewStyleGroupInstruments(styleGroupInstrumentsFull);
			
			this.serviceStyle.removeAllInstrumentsByStyleIdAndGroupId(styleGroupId, styleGroupInstrumentsFull.getId());
			this.serviceStyle.setAllInstrumentsByStyleIdAndGroupId(styleGroupInstrumentsFull, styleGroupInstrumentsFull.getStyleId(), styleGroupId);
			
		}
		else {
			this.serviceStyle.updateStyleGroupInstruments(styleGroupInstrumentsFull);
			this.serviceStyle.removeAllInstrumentsByStyleIdAndGroupId(styleGroupInstrumentsFull.getStyleId(), styleGroupInstrumentsFull.getId());
			this.serviceStyle.setAllInstrumentsByStyleIdAndGroupId(styleGroupInstrumentsFull, styleGroupInstrumentsFull.getStyleId(), styleGroupId);
		}
		
		if(styleGroupId > 0) {
			return ResponseEntity.status(HttpStatus.OK).body(true);
		}
		else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);
		}
	}
	
	@RequestMapping(path = "/addGroupDrums", method = RequestMethod.POST)
	public ResponseEntity<Boolean> addGroupDrums(@RequestBody StyleGroupDrumsFull styleGroupDrumsFull ) {
		
		long styleGroupId = styleGroupDrumsFull.getId();
				
		if(styleGroupId == 0) {
			styleGroupId = this.serviceStyle.addNewStyleGroupDrums(styleGroupDrumsFull);
			
//			this.serviceStyle.removeAllStyleGroupDrumsInsByGroupDrumsId(styleGroupId);
//			this.serviceStyle.removeAllDrumsByStyleIdAndGroupDrumsId(styleId, groupDrumsId)
//			this.serviceStyle.removeAllInstrumentsByStyleIdAndGroupId(styleGroupId, styleGroupInstrumentsFull.getId());
//			this.serviceStyle.setAllInstrumentsByStyleIdAndGroupId(styleGroupInstrumentsFull, styleGroupInstrumentsFull.getStyleId(), styleGroupId);
			
		}
		else {
			this.serviceStyle.updateStyleGroupDrums(styleGroupDrumsFull);
//			this.serviceStyle.removeAllInstrumentsByStyleIdAndGroupId(styleGroupInstrumentsFull.getStyleId(), styleGroupInstrumentsFull.getId());
//			this.serviceStyle.setAllInstrumentsByStyleIdAndGroupId(styleGroupInstrumentsFull, styleGroupInstrumentsFull.getStyleId(), styleGroupId);
		}
		
		if(styleGroupId > 0) {
			return ResponseEntity.status(HttpStatus.OK).body(true);
		}
		else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);
		}
	}
	
	@RequestMapping(path = "/addGroupDrumsIns", method = RequestMethod.POST)
	public ResponseEntity<Boolean> addGroupDrumsIns(@RequestBody StyleGroupDrumsInsFull styleGroupDrumsInsFull ) {
		
		long styleGroupDrumsInsId = styleGroupDrumsInsFull.getId();
				
		if(styleGroupDrumsInsId == 0) {
			styleGroupDrumsInsId = this.serviceStyle.addNewStyleGroupDrumsIns(styleGroupDrumsInsFull);
			
			this.serviceStyle.removeAllDrumsByStyleIdAndGroupDrumsIdAndGroupDrumsInsId(styleGroupDrumsInsFull.getStyleId(), styleGroupDrumsInsFull.getGroupDrumsId(), styleGroupDrumsInsFull.getId());
			this.serviceStyle.setAllDrumsInstrumentsByStyleIdAndGroupDrumsIdAndGroupDrumsInsId(styleGroupDrumsInsFull, styleGroupDrumsInsFull.getStyleId(), styleGroupDrumsInsFull.getGroupDrumsId(), styleGroupDrumsInsId);
			
		}
		else {
			this.serviceStyle.updateStyleGroupDrumsIns(styleGroupDrumsInsFull);
			this.serviceStyle.removeAllDrumsByStyleIdAndGroupDrumsIdAndGroupDrumsInsId(styleGroupDrumsInsFull.getStyleId(), styleGroupDrumsInsFull.getGroupDrumsId(), styleGroupDrumsInsFull.getId());
			this.serviceStyle.setAllDrumsInstrumentsByStyleIdAndGroupDrumsIdAndGroupDrumsInsId(styleGroupDrumsInsFull, styleGroupDrumsInsFull.getStyleId(), styleGroupDrumsInsFull.getGroupDrumsId(), styleGroupDrumsInsId);
		}
		
		if(styleGroupDrumsInsId > 0) {
			return ResponseEntity.status(HttpStatus.OK).body(true);
		}
		else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);
		}
	}
	
	@RequestMapping(path = "/deleteGroupInstruments/{group_id}/{style_id}", method = RequestMethod.GET)
	public ResponseEntity<Integer> deleteGroupInstruments(@PathVariable("group_id") long group_id, @PathVariable("style_id") long style_id) {
		
		int result = 0;
		
		result = this.serviceStyle.removeStyleGroupInstrumentsById(group_id);
		this.serviceStyle.removeAllInstrumentsByStyleIdAndGroupId(style_id, group_id);
		
		return ResponseEntity.status(HttpStatus.OK).body(result);
	}
	
	@RequestMapping(path = "/deleteGroupDrums/{group_drums_id}/{style_id}", method = RequestMethod.GET)
	public ResponseEntity<Integer> deleteGroupDrums(@PathVariable("group_drums_id") long group_drums_id, @PathVariable("style_id") long style_id) {
		
		int result = 0;
		
		result = this.serviceStyle.removeStyleGroupDrumsById(group_drums_id);
		this.serviceStyle.removeAllStyleGroupDrumsInsByGroupDrumsId(group_drums_id);
		this.serviceStyle.removeAllDrumsByStyleIdAndGroupDrumsId(style_id, group_drums_id);
		
		return ResponseEntity.status(HttpStatus.OK).body(result);
	}
	
	@RequestMapping(path = "/deleteGroupDrumsIns/{group_drums_ins_id}/{style_id}", method = RequestMethod.GET)
	public ResponseEntity<Integer> deleteGroupDrumsIns(@PathVariable("group_drums_ins_id") long group_drums_ins_id, @PathVariable("style_id") long style_id) {
		
		int result = 0;
		
		result = this.serviceStyle.removeStyleGroupDrumsInsById(group_drums_ins_id);
		this.serviceStyle.removeAllDrumsByStyleIdAndGroupDrumsInsId(style_id, group_drums_ins_id);
		
		return ResponseEntity.status(HttpStatus.OK).body(result);
	}
	
	@RequestMapping(path = "/addMesureTempsFort", method = RequestMethod.POST)
	public ResponseEntity<Boolean> addMesureTempsFort(@RequestBody MesureTempsFortFull mesureTempsFortFull ) {
		
		long mesureId = mesureTempsFortFull.getId();
				
		if(mesureId == 0) {
			mesureId = this.serviceStyle.addNewMesureTempsFort(mesureTempsFortFull);
			
			this.serviceStyle.removeAllTempsFortsByMesureId(mesureId);
			this.serviceStyle.setAllTempsFortsByMesureIdAndStyleId(mesureTempsFortFull, mesureId, mesureTempsFortFull.getStyleId());
		}
		else {
			this.serviceStyle.updateMesureTempsFort(mesureTempsFortFull);
			this.serviceStyle.removeAllTempsFortsByMesureId(mesureId);
			this.serviceStyle.setAllTempsFortsByMesureIdAndStyleId(mesureTempsFortFull, mesureId, mesureTempsFortFull.getStyleId());
		}
		
		if(mesureId > 0) {
			return ResponseEntity.status(HttpStatus.OK).body(true);
		}
		else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);
		}
	}
	
	@RequestMapping(path = "/deleteMesureTempsFort/{mesure_id}", method = RequestMethod.GET)
	public ResponseEntity<Integer> deleteMesureTempsFort(@PathVariable("mesure_id") long mesure_id) {
		
		int result = 0;
		
		result = this.serviceStyle.removeMesureTempsFortById(mesure_id);
		this.serviceStyle.removeAllTempsFortsByMesureId(mesure_id);
		
		return ResponseEntity.status(HttpStatus.OK).body(result);
	}
	
	@RequestMapping(path = "/addStyleCoefDegres", method = RequestMethod.POST)
	public ResponseEntity<Boolean> addStyleCoefDegres(@RequestBody StyleCoefDegres styleCoefDegres ) {
		
		long degreId = styleCoefDegres.getId();
				
		if(degreId == 0) {
			degreId = this.serviceStyle.addNewStyleCoefDegres(styleCoefDegres);
		}
		else {
			this.serviceStyle.updateStyleCoefDegres(styleCoefDegres);
		}
		
		if(degreId > 0) {
			return ResponseEntity.status(HttpStatus.OK).body(true);
		}
		else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(false);
		}
	}
	
	@RequestMapping(path = "/deleteStyleCoefDegres/{degre_id}", method = RequestMethod.GET)
	public ResponseEntity<Integer> deleteStyleCoefDegres(@PathVariable("degre_id") long degre_id) {
				
		int result = this.serviceStyle.removeStyleCoefDegresById(degre_id);
		
		return ResponseEntity.status(HttpStatus.OK).body(result);
	}

}
