package com.example.musicBackend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.musicBackend.model.Booster;
import com.example.musicBackend.model.Subscription;
import com.example.musicBackend.model.SubscriptionUser;
import com.example.musicBackend.service.ServiceSubscription;
import com.example.musicBackend.service.ServiceUser;

@RestController
@RequestMapping(value = "/subscription")
public class SubscriptionController {

	@Autowired
	ServiceSubscription serviceSubscription;
	
	@Autowired
	ServiceUser serviceUser;
	
	@RequestMapping(path = "/getAll", method = RequestMethod.GET)
	public ResponseEntity<List<Subscription>> getAll() {
		return ResponseEntity.ok(serviceSubscription.getAllSubscriptions());
	}
	
	@RequestMapping(path = "/getAllBooster", method = RequestMethod.GET)
	public ResponseEntity<List<Booster>> getAllBooster() {
		return ResponseEntity.ok(serviceSubscription.getAllBoosters());
	}
	
	@RequestMapping(path = "/getSubscriptionUserByUserId/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<SubscriptionUser> getSubscriptionUserByUserId(@PathVariable("user_id") long user_id) {
		return ResponseEntity.ok(serviceSubscription.getSubscriptionUserByUserId(user_id));
	}
	
	@RequestMapping(path = "/getSubscriptionOptionById/{id}", method = RequestMethod.GET)
	public ResponseEntity<Subscription> getSubscriptionOptionById(@PathVariable("id") long id) {
		return ResponseEntity.ok(serviceSubscription.getSubscriptionById(id));
	}
	
	@RequestMapping(path = "/getSubscriptionOptionIdByUserId/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<Long> getSubscriptionOptionIdByUserId(@PathVariable("user_id") long user_id) {
		return ResponseEntity.ok(serviceSubscription.getSubscriptionOptionIdByUserId(user_id));
	}
	
	@RequestMapping(path = "/getBooster/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<Booster> getBooster(@PathVariable("user_id") long user_id) {
		long boosterId = serviceUser.getBoosterIdByUserId(user_id);
		return ResponseEntity.ok(serviceSubscription.getBoosterById(boosterId));
	}
	
	@RequestMapping(path = "/getNumberMusicsBooster/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<Integer> getNumberMusicsBooster(@PathVariable("user_id") long user_id) {
		return ResponseEntity.ok(serviceSubscription.getNumberMusicsBoosterByUserId(user_id));
	}
	
	@RequestMapping(path = "/getNumberMusicsSubscription/{user_id}", method = RequestMethod.GET)
	public ResponseEntity<Integer> getNumberMusicsSubscription(@PathVariable("user_id") long user_id) {
		return ResponseEntity.ok(serviceSubscription.getNumberMusicsSubscriptionByUserId(user_id));
	}
	
	@RequestMapping(path = "/getOrderId", method = RequestMethod.GET)
	public ResponseEntity<Long> getOrderId() {
		return ResponseEntity.ok(serviceSubscription.getOrderId());
	}
}
