package com.example.musicBackend.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;

import com.example.musicBackend.config.JwtTokenUtil;
import com.example.musicBackend.model.AuthToken;
import com.example.musicBackend.model.Booster;
import com.example.musicBackend.model.Mail;
import com.example.musicBackend.model.Subscription;
import com.example.musicBackend.model.SubscriptionUser;
import com.example.musicBackend.model.User;
import com.example.musicBackend.model.UserFollowing;
import com.example.musicBackend.model.UserLogin;
import com.example.musicBackend.service.EmailService;
import com.example.musicBackend.service.PasswordGenerator;
import com.example.musicBackend.service.ServiceMusic;
import com.example.musicBackend.service.ServiceSubscription;
import com.example.musicBackend.service.ServiceUser;
import com.example.musicBackend.service.SessionIdentifierGenerator;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;

import static com.example.musicBackend.model.Constants.ACCESS_TOKEN_VALIDITY_SECONDS;
import static com.example.musicBackend.model.Constants.HEADER_STRING;
import static com.example.musicBackend.model.Constants.TOKEN_PREFIX;
import static com.example.musicBackend.model.Constants.ACCESS_TOKEN_REFRESH_VALIDITY_SECONDS;

@RestController
@RequestMapping(value = "/users")
public class UserController {
	
	/** Logger available to subclasses */
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	ServiceUser serviceUser;
	
	@Autowired
	ServiceMusic serviceMusic;
	
	@Autowired
	ServiceSubscription serviceSubscription;
	
	@Autowired
	EmailService emailService;
	
	@Autowired
	SessionIdentifierGenerator IdentifierGenerator;
	
	@Autowired
    private UserDetailsService userDetailsService;

    @Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	UserController() {}

    @RequestMapping(value = "/token", method = RequestMethod.POST)
    public ResponseEntity<?> token(@RequestBody UserLogin userLogin) {

    	Authentication authentication = null;
		try {
			String username = userLogin.getUsername();
			String password = userLogin.getPassword();
 
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
			authentication = this.authenticationManager.authenticate(token);

			SecurityContextHolder.getContext().setAuthentication(authentication);
 
			final User user = serviceUser.getByUsername(userLogin.getUsername());
			
			if(user.getEmailChecked()) {
				final String tokenOne = jwtTokenUtil.generateToken(user, ACCESS_TOKEN_VALIDITY_SECONDS);
		        final String refresh_token = jwtTokenUtil.generateToken(user, ACCESS_TOKEN_REFRESH_VALIDITY_SECONDS);
		        
		        AuthToken authToken = new AuthToken(tokenOne, refresh_token, System.currentTimeMillis() + ACCESS_TOKEN_VALIDITY_SECONDS*1000);
		        return ResponseEntity.ok(authToken);
			}
			else {
//				return new ResponseEntity<String>("email not verified", HttpStatus.FORBIDDEN);
				AuthToken authToken = new AuthToken("email", "error", System.currentTimeMillis() + ACCESS_TOKEN_VALIDITY_SECONDS*1000);
		        return ResponseEntity.ok(authToken);
			}
	        
	        
		} catch (BadCredentialsException bce) {
			return new ResponseEntity<AuthToken>(new AuthToken(), HttpStatus.UNPROCESSABLE_ENTITY);
 
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
        
    }
	
    @RequestMapping(value="/refreshtoken", method=RequestMethod.POST)
    public ResponseEntity<AuthToken> refreshToken(@RequestBody AuthToken authToken) {
        
		String username = null;
				
		if (authToken.getToken() != null && authToken.getRefresh_token() != null) {
            try {
//                username = jwtTokenUtil.getUsernameFromToken(authToken.getToken());
            	username = jwtTokenUtil.getUsernameFromToken(authToken.getRefresh_token());
                System.out.println("username = "+username);
            } catch (IllegalArgumentException e) {
                logger.error("an error occured during getting username from token", e);
            } catch (ExpiredJwtException e) {
                logger.warn("the token is expired and not valid anymore", e);
            } catch(SignatureException e){
                logger.error("Authentication Failed. Username or Password not valid.");
            }
        } else {
            logger.warn("couldn't find bearer string, will ignore the header");
        }
		
		if (username != null) {

			final User user = serviceUser.getByUsername(username);
            final String token = jwtTokenUtil.generateToken(user, ACCESS_TOKEN_VALIDITY_SECONDS);
            final String refresh_token = jwtTokenUtil.generateToken(user, ACCESS_TOKEN_REFRESH_VALIDITY_SECONDS);
            
            AuthToken newAuthToken = new AuthToken(token, refresh_token, System.currentTimeMillis() + ACCESS_TOKEN_VALIDITY_SECONDS*1000);
            return ResponseEntity.ok(newAuthToken);
        }
		
		return null;	
    }
	
	@RequestMapping(value = "/login/{username}", method = RequestMethod.GET)
    public ResponseEntity<User> login(@PathVariable("username") String username) {
		
		User user = serviceUser.getByUsername(username);
		user.setPassword("****");
        return ResponseEntity.ok(user);
    }
	
//	@RequestMapping(value = "/login/{username}/{password}", method = RequestMethod.GET)
//	public User login(@PathVariable("username") String username, @PathVariable("password") String password) {
//		return this.serviceUser.login(username, password);
//	}
	
	@RequestMapping(value = "/upload/{username}/{id}", method = RequestMethod.POST) ///{username}/{id}
//	@PostMapping("/upload/{username}/{id}")
	public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file, @PathVariable("username") String username, @PathVariable("id") long id) throws IOException { //@PathVariable("username") String username, @PathVariable("id") long id, 
		
		String message = "";
		
		try {
			this.serviceUser.upload(file.getOriginalFilename(), file.getBytes(), id, username);
 
			message = "You successfully uploaded " + file.getOriginalFilename() + "!";
			return ResponseEntity.status(HttpStatus.OK).body(message);
		} catch (Exception e) {
			message = "FAIL to upload " + file.getOriginalFilename() + "!";
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
		}

	}
	
	@RequestMapping(value = "/download/{id}/{random}", method = RequestMethod.GET)
	public String download(@PathVariable("id") long id, @PathVariable("random") int random)
	{
	    User user = serviceUser.downloadAvatar(id);

	    return Base64Utils.encodeToString(user.getAvatar());
//	    return Base64Utils.encode();
//	    return System.Convert.ToBase64String(image);
	}
	
	@RequestMapping(value = "/toggleFollow/{user_id}/{user_following_id}", method = RequestMethod.POST)
	public boolean toggleFollow(@PathVariable("user_id") long user_id, @PathVariable("user_following_id") long user_following_id) {
		return this.serviceUser.toggleFollow(user_id, user_following_id);
	}
	
	@RequestMapping(value = "/following/{user_id}/{user_following_id}", method = RequestMethod.GET)
	public boolean following(@PathVariable("user_id") long user_id, @PathVariable("user_following_id") long user_following_id) {
		return this.serviceUser.following(user_id, user_following_id);
	}
	
	@RequestMapping(value = "/numberFollowers/{user_id}", method = RequestMethod.GET)
	public int numberFollowers(@PathVariable("user_id") long user_id) {
		return this.serviceUser.numberFollowers(user_id);
	}
	
	@RequestMapping(value = "/numberLikes/{user_id}", method = RequestMethod.GET)
	public int numberLikes(@PathVariable("user_id") long user_id) {
		return this.serviceMusic.numberLikes(user_id);
	}
	
	@RequestMapping(value = "/numberCompositions/{user_id}", method = RequestMethod.GET)
	public int numberCompositions(@PathVariable("user_id") long user_id) {
		return this.serviceMusic.numberCompositions(user_id);
	}
	
	@RequestMapping(value = "/listUsersFollowed/{user_id}", method = RequestMethod.GET)
	public List<User> listUsersFollowed(@PathVariable("user_id") long user_id) {
		
		List<User> usersFollowed = new ArrayList<User>();
		
		List<UserFollowing> idOfUsersFollowed =  this.serviceUser.listUsersFollowed(user_id);
		
		for(UserFollowing uFollowing:idOfUsersFollowed) {
			User f = this.serviceUser.getUserById(uFollowing.getFollowing_user_id());
			f.setPassword("****");
			usersFollowed.add(f);
		}
		
		return usersFollowed;
	}

	
	
	@RequestMapping(value = "/validateUsername/{username}", method = RequestMethod.GET)
	public boolean validateUsername(@PathVariable("username") String username) {
		return this.serviceUser.validateUsername(username);
	}
	
	@RequestMapping(value = "/validateEmail", method = RequestMethod.POST)
	public boolean validateEmail(@RequestBody String email) {
		return this.serviceUser.validateEmail(email);
	}
	
	@RequestMapping(value = "/confirmEmail/{username}/{identifier}", method = RequestMethod.POST)
	public boolean confirmEmail(@PathVariable("username") String username, @PathVariable("identifier") String identifier) {
		User user = this.serviceUser.validateIdentifierMail(username, identifier);
		
		if(!user.getEmailChecked()) {
			if(this.serviceUser.setEmailChecked(user) > 0) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return true;
		}
	}

	@RequestMapping(value = "/getById/{id}", method = RequestMethod.GET)
	public User getUser(@PathVariable("id") Long id) {
		return this.serviceUser.getUserById(id);
	}
	
	@RequestMapping(value = "/getUserById/{id}", method = RequestMethod.GET)
	public User getUserById(@PathVariable("id") Long id) {
		
		User user = this.serviceUser.getUserById(id);
		user.setPassword("****");
		return user;
	}
	
	@RequestMapping(value = "/getByEmail/{email}", method = RequestMethod.GET)
	public User getUserByEmail(@PathVariable("email") String email) {
		return this.serviceUser.getUserByEmail(email);
	}
	
	@RequestMapping(value = "/idExists/{id}", method = RequestMethod.GET)
	public boolean checkUserExistById(@PathVariable("id") Long id) {
		return this.serviceUser.userExistById(id);
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<User> getAllUsers() {
		return this.serviceUser.getAllUsers();
	}
	
	@RequestMapping(value = "/deleteUserById/{id}", method = RequestMethod.POST)
	public ResponseEntity<Boolean> deleteUserById(@PathVariable("id") Long id) {
		
		int result = this.serviceUser.deleteUserById(id);
		
		if(result > 0) {
			this.serviceUser.deleteUserFollowingByUserId(id);
			this.serviceSubscription.deleteSubscriptionUserByUserId(id);
		}

		return ResponseEntity.ok(result>0);
	}

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public ResponseEntity<User> saveUser(@RequestBody User user) {
		
		String identifier = this.IdentifierGenerator.nextSessionId();
		user.setIdentifier(identifier);
		
		Booster freeBooster = this.serviceSubscription.getFreeBooster();
		user.setBoosterId(freeBooster.getId());
		long userId = this.serviceUser.addNew(user);
		this.serviceSubscription.setMusicBooster(userId, freeBooster.getNumberMusics());
		
		Subscription freeSubscription = this.serviceSubscription.getFreeSubscription();
		if(freeSubscription != null) {
			SubscriptionUser subscriptionUser = new SubscriptionUser(userId, freeSubscription.getId(), 0, 0, 1,false, false);
			long subscriptionUserId = this.serviceSubscription.addSubscriptionUser(subscriptionUser);
			this.serviceUser.setSubscriptionId(userId, subscriptionUserId);
		}
		
		Mail mail = new Mail();
        mail.setFrom("murmure.codemure@gmail.com");
        mail.setTo(user.getEmail());
        mail.setSubject("Welcome to Murmûre");
        
        String content = "Please click on this link to confirm your email adress:" ;
        
        Context context = new Context();
        context.setVariable("title","Welcome");        
        context.setVariable("subtitle", "We are pleased to have you with us");
        context.setVariable("content", content);
        context.setVariable("link", "http://murmure.tech/confirmation/"+user.getUsername()+"/"+identifier);
        
        try {
			emailService.sendMailConfirmation(mail, context);
		} catch (MessagingException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.ok(user);
	}
	
	@RequestMapping(value = "/resetPassword", method = RequestMethod.POST)
	public ResponseEntity<Boolean> resetPassword(@RequestBody String email) {

		User user = this.serviceUser.getUserByEmail(email);
		
		if(user != null) {
			String newPassword = new PasswordGenerator(8).getGenerated();
			this.serviceUser.setNewPassword(user, newPassword);

			Mail mail = new Mail();
            mail.setFrom("murmure.codemure@gmail.com");
            mail.setTo(user.getEmail());
            mail.setSubject("Murmûre the new password");
            
            String content = "Here is your new password : " + newPassword + ", don't forget to change it in your account";
            
            Context context = new Context();
            context.setVariable("title","New password");        
            context.setVariable("subtitle", "Don't forget it this time, I'll not be always there...");
            context.setVariable("content", content);
            
            try {
    			emailService.sendMessage(mail, context);
    		} catch (MessagingException | IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
			return ResponseEntity.ok(true);
		}
		else {
			ResponseEntity.notFound();
		}
		return null;
		
	}
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
	public ResponseEntity<Boolean> changePassword(@RequestBody UserLogin userLogin) {
		
		Authentication authentication = null;
		try {
			String username = userLogin.getUsername();
			String password = userLogin.getPassword();
 
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
			authentication = this.authenticationManager.authenticate(token);

			SecurityContextHolder.getContext().setAuthentication(authentication);
 
			final User user = serviceUser.getByUsername(userLogin.getUsername());
			if(user != null) {
				this.serviceUser.changePassword(user.getId(), userLogin.getNewPassword());
			}
			
	        return ResponseEntity.ok(true);
	        
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	@RequestMapping(value = "/updateUser", method = RequestMethod.POST)
	public User updateUser(@RequestBody User user) {
		
		User userBis = getUser(user.getId());
		String identifier;
		
		if(!userBis.getEmail().equals(user.getEmail())) {
			identifier = this.IdentifierGenerator.nextSessionId();
			user.setIdentifier(identifier);
			user.setEmailChecked(false);
			
			Mail mail = new Mail();
	        mail.setFrom("murmure.codemure@gmail.com");
	        mail.setTo(user.getEmail());
	        mail.setSubject("Welcome to Murmûre");
	        
	        String content = "Please click on this link to confirm your email adress: /n" ;
	        
	        Context context = new Context();
	        context.setVariable("title","Welcome");        
	        context.setVariable("subtitle", "E-mail check");
	        context.setVariable("content", content);
	        context.setVariable("link", "http://murmure.tech/confirmation/"+user.getUsername()+"/"+identifier);
	        
	        try {
				emailService.sendMailConfirmation(mail, context);
			} catch (MessagingException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		this.serviceUser.updateUser(user);
		
		user.setAvatar(userBis.getAvatar());
		user.setNameAvatar(userBis.getNameAvatar());
		
		return user;
		
	}

}