package com.example.musicBackend.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.musicBackend.youtube.Auth;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.common.collect.Lists;

@RestController
@RequestMapping(value = "/youtube")
public class YoutubeController {
	
	@RequestMapping(value = "/getAuth", method = RequestMethod.GET)
	public ResponseEntity<String> getAuth() {
		
		List<String> scopes = Lists.newArrayList("https://www.googleapis.com/auth/youtube");
		String token = null;
		
        try {
            // Authorize the request.
            Credential credential = Auth.authorize(scopes, "uploadvideo");
            token = credential.getAccessToken();
        }catch (GoogleJsonResponseException e) {
        	System.err.println("GoogleJsonResponseException code: " + e.getDetails().getCode() + " : "
        	+ e.getDetails().getMessage());
        	e.printStackTrace();
		} catch (IOException e) {
			System.err.println("IOException: " + e.getMessage());
			e.printStackTrace();
		} catch (Throwable t) {
			System.err.println("Throwable: " + t.getMessage());
			t.printStackTrace();
		}
		return ResponseEntity.ok(token);
	}
	
	@RequestMapping(value = "/getToken", method = RequestMethod.GET)
	public ResponseEntity<String> getToken() {
		
		String token = null;
//		Credential credential = null;
		
		token = Auth.getAccessToken();
		
//        try {
//            // Authorize the request.
//        	token = Auth.getAccessToken();
//            System.out.println(credential.getRefreshToken());
//            System.out.println(credential.toString());
////            System.out.println(credential.);
//            token = credential.getAccessToken();
//        }catch (GoogleJsonResponseException e) {
//        	System.err.println("GoogleJsonResponseException code: " + e.getDetails().getCode() + " : "
//        	+ e.getDetails().getMessage());
//        	e.printStackTrace();
//		} catch (IOException e) {
//			System.err.println("IOException: " + e.getMessage());
//			e.printStackTrace();
//		} catch (Throwable t) {
//			System.err.println("Throwable: " + t.getMessage());
//			t.printStackTrace();
//		}
        
        System.out.println("le token est: "+token);
		return ResponseEntity.ok(token);
	}
}
