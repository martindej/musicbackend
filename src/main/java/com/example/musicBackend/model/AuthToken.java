package com.example.musicBackend.model;

public class AuthToken {

    private String token;
    private String refresh_token;
    private long expiration_date;

    public AuthToken(){

    }

    public AuthToken(String token, String refresh_token, long expiration_date){
        this.token = token;
        this.refresh_token = refresh_token;
        this.expiration_date = expiration_date;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}

	public long getExpiration_date() {
		return expiration_date;
	}

	public void setExpiration_date(long expiration_date) {
		this.expiration_date = expiration_date;
	}

}

