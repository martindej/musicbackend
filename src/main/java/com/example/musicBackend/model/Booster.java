package com.example.musicBackend.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Booster implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "serial")
	private long id;
	private String name;
	private int numberMusics;
	private double price;
	private byte[] logo;
	private boolean generalSettings;
	private boolean advancedSettings;
	private boolean melodySettings;
	
	public Booster() {
		
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getNumberMusics() {
		return numberMusics;
	}
	public void setNumberMusics(int numberMusics) {
		this.numberMusics = numberMusics;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}
	
	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public boolean isGeneralSettings() {
		return generalSettings;
	}

	public void setGeneralSettings(boolean generalSettings) {
		this.generalSettings = generalSettings;
	}

	public boolean isMelodySettings() {
		return melodySettings;
	}

	public void setMelodySettings(boolean melodySettings) {
		this.melodySettings = melodySettings;
	}

	public boolean isAdvancedSettings() {
		return advancedSettings;
	}

	public void setAdvancedSettings(boolean advancedSettings) {
		this.advancedSettings = advancedSettings;
	}

}
