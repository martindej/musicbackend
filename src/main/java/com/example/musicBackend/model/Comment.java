package com.example.musicBackend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Comment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "serial")
	private long id;
	
	private long user_id;
	private String user_name;
	private long music_id;
	private long date;
	
	@Column(columnDefinition = "text")
	private String message;
	
	public Comment(long user_id, String user_name, long music_id, long date, String message, int numberLikes, int numberDislikes, String idOfUsersLikes, String idOfUsersDislikes) {
		this.setUser_id(user_id);
		this.setMusic_id(music_id);
		this.setDate(date);
		this.setMessage(message);
		this.user_name = user_name;
	}
	
	public Comment() {}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public long getMusic_id() {
		return music_id;
	}

	public void setMusic_id(long music_id) {
		this.music_id = music_id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public void setId(long id) {
		this.id = id;
		
	}
	
	public long getId() {
		return id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

}
