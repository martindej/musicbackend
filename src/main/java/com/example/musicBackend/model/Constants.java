package com.example.musicBackend.model;

public class Constants {

    public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 5*60*60;
    public static final long ACCESS_TOKEN_REFRESH_VALIDITY_SECONDS = 7*60*60;
    public static final String SIGNING_KEY = "murmure";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}
