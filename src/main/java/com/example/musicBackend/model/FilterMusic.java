package com.example.musicBackend.model;

import java.util.List;

public class FilterMusic {
	
	private String name;
	private boolean like;
	private boolean visibility;
	private List<String> style;
	private List<String> instruments;
	
	public FilterMusic(){}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isLike() {
		return like;
	}
	public void setLike(boolean like) {
		this.like = like;
	}
	public boolean isVisibility() {
		return visibility;
	}
	public void setVisibility(boolean visibility) {
		this.visibility = visibility;
	}
	public List<String> getStyle() {
		return style;
	}
	public void setStyle(List<String> style) {
		this.style = style;
	}
	public List<String> getInstruments() {
		return instruments;
	}
	public void setInstruments(List<String> instruments) {
		this.instruments = instruments;
	}
	

}
