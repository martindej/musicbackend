package com.example.musicBackend.model;

public class MessageMail {
	
	String name;
	String email;
	String message;
	
	public MessageMail() {
		
	}
	
	public MessageMail (String name, String email, String message) {
	  this.name = name;
	  this.email = email;
	  this.message = message;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getMessage() {
		return this.message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
}
