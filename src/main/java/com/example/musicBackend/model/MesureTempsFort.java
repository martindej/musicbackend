package com.example.musicBackend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MesureTempsFort {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "serial")
	private long id;
	
	private long styleId;
	private int mesure;
	private boolean etOu;
	
	public MesureTempsFort(long styleId, int mesure, boolean etOu) {
		this.setStyleId(styleId);
		this.setMesure(mesure);
		this.setEtOu(etOu);
	}

	public MesureTempsFort() {
		// TODO Auto-generated constructor stub
	}

	public long getStyleId() {
		return styleId;
	}

	public void setStyleId(long styleId) {
		this.styleId = styleId;
	}

	public int getMesure() {
		return mesure;
	}

	public void setMesure(int mesure) {
		this.mesure = mesure;
	}

	public boolean isEtOu() {
		return etOu;
	}

	public void setEtOu(boolean etOu) {
		this.etOu = etOu;
	}
	
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
