package com.example.musicBackend.model;

import java.util.List;

public class MesureTempsFortFull {
	
	private long id;
	
	private long styleId;
	private int mesure;
	private List<Integer> tempsFort;
	private boolean etOu;
	
	public MesureTempsFortFull(MesureTempsFort mesureTempsFort) {
		this.id = mesureTempsFort.getId();
		this.styleId = mesureTempsFort.getStyleId();
		this.mesure = mesureTempsFort.getMesure();
		this.etOu = mesureTempsFort.isEtOu();
	}

	public MesureTempsFortFull() {
		// TODO Auto-generated constructor stub
	}

	public long getStyleId() {
		return styleId;
	}

	public void setStyleId(long styleId) {
		this.styleId = styleId;
	}

	public int getMesure() {
		return mesure;
	}

	public void setMesure(int mesure) {
		this.mesure = mesure;
	}

	public List<Integer> getTempsFort() {
		return tempsFort;
	}

	public void setTempsFort(List<Integer> tempsFort) {
		this.tempsFort = tempsFort;
	}

	public boolean isEtOu() {
		return etOu;
	}

	public void setEtOu(boolean etOu) {
		this.etOu = etOu;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

}
