package com.example.musicBackend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MusicBooster {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "serial")
	private long id;
	private long user_id;
	private int number_musics;
	
	public MusicBooster() {}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public int getNumber_musics() {
		return number_musics;
	}

	public void setNumber_musics(int number_musics) {
		this.number_musics = number_musics;
	}

}
