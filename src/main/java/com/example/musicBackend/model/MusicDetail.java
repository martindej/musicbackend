package com.example.musicBackend.model;

public class MusicDetail {
	
	private String title;
	private String description;
	private long music_id;
	private long user_id;
	
	public MusicDetail(){}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getMusic_id() {
		return music_id;
	}

	public void setMusic_id(long music_id) {
		this.music_id = music_id;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

}
