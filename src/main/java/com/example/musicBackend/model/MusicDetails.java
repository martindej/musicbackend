package com.example.musicBackend.model;

public class MusicDetails {
	
	private String title;
	private String description;
	private long user_id;
	private long music_id;
	
	public MusicDetails() {}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public long getMusic_id() {
		return music_id;
	}

	public void setMusic_id(long music_id) {
		this.music_id = music_id;
	}

}
