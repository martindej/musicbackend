package com.example.musicBackend.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "musicFile")
public class MusicFile implements Serializable {
	
	private static final long serialVersionUID = 4638669994147392720L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "serial")
	private long id;
	private String name;
	
	@Lob
	private String description;
	
	private long user_id;
	private String user_name;
	
	private boolean visible_origin;
	private long user_id_origin;
	private String user_name_origin;
	
	private boolean visible_modified;
	private long user_id_modified;
	private String user_name_modified;
	
	private boolean visible_video;
	private long user_id_video;
	private String user_name_video;
		
//	@Column(name = "midiFile")
	private byte[] midiFile;
	
	@Lob
	private String musicXmlFile;
	
	@Lob
	private String musicXmlFileModified;
	
	@Lob
	@Column(columnDefinition = "bytea")
	private byte[] mp3File;
	
	private String videoId;
	private String instruments;
	private int tempo;
	private String style;
	
	private long creationDate;
	
	private String tonality;
	
	private int pictureNumber;
	
	private String soundFont;
	
	private long mother_id;

	public MusicFile(byte[] midiFile, String musicXmlFile, String musicXmlFileModified, String videoId, String instruments, int tempo, String style, long user_id, String user_name, long user_id_origin, long user_id_modified, long user_id_video, String user_name_origin, String name, String description, long creationDate, boolean visible_origin, String tonality, long mother_id, byte[] mp3File, int pictureNumber, String soundFont) {
		
		this.midiFile = midiFile;
		this.musicXmlFile = musicXmlFile;
		this.musicXmlFileModified = musicXmlFileModified;
		this.videoId = videoId;
		this.instruments = instruments;
		this.tempo = tempo;
		this.style = style;
		this.user_id = user_id;
		this.user_name = user_name;
		this.user_id_origin = user_id_origin;
		this.user_id_modified = user_id_modified;
		this.user_id_video = user_id_video;
		this.user_name_origin = user_name_origin;
		this.name = name;
		this.description = description;
		this.creationDate = creationDate;
		this.visible_origin = visible_origin;
		this.tonality = tonality;
		this.mother_id = mother_id;
		this.mp3File = mp3File;
		this.pictureNumber = pictureNumber;
		this.soundFont = soundFont;
	}
	
	public MusicFile() {
		// TODO Auto-generated constructor stub
	}

//	public byte[] getMidiFile() {
//		return this.midiFile;
//	}
	
	public String getMusicXmlFile() {
		return this.musicXmlFile;
	}
	
//	public File getWavFile() {
//
//		Resource resource = new ClassPathResource("/soundfonts/timbres.sf2");
//		
//	    try {
//	        File dbAsStream = resource.getFile();
//	        System.out.println("file opened");
//	        MidiToWav wav = new MidiToWav(new File("essai.mid"), dbAsStream);
//	        System.out.println("successfully music written");
//			File wavFile = wav.getWavFile();
//			System.out.println("file gotten");
//			return wavFile;
//			
//	    } catch (IOException e) {
//	        e.printStackTrace();
//	    }
//		
//	    return null;
//	}
	
//	public void setMidiFile(byte[] midiFile) {
//		this.midiFile = midiFile;
//	}
	
	public void setMusicXmlFile(String musicXmlFile) {
		this.musicXmlFile = musicXmlFile;
	}

	public void setId(long l) {
		this.id = l;
	}

	public long getId() {
		return this.id;
	}

	public String getMusicXmlFileModified() {
		return musicXmlFileModified;
	}

	public void setMusicXmlFileModified(String musicXmlFileModified) {
		this.musicXmlFileModified = musicXmlFileModified;
	}

	public String getInstruments() {
		return instruments;
	}

	public void setInstruments(String instruments) {
		this.instruments = instruments;
	}

	public int getTempo() {
		return tempo;
	}

	public void setTempo(int tempo) {
		this.tempo = tempo;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getMidiFile() {
		return midiFile;
	}

	public void setMidiFile(byte[] midiFile) {
		this.midiFile = midiFile;
	}
	
	public byte[] getMp3File() {
		return mp3File;
	}

	public void setMp3File(byte[] mp3File) {
		this.mp3File = mp3File;
	}

	public long getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(long creationDate) {
		this.creationDate = creationDate;
	}

//	public String getIdOfUserLikes() {
//		return idOfUserLikes;
//	}
//
//	public void setIdOfUserlikes(String idOfUserLikes) {
//		this.idOfUserLikes = idOfUserLikes;
//	}

	public String getTonality() {
		return tonality;
	}

	public void setTonality(String tonality) {
		this.tonality = tonality;
	}

//	public int getLikes() {
//		return likes;
//	}
//
//	public void setLikes(int likes) {
//		this.likes = likes;
//	}

	public int getPictureNumber() {
		return pictureNumber;
	}

	public void setPictureNumber(int pictureNumber) {
		this.pictureNumber = pictureNumber;
	}

	public long getUser_id_origin() {
		return user_id_origin;
	}

	public void setUser_id_origin(long user_id_origin) {
		this.user_id_origin = user_id_origin;
	}

	public String getUser_name_origin() {
		return user_name_origin;
	}

	public void setUser_name_origin(String user_name_origin) {
		this.user_name_origin = user_name_origin;
	}

	public long getUser_id_modified() {
		return user_id_modified;
	}

	public void setUser_id_modified(long user_id_modified) {
		this.user_id_modified = user_id_modified;
	}

	public String getUser_name_modified() {
		return user_name_modified;
	}

	public void setUser_name_modified(String user_name_modified) {
		this.user_name_modified = user_name_modified;
	}

	public long getUser_id_video() {
		return user_id_video;
	}

	public void setUser_id_video(long user_id_video) {
		this.user_id_video = user_id_video;
	}

	public String getUser_name_video() {
		return user_name_video;
	}

	public void setUser_name_video(String user_name_video) {
		this.user_name_video = user_name_video;
	}

	public boolean isVisible_origin() {
		return visible_origin;
	}

	public void setVisible_origin(boolean visible_origin) {
		this.visible_origin = visible_origin;
	}

	public boolean isVisible_modified() {
		return visible_modified;
	}

	public void setVisible_modified(boolean visible_modified) {
		this.visible_modified = visible_modified;
	}

	public boolean isVisible_video() {
		return visible_video;
	}

	public void setVisible_video(boolean visible_video) {
		this.visible_video = visible_video;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public long getMother_id() {
		return mother_id;
	}

	public void setMother_id(long mother_id) {
		this.mother_id = mother_id;
	}

	public String getVideoId() {
		return videoId;
	}

	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}

	public String getSoundFont() {
		return soundFont;
	}

	public void setSoundFont(String soundFont) {
		this.soundFont = soundFont;
	}

}
