package com.example.musicBackend.model;

public class MusicFileLight {

	private long id;
	private String name;
	private String description;
	
	private long user_id;
	private String user_name;
	
	private boolean visible_origin;
	private long user_id_origin;
	private String user_name_origin;
	
	private boolean visible_modified;
	private long user_id_modified;
	private String user_name_modified;
	
	private boolean visible_video;
	private long user_id_video;
	private String user_name_video;

	private String videoId;
	private String instruments;
	private int tempo;
	private String style;
	
	private long creationDate;
	
	private String tonality;
	
	private int pictureNumber;
	
	private long mother_id;

	public MusicFileLight() {
		// TODO Auto-generated constructor stub
	}
	
	public MusicFileLight(MusicFile musicFile) {
		this.id = musicFile.getId();
		this.setVideoId(musicFile.getVideoId());
		this.instruments = musicFile.getInstruments();
		this.tempo = musicFile.getTempo();
		this.style = musicFile.getStyle();
		this.user_id = musicFile.getUser_id();
		this.user_name = musicFile.getUser_name();
		this.user_id_origin = musicFile.getUser_id_origin();
		this.user_id_modified = musicFile.getUser_id_modified();
		this.user_id_video = musicFile.getUser_id_video();
		this.user_name_origin = musicFile.getUser_name_origin();
		this.name = musicFile.getName();
		this.description = musicFile.getDescription();
		this.creationDate = musicFile.getCreationDate();
		this.visible_origin = musicFile.isVisible_origin();
		this.visible_modified = musicFile.isVisible_modified();
		this.visible_video = musicFile.isVisible_video();
		this.tonality = musicFile.getTonality();
		this.mother_id = musicFile.getMother_id();
		this.pictureNumber = musicFile.getPictureNumber();
	}

	public void setId(long l) {
		this.id = l;
	}

	public long getId() {
		return this.id;
	}

	public String getInstruments() {
		return instruments;
	}

	public void setInstruments(String instruments) {
		this.instruments = instruments;
	}

	public int getTempo() {
		return tempo;
	}

	public void setTempo(int tempo) {
		this.tempo = tempo;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(long creationDate) {
		this.creationDate = creationDate;
	}

	public String getTonality() {
		return tonality;
	}

	public void setTonality(String tonality) {
		this.tonality = tonality;
	}
	
	public int getPictureNumber() {
		return pictureNumber;
	}

	public void setPictureNumber(int pictureNumber) {
		this.pictureNumber = pictureNumber;
	}

	public long getUser_id_origin() {
		return user_id_origin;
	}

	public void setUser_id_origin(long user_id_origin) {
		this.user_id_origin = user_id_origin;
	}

	public String getUser_name_origin() {
		return user_name_origin;
	}

	public void setUser_name_origin(String user_name_origin) {
		this.user_name_origin = user_name_origin;
	}

	public long getUser_id_modified() {
		return user_id_modified;
	}

	public void setUser_id_modified(long user_id_modified) {
		this.user_id_modified = user_id_modified;
	}

	public String getUser_name_modified() {
		return user_name_modified;
	}

	public void setUser_name_modified(String user_name_modified) {
		this.user_name_modified = user_name_modified;
	}

	public long getUser_id_video() {
		return user_id_video;
	}

	public void setUser_id_video(long user_id_video) {
		this.user_id_video = user_id_video;
	}

	public String getUser_name_video() {
		return user_name_video;
	}

	public void setUser_name_video(String user_name_video) {
		this.user_name_video = user_name_video;
	}

	public boolean isVisible_origin() {
		return visible_origin;
	}

	public void setVisible_origin(boolean visible_origin) {
		this.visible_origin = visible_origin;
	}

	public boolean isVisible_modified() {
		return visible_modified;
	}

	public void setVisible_modified(boolean visible_modified) {
		this.visible_modified = visible_modified;
	}

	public boolean isVisible_video() {
		return visible_video;
	}

	public void setVisible_video(boolean visible_video) {
		this.visible_video = visible_video;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public long getMother_id() {
		return mother_id;
	}

	public void setMother_id(long mother_id) {
		this.mother_id = mother_id;
	}

	public String getVideoId() {
		return videoId;
	}

	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}

}
