package com.example.musicBackend.model;

import java.util.ArrayList;
import java.util.List;

import enums.EnumInstruments;
import musicRandom.Music;
import styles.Style;
import styles.StyleGroupDrums;
import styles.StyleGroupDrumsIns;
import styles.StyleGroupIns;
import styles.StyleCoefDegres;
import styles.MesureTempsFort;

public class MusicParameters {
	
	private int tempo;
	private int tempoMin;
	private int tempoMax;
	
	private int swing;
	private int swingMin;
	private int swingMax;
	
	private String tonality;
	private boolean percussion;
	
	private Style style;
	private List<Integer> listInstruments;
	private String musicTitle;
	
	public MusicParameters(StyleMusicFull styleMusicFull,String musicTitle) {
		
		List<StyleGroupIns> styleGroupIns = new ArrayList<StyleGroupIns>();
		List<MesureTempsFort> mesureTempsForts = new ArrayList<MesureTempsFort>();
		List<StyleCoefDegres> styleCoefDegres = new ArrayList<StyleCoefDegres>();
		List<StyleGroupDrums> styleGroupDrums = new ArrayList<StyleGroupDrums>();
		List<StyleGroupDrumsIns> styleGroupDrumsIns = new ArrayList<StyleGroupDrumsIns>();
		
		for(StyleGroupInstrumentsFull sgIns:styleMusicFull.getStyleGroupIns()) {
			
			styleGroupIns.add(new StyleGroupIns(
					sgIns.getId(),
					sgIns.getName(),
					sgIns.getMotherId(),
					sgIns.getInstruments(),
					sgIns.getPourcentagePlayingMin(),
					sgIns.getPourcentagePlayingMax(), 
					sgIns.getBalanceBassMelodyMin(), 
					sgIns.getBalanceBassMelodyMax(), 
					sgIns.getBalanceMelodyMelodyBisMin(), 
					sgIns.getBalanceMelodyMelodyBisMax(), 
					sgIns.getgBetweenNotesMin(),
					sgIns.getgBetweenNotesMax(),
					sgIns.getDispersionNotesMin(),
					sgIns.getDispersionNotesMax(),
					sgIns.getPourcentageSilMin(),
					sgIns.getPourcentageSilMax(),
					sgIns.getSpeedMax(),
					sgIns.getSpeedMin(),
					sgIns.getpSyncopesMin(),
					sgIns.getpSyncopesMax(),
					sgIns.getpSyncopettesMin(),
					sgIns.getpSyncopettesMax(),
					sgIns.getpSyncopesTempsFortsMin(),
					sgIns.getpSyncopesTempsFortsMax(),
					sgIns.getpContreTempsMin(),
					sgIns.getpContreTempsMax(),
					sgIns.getpDissonanceMin(),
					sgIns.getpDissonanceMax(),
					sgIns.getpIrregularitesMin(),
					sgIns.getpIrregularitesMax(),
					sgIns.getbBinaireTernaireMin(),
					sgIns.getbBinaireTernaireMax(),
					sgIns.getpChanceSupSameRythmMin(),
					sgIns.getpChanceSupSameRythmMax(),
					sgIns.getNumberNotesSimultaneouslyMin(),
					sgIns.getNumberNotesSimultaneouslyMax(),
					sgIns.getProbabilityRythmMin(),
					sgIns.getProbabilityRythmMax(),
					sgIns.getNumberRythmMin(),
					sgIns.getNumberRythmMax(),
					sgIns.getWeightRythmMin(),
					sgIns.getWeightRythmMax(),
					sgIns.getpChanceSameNoteInMotifMin(),
					sgIns.getpChanceSameNoteInMotifMax(),
					sgIns.getpChanceStartByTonaleAsBassMin(),
					sgIns.getpChanceStartByTonaleAsBassMax(),
					sgIns.getpChanceStayAroundNoteRefMin(),
					sgIns.getpChanceStayAroundNoteRefMax(),
					sgIns.getpChancePlayChordMin(),
					sgIns.getpChancePlayChordMax()));

		}
		this.listInstruments = new ArrayList<Integer>();
		
		for(MesureTempsFortFull mTF:styleMusicFull.getMesureTempsFort()) {
			
			mesureTempsForts.add(new MesureTempsFort(
					mTF.getId(),
					mTF.getStyleId(),
					mTF.getMesure(),
					mTF.getTempsFort(),
					mTF.isEtOu()));

		}
		
		for(com.example.musicBackend.model.StyleCoefDegres sCD:styleMusicFull.getStyleCoefDegres()) {
			
			styleCoefDegres.add(new StyleCoefDegres(
					sCD.getC11(),
					sCD.getC12(),
					sCD.getC13(),
					sCD.getC14(),
					sCD.getC15(),
					sCD.getC16(),
					sCD.getC17(),
					
					sCD.getC21(),
					sCD.getC22(),
					sCD.getC23(),
					sCD.getC24(),
					sCD.getC25(),
					sCD.getC26(),
					sCD.getC27(),
					
					sCD.getC31(),
					sCD.getC32(),
					sCD.getC33(),
					sCD.getC34(),
					sCD.getC35(),
					sCD.getC36(),
					sCD.getC37(),
					
					sCD.getC41(),
					sCD.getC42(),
					sCD.getC43(),
					sCD.getC44(),
					sCD.getC45(),
					sCD.getC46(),
					sCD.getC47(),
					
					sCD.getC51(),
					sCD.getC52(),
					sCD.getC53(),
					sCD.getC54(),
					sCD.getC55(),
					sCD.getC56(),
					sCD.getC57(),
					
					sCD.getC61(),
					sCD.getC62(),
					sCD.getC63(),
					sCD.getC64(),
					sCD.getC65(),
					sCD.getC66(),
					sCD.getC67(),
					
					sCD.getC71(),
					sCD.getC72(),
					sCD.getC73(),
					sCD.getC74(),
					sCD.getC75(),
					sCD.getC76(),
					sCD.getC77(),
					sCD.getStyleId(),
					sCD.getCoefDegre()
					));

		}
				
		for(StyleGroupDrumsFull sgIns:styleMusicFull.getStyleGroupDrums()) {
			
			styleGroupDrumsIns = new ArrayList<StyleGroupDrumsIns>();
			
			for(StyleGroupDrumsInsFull sgDIs:sgIns.getDrumsIns()) {
				styleGroupDrumsIns.add(new StyleGroupDrumsIns(
						sgDIs.getId(),
						sgDIs.getStyleId(),
						sgDIs.getGroupDrumsId(),
						sgDIs.getName(),
						sgDIs.getRhythm(),
						sgDIs.getPourcentagePlayingTempsFortMin(),
						sgDIs.getPourcentagePlayingTempsFortMax(),
						sgDIs.getPourcentagePlayingTempsFaibleMin(),
						sgDIs.getPourcentagePlayingTempsFaibleMax(),
						sgDIs.getPourcentagePlayingTemps1Min(),
						sgDIs.getPourcentagePlayingTemps1Max(),
						sgDIs.getPourcentagePlayingTemps2Min(),
						sgDIs.getPourcentagePlayingTemps2Max(),
						sgDIs.getPourcentagePlayingTemps3Min(),
						sgDIs.getPourcentagePlayingTemps3Max(),
						sgDIs.getPourcentagePlayingTemps4Min(),
						sgDIs.getPourcentagePlayingTemps4Max(),
						sgDIs.getInstruments()));
			}
			
			styleGroupDrums.add(new StyleGroupDrums(
					sgIns.getId(),
					sgIns.getStyleId(),
					sgIns.getName(),
					sgIns.getRhythm(),
					sgIns.getPourcentagePlayingTempsFortMin(),
					sgIns.getPourcentagePlayingTempsFortMax(),
					sgIns.getPourcentagePlayingTempsFaibleMin(),
					sgIns.getPourcentagePlayingTempsFaibleMax(),
					sgIns.getPourcentagePlayingTemps1Min(),
					sgIns.getPourcentagePlayingTemps1Max(),
					sgIns.getPourcentagePlayingTemps2Min(),
					sgIns.getPourcentagePlayingTemps2Max(),
					sgIns.getPourcentagePlayingTemps3Min(),
					sgIns.getPourcentagePlayingTemps3Max(),
					sgIns.getPourcentagePlayingTemps4Min(),
					sgIns.getPourcentagePlayingTemps4Max(),
					styleGroupDrumsIns));
		}

		this.style = new Style(styleMusicFull.getStyleName(),
				styleMusicFull.getAvailableTon(),
				0,
				styleMusicFull.getRythmeMax(),
				styleMusicFull.getRythmeMin(), 
				styleMusicFull.getnInstrumentMin(), 
				styleMusicFull.getnInstrumentMax(),
				styleMusicFull.getnPercussionMin(), 
				styleMusicFull.getnPercussionMax(),
				styleMusicFull.getComplexityChordMin(),
				styleMusicFull.getComplexityChordMax(),
				styleMusicFull.getNumberChordMin(),
				styleMusicFull.getNumberChordMax(),
				styleCoefDegres,
				mesureTempsForts,
				styleGroupIns,
				styleGroupDrums);

		Music music = new Music(this.style);
		this.tempoMin = this.style.getRythmeMin();
		this.tempoMax = this.style.getRythmeMax();
		this.tempo = music.getRythme();
		this.percussion = music.getNumberBat() > 0;
		
		this.tonality = music.getTonality().getEngName();
		
		this.getSwingValues(styleMusicFull);
		this.setMusicTitle(musicTitle);
		
		for(EnumInstruments ins:music.getListEnumInstruments().keySet()) {
			if(!this.listInstruments.contains(ins.getNnumInstrument()) && !ins.equals(EnumInstruments.PERCUSSIONS)) {
				this.listInstruments.add(ins.getNnumInstrument());
			}
		}
		
	}
	
	private void getSwingValues(StyleMusicFull styleMusicFull) {
		
		int swingMinLocal = 0, swingMaxLocal = 0, swingLocal = 0;
		
		for(StyleGroupInstrumentsFull sgIns:styleMusicFull.getStyleGroupIns()) {
			
			swingMinLocal = swingMinLocal+ sgIns.getPourcentageSilMin() + sgIns.getpSyncopesMin() + sgIns.getpSyncopettesMin()
							+ sgIns.getpSyncopesTempsFortsMin() + sgIns.getpContreTempsMin() + sgIns.getpDissonanceMin()
							+ sgIns.getpIrregularitesMin() + sgIns.getpChanceSupSameRythmMin();
			
			swingMaxLocal = swingMaxLocal + sgIns.getPourcentageSilMax() + sgIns.getpSyncopesMax() + sgIns.getpSyncopettesMax()
							+ sgIns.getpSyncopesTempsFortsMax() + sgIns.getpContreTempsMax() + sgIns.getpDissonanceMax()
							+ sgIns.getpIrregularitesMax() + sgIns.getpChanceSupSameRythmMax();
			
		}
		
		for(StyleGroupIns sgIns: this.style.getStyleGroupInstruments()) {
			swingLocal = swingLocal + sgIns.getPourcentageSil() + sgIns.getpSyncopes() + sgIns.getpSyncopettes()
							+ sgIns.getpSyncopesTempsForts() + sgIns.getpContreTemps() + sgIns.getpDissonance()
							+ sgIns.getpIrregularites() + sgIns.getpChanceSupSameRythm();
		}
		
		this.swing = Math.round(swingLocal/(this.style.getStyleGroupInstruments().size() * 8));
		this.swingMin = Math.round(swingMinLocal/(styleMusicFull.getStyleGroupIns().size() * 8));
		this.swingMax = Math.round(swingMaxLocal/(styleMusicFull.getStyleGroupIns().size() * 8));
	}

	public MusicParameters() {
		// TODO Auto-generated constructor stub
	}

	public int getTempo() {
		return tempo;
	}

	public void setTempo(int tempo) {
		this.tempo = tempo;
	}

	public Style getStyle() {
		return style;
	}

	public void setStyle(Style style) {
		this.style = style;
	}

	public List<Integer> getListInstruments() {
		return listInstruments;
	}

	public void setListInstruments(List<Integer> listInstruments) {
		this.listInstruments = listInstruments;
	}

	public String getMusicTitle() {
		return musicTitle;
	}

	public void setMusicTitle(String musicTitle) {
		this.musicTitle = musicTitle;
	}

	public int getTempoMin() {
		return tempoMin;
	}

	public void setTempoMin(int tempoMin) {
		this.tempoMin = tempoMin;
	}

	public int getTempoMax() {
		return tempoMax;
	}

	public void setTempoMax(int tempoMax) {
		this.tempoMax = tempoMax;
	}

	public int getSwing() {
		return swing;
	}

	public void setSwing(int swing) {
		this.swing = swing;
	}

	public int getSwingMin() {
		return swingMin;
	}

	public void setSwingMin(int swingMin) {
		this.swingMin = swingMin;
	}

	public int getSwingMax() {
		return swingMax;
	}

	public void setSwingMax(int swingMax) {
		this.swingMax = swingMax;
	}

	public String getTonality() {
		return tonality;
	}

	public void setTonality(String tonality) {
		this.tonality = tonality;
	}

	public boolean isPercussion() {
		return percussion;
	}

	public void setPercussion(boolean percussion) {
		this.percussion = percussion;
	}

}
