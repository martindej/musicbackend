package com.example.musicBackend.model;

import java.util.List;

public class MusicParametersLight {
	
	private int tempo;
	private int swing;
	private long style;
	private List<Integer> listInstruments;
	private String musicTitle;
	private int idImage;
	private String tonality;
	private boolean percussions;

	public MusicParametersLight() {
		// TODO Auto-generated constructor stub
	}

	public int getTempo() {
		return tempo;
	}

	public void setTempo(int tempo) {
		this.tempo = tempo;
	}

	public long getStyle() {
		return style;
	}

	public void setStyle(long style) {
		this.style = style;
	}

	public List<Integer> getListInstruments() {
		return listInstruments;
	}

	public void setListInstruments(List<Integer> listInstruments) {
		this.listInstruments = listInstruments;
	}

	public String getMusicTitle() {
		return musicTitle;
	}

	public void setMusicTitle(String musicTitle) {
		this.musicTitle = musicTitle;
	}

	public int getIdImage() {
		return idImage;
	}

	public void setIdImage(int idImage) {
		this.idImage = idImage;
	}

	public int getSwing() {
		return swing;
	}

	public void setSwing(int swing) {
		this.swing = swing;
	}

	public String getTonality() {
		return this.tonality;
	}

	public void setTonality(String tonality) {
		this.tonality = tonality;
	}

	public boolean isPercussions() {
		return percussions;
	}

	public void setPercussions(boolean percussions) {
		this.percussions = percussions;
	}

}

