package com.example.musicBackend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class StyleChordDuration {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "serial")
	private long id;
	
	private long styleId;
	private String chordDuration;
	
	public StyleChordDuration(long styleId, String chordDuration) {
		this.setChordDuration(chordDuration);
		this.setStyleId(styleId);
	}

	public String getChordDuration() {
		return chordDuration;
	}

	public void setChordDuration(String chordDuration) {
		this.chordDuration = chordDuration;
	}
	
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public long getStyleId() {
		return styleId;
	}

	public void setStyleId(long styleId) {
		this.styleId = styleId;
	}

}
