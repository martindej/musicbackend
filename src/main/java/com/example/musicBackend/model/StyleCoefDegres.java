package com.example.musicBackend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class StyleCoefDegres {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "serial")
	private long id;
	
	private int c11;
	private int c12;
	private int c13;
	private int c14;
	private int c15;
	private int c16;
	private int c17;
	
	private int c21;
	private int c22;
	private int c23;
	private int c24;
	private int c25;
	private int c26;
	private int c27;
	
	private int c31;
	private int c32;
	private int c33;
	private int c34;
	private int c35;
	private int c36;
	private int c37;
	
	private int c41;
	private int c42;
	private int c43;
	private int c44;
	private int c45;
	private int c46;
	private int c47;
	
	private int c51;
	private int c52;
	private int c53;
	private int c54;
	private int c55;
	private int c56;
	private int c57;
	
	private int c61;
	private int c62;
	private int c63;
	private int c64;
	private int c65;
	private int c66;
	private int c67;
	
	private int c71;
	private int c72;
	private int c73;
	private int c74;
	private int c75;
	private int c76;
	private int c77;
	
	private long styleId;
	private String coefDegre;
	
	public StyleCoefDegres(int c11, int c12, int c13, int c14, int c15, int c16, int c17, int c21, int c22, int c23,
			int c24, int c25, int c26, int c27, int c31, int c32, int c33, int c34, int c35, int c36, int c37, int c41,
			int c42, int c43, int c44, int c45, int c46, int c47, int c51, int c52, int c53, int c54, int c55, int c56,
			int c57, int c61, int c62, int c63, int c64, int c65, int c66, int c67, int c71, int c72, int c73, int c74,
			int c75, int c76, int c77, long styleId, String coefDegre) {
		super();
		this.c11 = c11;
		this.c12 = c12;
		this.c13 = c13;
		this.c14 = c14;
		this.c15 = c15;
		this.c16 = c16;
		this.c17 = c17;
		this.c21 = c21;
		this.c22 = c22;
		this.c23 = c23;
		this.c24 = c24;
		this.c25 = c25;
		this.c26 = c26;
		this.c27 = c27;
		this.c31 = c31;
		this.c32 = c32;
		this.c33 = c33;
		this.c34 = c34;
		this.c35 = c35;
		this.c36 = c36;
		this.c37 = c37;
		this.c41 = c41;
		this.c42 = c42;
		this.c43 = c43;
		this.c44 = c44;
		this.c45 = c45;
		this.c46 = c46;
		this.c47 = c47;
		this.c51 = c51;
		this.c52 = c52;
		this.c53 = c53;
		this.c54 = c54;
		this.c55 = c55;
		this.c56 = c56;
		this.c57 = c57;
		this.c61 = c61;
		this.c62 = c62;
		this.c63 = c63;
		this.c64 = c64;
		this.c65 = c65;
		this.c66 = c66;
		this.c67 = c67;
		this.c71 = c71;
		this.c72 = c72;
		this.c73 = c73;
		this.c74 = c74;
		this.c75 = c75;
		this.c76 = c76;
		this.c77 = c77;
		this.styleId = styleId;
		this.coefDegre = coefDegre;
	}

	public StyleCoefDegres() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getC34() {
		return c34;
	}

	public void setC34(int c34) {
		this.c34 = c34;
	}

	public int getC35() {
		return c35;
	}

	public void setC35(int c35) {
		this.c35 = c35;
	}

	public int getC36() {
		return c36;
	}

	public void setC36(int c36) {
		this.c36 = c36;
	}

	public int getC37() {
		return c37;
	}

	public void setC37(int c37) {
		this.c37 = c37;
	}

	public int getC41() {
		return c41;
	}

	public void setC41(int c41) {
		this.c41 = c41;
	}

	public int getC42() {
		return c42;
	}

	public void setC42(int c42) {
		this.c42 = c42;
	}

	public int getC43() {
		return c43;
	}

	public void setC43(int c43) {
		this.c43 = c43;
	}

	public int getC44() {
		return c44;
	}

	public void setC44(int c44) {
		this.c44 = c44;
	}

	public int getC45() {
		return c45;
	}

	public void setC45(int c45) {
		this.c45 = c45;
	}

	public int getC46() {
		return c46;
	}

	public void setC46(int c46) {
		this.c46 = c46;
	}

	public int getC47() {
		return c47;
	}

	public void setC47(int c47) {
		this.c47 = c47;
	}

	public int getC51() {
		return c51;
	}

	public void setC51(int c51) {
		this.c51 = c51;
	}

	public int getC52() {
		return c52;
	}

	public void setC52(int c52) {
		this.c52 = c52;
	}

	public int getC53() {
		return c53;
	}

	public void setC53(int c53) {
		this.c53 = c53;
	}

	public int getC54() {
		return c54;
	}

	public void setC54(int c54) {
		this.c54 = c54;
	}

	public int getC55() {
		return c55;
	}

	public void setC55(int c55) {
		this.c55 = c55;
	}

	public int getC56() {
		return c56;
	}

	public void setC56(int c56) {
		this.c56 = c56;
	}

	public int getC57() {
		return c57;
	}

	public void setC57(int c57) {
		this.c57 = c57;
	}

	public int getC61() {
		return c61;
	}

	public void setC61(int c61) {
		this.c61 = c61;
	}

	public int getC62() {
		return c62;
	}

	public void setC62(int c62) {
		this.c62 = c62;
	}

	public int getC63() {
		return c63;
	}

	public void setC63(int c63) {
		this.c63 = c63;
	}

	public int getC64() {
		return c64;
	}

	public void setC64(int c64) {
		this.c64 = c64;
	}

	public int getC65() {
		return c65;
	}

	public void setC65(int c65) {
		this.c65 = c65;
	}

	public int getC66() {
		return c66;
	}

	public void setC66(int c66) {
		this.c66 = c66;
	}

	public int getC67() {
		return c67;
	}

	public void setC67(int c67) {
		this.c67 = c67;
	}

	public int getC71() {
		return c71;
	}

	public void setC71(int c71) {
		this.c71 = c71;
	}

	public int getC72() {
		return c72;
	}

	public void setC72(int c72) {
		this.c72 = c72;
	}

	public int getC73() {
		return c73;
	}

	public void setC73(int c73) {
		this.c73 = c73;
	}

	public int getC74() {
		return c74;
	}

	public void setC74(int c74) {
		this.c74 = c74;
	}

	public int getC75() {
		return c75;
	}

	public void setC75(int c75) {
		this.c75 = c75;
	}

	public int getC76() {
		return c76;
	}

	public void setC76(int c76) {
		this.c76 = c76;
	}

	public int getC77() {
		return c77;
	}

	public void setC77(int c77) {
		this.c77 = c77;
	}

	public String getCoefDegre() {
		return coefDegre;
	}

	public void setCoefDegre(String coefDegre) {
		this.coefDegre = coefDegre;
	}

	public long getStyleId() {
		return styleId;
	}

	public void setStyleId(long styleId) {
		this.styleId = styleId;
	}

	public int getC11() {
		return c11;
	}

	public void setC11(int c11) {
		this.c11 = c11;
	}

	public int getC12() {
		return c12;
	}

	public void setC12(int c12) {
		this.c12 = c12;
	}

	public int getC13() {
		return c13;
	}

	public void setC13(int c13) {
		this.c13 = c13;
	}

	public int getC14() {
		return c14;
	}

	public void setC14(int c14) {
		this.c14 = c14;
	}

	public int getC15() {
		return c15;
	}

	public void setC15(int c15) {
		this.c15 = c15;
	}

	public int getC16() {
		return c16;
	}

	public void setC16(int c16) {
		this.c16 = c16;
	}

	public int getC17() {
		return c17;
	}

	public void setC17(int c17) {
		this.c17 = c17;
	}

	public int getC21() {
		return c21;
	}

	public void setC21(int c21) {
		this.c21 = c21;
	}

	public int getC22() {
		return c22;
	}

	public void setC22(int c22) {
		this.c22 = c22;
	}

	public int getC23() {
		return c23;
	}

	public void setC23(int c23) {
		this.c23 = c23;
	}

	public int getC24() {
		return c24;
	}

	public void setC24(int c24) {
		this.c24 = c24;
	}

	public int getC25() {
		return c25;
	}

	public void setC25(int c25) {
		this.c25 = c25;
	}

	public int getC26() {
		return c26;
	}

	public void setC26(int c26) {
		this.c26 = c26;
	}

	public int getC27() {
		return c27;
	}

	public void setC27(int c27) {
		this.c27 = c27;
	}

	public int getC31() {
		return c31;
	}

	public void setC31(int c31) {
		this.c31 = c31;
	}

	public int getC32() {
		return c32;
	}

	public void setC32(int c32) {
		this.c32 = c32;
	}

	public int getC33() {
		return c33;
	}

	public void setC33(int c33) {
		this.c33 = c33;
	}

}
