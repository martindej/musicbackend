package com.example.musicBackend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class StyleDrums {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "serial")
	private long id;
	
	private long styleId;
	private long groupDrumsId;
	private long groupDrumsInsId;
	private String instrument;
	
	public StyleDrums(long styleId, long groupDrumsId, long groupDrumsInsId, String instrument) {
		this.instrument = instrument;
		this.styleId = styleId;
		this.groupDrumsId = groupDrumsId;
		this.groupDrumsInsId = groupDrumsInsId;
	}

	public String getInstrument() {
		return instrument;
	}

	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
	
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public long getStyleId() {
		return styleId;
	}

	public void setStyleId(long styleId) {
		this.styleId = styleId;
	}

	public long getGroupDrumsInsId() {
		return groupDrumsInsId;
	}

	public void setGroupDrumsInsId(long groupDrumsInsId) {
		this.groupDrumsInsId = groupDrumsInsId;
	}

	public long getGroupDrumsId() {
		return groupDrumsId;
	}

	public void setGroupDrumsId(long groupDrumsId) {
		this.groupDrumsId = groupDrumsId;
	}

}

