package com.example.musicBackend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class StyleGroupDrums {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "serial")
	private long id;
	private long styleId;
	private String name;
	
	private int rhythm;
	private int pourcentagePlayingTempsFortMin;
	private int pourcentagePlayingTempsFortMax;
	private int pourcentagePlayingTempsFaibleMin;
	private int pourcentagePlayingTempsFaibleMax;
	
	private int pourcentagePlayingTemps1Min;
	private int pourcentagePlayingTemps1Max;
	
	private int pourcentagePlayingTemps2Min;
	private int pourcentagePlayingTemps2Max;
	
	private int pourcentagePlayingTemps3Min;
	private int pourcentagePlayingTemps3Max;
	
	private int pourcentagePlayingTemps4Min;
	private int pourcentagePlayingTemps4Max;
	
	public StyleGroupDrums() {}
	
	public long getStyleId() {
		return styleId;
	}
	public void setStyleId(long styleId) {
		this.styleId = styleId;
	}
	public int getRhythm() {
		return rhythm;
	}
	public void setRhythm(int rhythm) {
		this.rhythm = rhythm;
	}
	public int getPourcentagePlayingTempsFortMin() {
		return pourcentagePlayingTempsFortMin;
	}
	public void setPourcentagePlayingTempsFortMin(int pourcentagePlayingTempsFortMin) {
		this.pourcentagePlayingTempsFortMin = pourcentagePlayingTempsFortMin;
	}
	public int getPourcentagePlayingTempsFortMax() {
		return pourcentagePlayingTempsFortMax;
	}
	public void setPourcentagePlayingTempsFortMax(int pourcentagePlayingTempsFortMax) {
		this.pourcentagePlayingTempsFortMax = pourcentagePlayingTempsFortMax;
	}
	public int getPourcentagePlayingTempsFaibleMin() {
		return pourcentagePlayingTempsFaibleMin;
	}
	public void setPourcentagePlayingTempsFaibleMin(int pourcentagePlayingTempsFaibleMin) {
		this.pourcentagePlayingTempsFaibleMin = pourcentagePlayingTempsFaibleMin;
	}
	public int getPourcentagePlayingTempsFaibleMax() {
		return pourcentagePlayingTempsFaibleMax;
	}
	public void setPourcentagePlayingTempsFaibleMax(int pourcentagePlayingTempsFaibleMax) {
		this.pourcentagePlayingTempsFaibleMax = pourcentagePlayingTempsFaibleMax;
	}
	public int getPourcentagePlayingTemps1Min() {
		return pourcentagePlayingTemps1Min;
	}
	public void setPourcentagePlayingTemps1Min(int pourcentagePlayingTemps1Min) {
		this.pourcentagePlayingTemps1Min = pourcentagePlayingTemps1Min;
	}
	public int getPourcentagePlayingTemps1Max() {
		return pourcentagePlayingTemps1Max;
	}
	public void setPourcentagePlayingTemps1Max(int pourcentagePlayingTemps1Max) {
		this.pourcentagePlayingTemps1Max = pourcentagePlayingTemps1Max;
	}
	public int getPourcentagePlayingTemps2Min() {
		return pourcentagePlayingTemps2Min;
	}
	public void setPourcentagePlayingTemps2Min(int pourcentagePlayingTemps2Min) {
		this.pourcentagePlayingTemps2Min = pourcentagePlayingTemps2Min;
	}
	public int getPourcentagePlayingTemps2Max() {
		return pourcentagePlayingTemps2Max;
	}
	public void setPourcentagePlayingTemps2Max(int pourcentagePlayingTemps2Max) {
		this.pourcentagePlayingTemps2Max = pourcentagePlayingTemps2Max;
	}
	public int getPourcentagePlayingTemps3Min() {
		return pourcentagePlayingTemps3Min;
	}
	public void setPourcentagePlayingTemps3Min(int pourcentagePlayingTemps3Min) {
		this.pourcentagePlayingTemps3Min = pourcentagePlayingTemps3Min;
	}
	public int getPourcentagePlayingTemps3Max() {
		return pourcentagePlayingTemps3Max;
	}
	public void setPourcentagePlayingTemps3Max(int pourcentagePlayingTemps3Max) {
		this.pourcentagePlayingTemps3Max = pourcentagePlayingTemps3Max;
	}
	public int getPourcentagePlayingTemps4Min() {
		return pourcentagePlayingTemps4Min;
	}
	public void setPourcentagePlayingTemps4Min(int pourcentagePlayingTemps4Min) {
		this.pourcentagePlayingTemps4Min = pourcentagePlayingTemps4Min;
	}
	public int getPourcentagePlayingTemps4Max() {
		return pourcentagePlayingTemps4Max;
	}
	public void setPourcentagePlayingTemps4Max(int pourcentagePlayingTemps4Max) {
		this.pourcentagePlayingTemps4Max = pourcentagePlayingTemps4Max;
	}
	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public long getId() {
		return this.id;
	}
}
