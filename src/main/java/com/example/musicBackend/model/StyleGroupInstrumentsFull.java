package com.example.musicBackend.model;

import java.util.List;

public class StyleGroupInstrumentsFull {
	
	private long id;
	private long styleId;
	private long motherId;
	private String name;
	private List<Integer> instruments;
	
	private int pourcentagePlayingMin; 
	private int pourcentagePlayingMax; 
	private int balanceBassMelodyMin; 
	private int balanceBassMelodyMax; 
	private int balanceMelodyMelodyBisMin; 
	private int balanceMelodyMelodyBisMax; 
	private int gBetweenNotesMin; 
	private int gBetweenNotesMax;
	private int dispersionNotesMin; 
	private int dispersionNotesMax;
	private int pourcentageSilMin; 
	private int pourcentageSilMax;
	private int speedMax; 
	private int speedMin; 
//	private Map<EnumDurationNote, Double> allowedR; 
	private int pSyncopesMin; 
	private int pSyncopesMax;
	private int pSyncopettesMin; 
	private int pSyncopettesMax;
	private int pSyncopesTempsFortsMin; 
	private int pSyncopesTempsFortsMax; 
	private int pContreTempsMin; 
	private int pContreTempsMax;
	private int pDissonanceMin; 
	private int pDissonanceMax;
	private int pIrregularitesMin; 
	private int pIrregularitesMax;
	private int bBinaireTernaireMin; 
	private int bBinaireTernaireMax; 
	private int pChanceSupSameRythmMin;
	private int pChanceSupSameRythmMax;
	private int numberNotesSimultaneouslyMin;
	private int numberNotesSimultaneouslyMax;
	private int probabilityRythmMin;
	private int probabilityRythmMax;
	private int numberRythmMin;
	private int numberRythmMax;
	private int weightRythmMin;
	private int weightRythmMax;
	private int pChanceSameNoteInMotifMin;
	private int pChanceSameNoteInMotifMax;
	private int pChanceStartByTonaleAsBassMin;
	private int pChanceStartByTonaleAsBassMax;
	private int pChanceStayAroundNoteRefMin;
	private int pChanceStayAroundNoteRefMax;
	private int pChancePlayChordMin;
	private int pChancePlayChordMax;
	
	

	public StyleGroupInstrumentsFull() {}
	
	public StyleGroupInstrumentsFull(StyleGroupInstruments st) {
		this.setId(st.getId());
		this.setStyleId(st.getStyleId());
		this.name = st.getName();
		this.motherId = st.getMotherId();
      
		this.pourcentagePlayingMin =  st.getPourcentagePlayingMin();
		this.pourcentagePlayingMax =  st.getPourcentagePlayingMax(); 
		this.balanceBassMelodyMin =  st.getBalanceBassMelodyMin(); 
		this.balanceBassMelodyMax =  st.getBalanceBassMelodyMax(); 
		this.balanceMelodyMelodyBisMin =  st.getBalanceMelodyMelodyBisMin(); 
		this.balanceMelodyMelodyBisMax =  st.getBalanceMelodyMelodyBisMax(); 
		this.gBetweenNotesMin =  st.getgBetweenNotesMin();
		this.gBetweenNotesMax = st.getgBetweenNotesMax();
		this.dispersionNotesMin = st.getDispersionNotesMin();
		this.dispersionNotesMax = st.getDispersionNotesMax();
		this.pourcentageSilMin =  st.getPourcentageSilMin();
		this.pourcentageSilMax = st.getPourcentageSilMax();
		this.speedMax =  st.getSpeedMax();
		this.speedMin =  st.getSpeedMin();
		this.pSyncopesMin =  st.getpSyncopesMin();
		this.pSyncopesMax = st.getpSyncopesMax();
		this.pSyncopettesMin =  st.getpSyncopettesMin();
		this.pSyncopettesMax = st.getpSyncopettesMax();
		this.pSyncopesTempsFortsMin =  st.getpSyncopesTempsFortsMin();
		this.pSyncopesTempsFortsMax =  st.getpSyncopesTempsFortsMax();
		this.pContreTempsMin =  st.getpContreTempsMin();
		this.pContreTempsMax = st.getpContreTempsMax();
		this.pDissonanceMin = st.getpDissonanceMin(); 
		this.pDissonanceMax = st.getpDissonanceMax();
		this.pIrregularitesMin = st.getpIrregularitesMin(); 
		this.pIrregularitesMax = st.getpIrregularitesMax();
		this.bBinaireTernaireMin = st.getbBinaireTernaireMin(); 
		this.bBinaireTernaireMax = st.getbBinaireTernaireMax(); 
		this.pChanceSupSameRythmMin = st.getpChanceSupSameRythmMin();
		this.pChanceSupSameRythmMax = st.getpChanceSupSameRythmMax();
		this.numberNotesSimultaneouslyMin = st.getNumberNotesSimultaneouslyMin();
		this.numberNotesSimultaneouslyMax = st.getNumberNotesSimultaneouslyMax();
		this.probabilityRythmMin = st.getProbabilityRythmMin();
		this.probabilityRythmMax = st.getProbabilityRythmMax();
		this.numberRythmMin = st.getNumberRythmMin();
		this.numberRythmMax = st.getNumberRythmMax();
		this.weightRythmMin = st.getWeightRythmMin();
		this.weightRythmMax = st.getWeightRythmMax();
		this.pChanceSameNoteInMotifMin = st.getpChanceSameNoteInMotifMin();
		this.pChanceSameNoteInMotifMax = st.getpChanceSameNoteInMotifMax();
		this.pChanceStartByTonaleAsBassMin = st.getpChanceStartByTonaleAsBassMin();
		this.pChanceStartByTonaleAsBassMax = st.getpChanceStartByTonaleAsBassMax();
		this.pChanceStayAroundNoteRefMin = st.getpChanceStayAroundNoteRefMin();
		this.pChanceStayAroundNoteRefMax = st.getpChanceStayAroundNoteRefMax();
		this.pChancePlayChordMin = st.getpChancePlayChordMin();
		this.pChancePlayChordMax = st.getpChancePlayChordMax();
	}
	
	public int getSpeedMax() {
		return speedMax;
	}
	public void setSpeedMax(int speedMax) {
		this.speedMax = speedMax;
	}
	public int getSpeedMin() {
		return speedMin;
	}
	public void setSpeedMin(int speedMin) {
		this.speedMin = speedMin;
	}
	public int getgBetweenNotesMin() {
		return gBetweenNotesMin;
	}
	public void setgBetweenNotesMin(int gBetweenNotesMin) {
		this.gBetweenNotesMin = gBetweenNotesMin;
	}
	public int getgBetweenNotesMax() {
		return gBetweenNotesMax;
	}
	public void setgBetweenNotesMax(int gBetweenNotesMax) {
		this.gBetweenNotesMax = gBetweenNotesMax;
	}
	public int getPourcentageSilMin() {
		return pourcentageSilMin;
	}
	public void setPourcentageSilMin(int pourcentageSilMin) {
		this.pourcentageSilMin = pourcentageSilMin;
	}
	public int getPourcentageSilMax() {
		return pourcentageSilMax;
	}
	public void setPourcentageSilMax(int pourcentageSilMax) {
		this.pourcentageSilMax = pourcentageSilMax;
	}
	public int getpSyncopesMin() {
		return pSyncopesMin;
	}
	public void setpSyncopesMin(int pSyncopesMin) {
		this.pSyncopesMin = pSyncopesMin;
	}
	public int getpSyncopesMax() {
		return pSyncopesMax;
	}
	public void setpSyncopesMax(int pSyncopesMax) {
		this.pSyncopesMax = pSyncopesMax;
	}
	public int getpSyncopettesMin() {
		return pSyncopettesMin;
	}
	public void setpSyncopettesMin(int pSyncopettesMin) {
		this.pSyncopettesMin = pSyncopettesMin;
	}
	public int getpSyncopettesMax() {
		return pSyncopettesMax;
	}
	public void setpSyncopettesMax(int pSyncopettesMax) {
		this.pSyncopettesMax = pSyncopettesMax;
	}
	public int getpSyncopesTempsFortsMin() {
		return pSyncopesTempsFortsMin;
	}
	public void setpSyncopesTempsFortsMin(int pSyncopesTempsFortsMin) {
		this.pSyncopesTempsFortsMin = pSyncopesTempsFortsMin;
	}
	public int getpSyncopesTempsFortsMax() {
		return pSyncopesTempsFortsMax;
	}
	public void setpSyncopesTempsFortsMax(int pSyncopesTempsFortsMax) {
		this.pSyncopesTempsFortsMax = pSyncopesTempsFortsMax;
	}
	public int getpContreTempsMin() {
		return pContreTempsMin;
	}
	public void setpContreTempsMin(int pContreTempsMin) {
		this.pContreTempsMin = pContreTempsMin;
	}
	public int getpContreTempsMax() {
		return pContreTempsMax;
	}
	public void setpContreTempsMax(int pContreTempsMax) {
		this.pContreTempsMax = pContreTempsMax;
	}
	public int getpDissonanceMin() {
		return pDissonanceMin;
	}
	public void setpDissonanceMin(int pDissonanceMin) {
		this.pDissonanceMin = pDissonanceMin;
	}
	public int getpDissonanceMax() {
		return pDissonanceMax;
	}
	public void setpDissonanceMax(int pDissonanceMax) {
		this.pDissonanceMax = pDissonanceMax;
	}
	public int getpIrregularitesMin() {
		return pIrregularitesMin;
	}
	public void setpIrregularitesMin(int pIrregularitesMin) {
		this.pIrregularitesMin = pIrregularitesMin;
	}
	public int getpIrregularitesMax() {
		return pIrregularitesMax;
	}
	public void setpIrregularitesMax(int pIrregularitesMax) {
		this.pIrregularitesMax = pIrregularitesMax;
	}
	public int getpChanceSupSameRythmMax() {
		return pChanceSupSameRythmMax;
	}
	public void setpChanceSupSameRythmMax(int pChanceSupSameRythmMax) {
		this.pChanceSupSameRythmMax = pChanceSupSameRythmMax;
	}
	public int getpChanceSupSameRythmMin() {
		return pChanceSupSameRythmMin;
	}
	public void setpChanceSupSameRythmMin(int pChanceSupSameRythmMin) {
		this.pChanceSupSameRythmMin = pChanceSupSameRythmMin;
	}
	public int getbBinaireTernaireMin() {
		return bBinaireTernaireMin;
	}
	public void setbBinaireTernaireMin(int bBinaireTernaireMin) {
		this.bBinaireTernaireMin = bBinaireTernaireMin;
	}
	public int getbBinaireTernaireMax() {
		return bBinaireTernaireMax;
	}
	public void setbBinaireTernaireMax(int bBinaireTernaireMax) {
		this.bBinaireTernaireMax = bBinaireTernaireMax;
	}

	public long getStyleId() {
		return styleId;
	}

	public void setStyleId(long style_id) {
		this.styleId = style_id;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<Integer> getInstruments() {
		return instruments;
	}

	public void setInstruments(List<Integer> instruments) {
		this.instruments = instruments;
	}

	public int getPourcentagePlayingMin() {
		return pourcentagePlayingMin;
	}

	public void setPourcentagePlayingMin(int pourcentagePlayingMin) {
		this.pourcentagePlayingMin = pourcentagePlayingMin;
	}

	public int getPourcentagePlayingMax() {
		return pourcentagePlayingMax;
	}

	public void setPourcentagePlayingMax(int pourcentagePlayingMax) {
		this.pourcentagePlayingMax = pourcentagePlayingMax;
	}

	public int getBalanceBassMelodyMin() {
		return balanceBassMelodyMin;
	}

	public void setBalanceBassMelodyMin(int balanceBassMelodyMin) {
		this.balanceBassMelodyMin = balanceBassMelodyMin;
	}

	public int getBalanceBassMelodyMax() {
		return balanceBassMelodyMax;
	}

	public void setBalanceBassMelodyMax(int balanceBassMelodyMax) {
		this.balanceBassMelodyMax = balanceBassMelodyMax;
	}

	public int getBalanceMelodyMelodyBisMin() {
		return balanceMelodyMelodyBisMin;
	}

	public void setBalanceMelodyMelodyBisMin(int balanceMelodyMelodyBisMin) {
		this.balanceMelodyMelodyBisMin = balanceMelodyMelodyBisMin;
	}

	public int getBalanceMelodyMelodyBisMax() {
		return balanceMelodyMelodyBisMax;
	}

	public void setBalanceMelodyMelodyBisMax(int balanceMelodyMelodyBisMax) {
		this.balanceMelodyMelodyBisMax = balanceMelodyMelodyBisMax;
	}

	public long getMotherId() {
		return motherId;
	}

	public void setMotherId(long motherId) {
		this.motherId = motherId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDispersionNotesMin() {
		return dispersionNotesMin;
	}

	public void setDispersionNotesMin(int dispersionNotesMin) {
		this.dispersionNotesMin = dispersionNotesMin;
	}

	public int getDispersionNotesMax() {
		return dispersionNotesMax;
	}

	public void setDispersionNotesMax(int dispersionNotesMax) {
		this.dispersionNotesMax = dispersionNotesMax;
	}

	public int getNumberNotesSimultaneouslyMin() {
		return numberNotesSimultaneouslyMin;
	}

	public void setNumberNotesSimultaneouslyMin(int numberNotesSimultaneouslyMin) {
		this.numberNotesSimultaneouslyMin = numberNotesSimultaneouslyMin;
	}

	public int getNumberNotesSimultaneouslyMax() {
		return numberNotesSimultaneouslyMax;
	}

	public void setNumberNotesSimultaneouslyMax(int numberNotesSimultaneouslyMax) {
		this.numberNotesSimultaneouslyMax = numberNotesSimultaneouslyMax;
	}

	public int getProbabilityRythmMin() {
		return probabilityRythmMin;
	}

	public void setProbabilityRythmMin(int probabilityRythmMin) {
		this.probabilityRythmMin = probabilityRythmMin;
	}

	public int getProbabilityRythmMax() {
		return probabilityRythmMax;
	}

	public void setProbabilityRythmMax(int probabilityRythmMax) {
		this.probabilityRythmMax = probabilityRythmMax;
	}

	public int getNumberRythmMin() {
		return numberRythmMin;
	}

	public void setNumberRythmMin(int numberRythmMin) {
		this.numberRythmMin = numberRythmMin;
	}

	public int getNumberRythmMax() {
		return numberRythmMax;
	}

	public void setNumberRythmMax(int numberRythmMax) {
		this.numberRythmMax = numberRythmMax;
	}

	public int getWeightRythmMin() {
		return weightRythmMin;
	}

	public void setWeightRythmMin(int weightRythmMin) {
		this.weightRythmMin = weightRythmMin;
	}

	public int getWeightRythmMax() {
		return weightRythmMax;
	}

	public void setWeightRythmMax(int weightRythmMax) {
		this.weightRythmMax = weightRythmMax;
	}
	
	public int getpChanceSameNoteInMotifMin() {
		return pChanceSameNoteInMotifMin;
	}

	public void setpChanceSameNoteInMotifMin(int pChanceSameNoteInMotifMin) {
		this.pChanceSameNoteInMotifMin = pChanceSameNoteInMotifMin;
	}

	public int getpChanceSameNoteInMotifMax() {
		return pChanceSameNoteInMotifMax;
	}

	public void setpChanceSameNoteInMotifMax(int pChanceSameNoteInMotifMax) {
		this.pChanceSameNoteInMotifMax = pChanceSameNoteInMotifMax;
	}

	public int getpChanceStartByTonaleAsBassMin() {
		return pChanceStartByTonaleAsBassMin;
	}

	public void setpChanceStartByTonaleAsBassMin(int pChanceStartByTonaleAsBassMin) {
		this.pChanceStartByTonaleAsBassMin = pChanceStartByTonaleAsBassMin;
	}

	public int getpChanceStartByTonaleAsBassMax() {
		return pChanceStartByTonaleAsBassMax;
	}

	public void setpChanceStartByTonaleAsBassMax(int pChanceStartByTonaleAsBassMax) {
		this.pChanceStartByTonaleAsBassMax = pChanceStartByTonaleAsBassMax;
	}

	public int getpChanceStayAroundNoteRefMin() {
		return pChanceStayAroundNoteRefMin;
	}

	public void setpChanceStayAroundNoteRefMin(int pChanceStayAroundNoteRefMin) {
		this.pChanceStayAroundNoteRefMin = pChanceStayAroundNoteRefMin;
	}

	public int getpChanceStayAroundNoteRefMax() {
		return pChanceStayAroundNoteRefMax;
	}

	public void setpChanceStayAroundNoteRefMax(int pChanceStayAroundNoteRefMax) {
		this.pChanceStayAroundNoteRefMax = pChanceStayAroundNoteRefMax;
	}

	public int getpChancePlayChordMin() {
		return pChancePlayChordMin;
	}

	public void setpChancePlayChordMin(int pChancePlayChordMin) {
		this.pChancePlayChordMin = pChancePlayChordMin;
	}

	public int getpChancePlayChordMax() {
		return pChancePlayChordMax;
	}

	public void setpChancePlayChordMax(int pChancePlayChordMax) {
		this.pChancePlayChordMax = pChancePlayChordMax;
	}

}

