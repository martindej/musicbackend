package com.example.musicBackend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class StyleMusic {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "serial")
	private long id;
	
	private boolean publish; 
	
	private String styleName; 
	private int rythmeMax; 
	private int rythmeMin; 
	private int nInstrumentMin; 
	private int nInstrumentMax;
	private int nPercussionMin; 
	private int nPercussionMax;
	
	private int complexityChordMin;
	private int complexityChordMax;
	private int numberChordMin;
	private int numberChordMax;
	
	private String soundFont;
	
	private byte[] image;
	
	public StyleMusic() {}
	
	public String getStyleName() {
		return styleName;
	}
	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}
	public int getRythmeMax() {
		return rythmeMax;
	}
	public void setRythmeMax(int rythmeMax) {
		this.rythmeMax = rythmeMax;
	}
	public int getRythmeMin() {
		return rythmeMin;
	}
	public void setRythmeMin(int rythmeMin) {
		this.rythmeMin = rythmeMin;
	}
	public int getnInstrumentMin() {
		return nInstrumentMin;
	}
	public void setnInstrumentMin(int nInstrumentMin) {
		this.nInstrumentMin = nInstrumentMin;
	}
	public int getnInstrumentMax() {
		return nInstrumentMax;
	}
	public void setnInstrumentMax(int nInstrumentMax) {
		this.nInstrumentMax = nInstrumentMax;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}
	
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public boolean isPublish() {
		return publish;
	}

	public void setPublish(boolean publish) {
		this.publish = publish;
	}

	public int getnPercussionMin() {
		return nPercussionMin;
	}

	public void setnPercussionMin(int nPercussionMin) {
		this.nPercussionMin = nPercussionMin;
	}

	public int getnPercussionMax() {
		return nPercussionMax;
	}

	public void setnPercussionMax(int nPercussionMax) {
		this.nPercussionMax = nPercussionMax;
	}

	public String getSoundFont() {
		return soundFont;
	}

	public void setSoundFont(String soundFont) {
		this.soundFont = soundFont;
	}

	public int getComplexityChordMin() {
		return complexityChordMin;
	}

	public void setComplexityChordMin(int complexityChordMin) {
		this.complexityChordMin = complexityChordMin;
	}

	public int getComplexityChordMax() {
		return complexityChordMax;
	}

	public void setComplexityChordMax(int complexityChordMax) {
		this.complexityChordMax = complexityChordMax;
	}

	public int getNumberChordMin() {
		return numberChordMin;
	}

	public void setNumberChordMin(int numberChordMin) {
		this.numberChordMin = numberChordMin;
	}

	public int getNumberChordMax() {
		return numberChordMax;
	}

	public void setNumberChordMax(int numberChordMax) {
		this.numberChordMax = numberChordMax;
	}
	
}
