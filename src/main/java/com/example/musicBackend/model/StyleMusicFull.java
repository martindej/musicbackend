package com.example.musicBackend.model;

import java.util.ArrayList;
import java.util.List;

public class StyleMusicFull {
	
	private long id;
	
	private boolean publish;
	
	private String styleName; 
	private List<StyleGroupInstrumentsFull> styleGroupIns;
	private List<StyleGroupDrumsFull> styleGroupDrums;
	private List<MesureTempsFortFull> mesureTempsFort;
	private List<StyleCoefDegres> styleCoefDegres;
	private List<Integer> availableTon;  // table styleTonality
	private int rythmeMax; 
	private int rythmeMin; 
	private int nInstrumentMin; 
	private int nInstrumentMax;
	private int nPercussionMin; 
	private int nPercussionMax;
	
	private int complexityChordMin;
	private int complexityChordMax;
	private int numberChordMin;
	private int numberChordMax;
	
	private String soundFont;

	private byte[] image;
	
	public StyleMusicFull() {}
	
	public StyleMusicFull(StyleMusic st) {
		this.setId(st.getId());
		this.setPublish(st.isPublish());
		this.styleName = st.getStyleName();
		this.rythmeMax =  st.getRythmeMax();
		this.rythmeMin =  st.getRythmeMin();
		this.nInstrumentMin =  st.getnInstrumentMin();
		this.nInstrumentMax = st.getnInstrumentMax();
		this.nPercussionMin = st.getnPercussionMin();
		this.nPercussionMax = st.getnPercussionMax();
		this.complexityChordMin = st.getComplexityChordMin();
		this.complexityChordMax = st.getComplexityChordMax();
		this.numberChordMin = st.getNumberChordMin();
		this.numberChordMax = st.getNumberChordMax();
		this.soundFont = st.getSoundFont();
		
		this.mesureTempsFort = new ArrayList<MesureTempsFortFull>();
		this.styleCoefDegres = new ArrayList<StyleCoefDegres>();
		this.styleGroupIns = new ArrayList<StyleGroupInstrumentsFull>();
		this.styleGroupDrums = new ArrayList<StyleGroupDrumsFull>();
	}

	public String getStyleName() {
		return styleName;
	}
	public void setStyleName(String styleName) {
		this.styleName = styleName;
	}
	public int getRythmeMax() {
		return rythmeMax;
	}
	public void setRythmeMax(int rythmeMax) {
		this.rythmeMax = rythmeMax;
	}
	public int getRythmeMin() {
		return rythmeMin;
	}
	public void setRythmeMin(int rythmeMin) {
		this.rythmeMin = rythmeMin;
	}

	public int getnInstrumentMin() {
		return nInstrumentMin;
	}
	public void setnInstrumentMin(int nInstrumentMin) {
		this.nInstrumentMin = nInstrumentMin;
	}
	public int getnInstrumentMax() {
		return nInstrumentMax;
	}
	public void setnInstrumentMax(int nInstrumentMax) {
		this.nInstrumentMax = nInstrumentMax;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public List<Integer> getAvailableTon() {
		return availableTon;
	}

	public void setAvailableTon(List<Integer> availableTon) {
		this.availableTon = availableTon;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public boolean isPublish() {
		return publish;
	}

	public void setPublish(boolean publish) {
		this.publish = publish;
	}

	public List<StyleGroupInstrumentsFull> getStyleGroupIns() {
		return styleGroupIns;
	}

	public void setStyleGroupIns(List<StyleGroupInstrumentsFull> styleGroupIns) {
		this.styleGroupIns = styleGroupIns;
	}

	public int getnPercussionMin() {
		return nPercussionMin;
	}

	public void setnPercussionMin(int nPercussionMin) {
		this.nPercussionMin = nPercussionMin;
	}

	public int getnPercussionMax() {
		return nPercussionMax;
	}

	public void setnPercussionMax(int nPercussionMax) {
		this.nPercussionMax = nPercussionMax;
	}

	public String getSoundFont() {
		return soundFont;
	}

	public void setSoundFont(String soundFont) {
		this.soundFont = soundFont;
	}

	public List<MesureTempsFortFull> getMesureTempsFort() {
		return mesureTempsFort;
	}

	public void setMesureTempsFort(List<MesureTempsFortFull> mesureTempsFort) {
		this.mesureTempsFort = mesureTempsFort;
	}

	public int getComplexityChordMin() {
		return complexityChordMin;
	}

	public void setComplexityChordMin(int complexityChordMin) {
		this.complexityChordMin = complexityChordMin;
	}

	public int getComplexityChordMax() {
		return complexityChordMax;
	}

	public void setComplexityChordMax(int complexityChordMax) {
		this.complexityChordMax = complexityChordMax;
	}

	public int getNumberChordMin() {
		return numberChordMin;
	}

	public void setNumberChordMin(int numberChordMin) {
		this.numberChordMin = numberChordMin;
	}

	public int getNumberChordMax() {
		return numberChordMax;
	}

	public void setNumberChordMax(int numberChordMax) {
		this.numberChordMax = numberChordMax;
	}

	public List<StyleGroupDrumsFull> getStyleGroupDrums() {
		return styleGroupDrums;
	}

	public void setStyleGroupDrums(List<StyleGroupDrumsFull> styleGroupDrums) {
		this.styleGroupDrums = styleGroupDrums;
	}

	public List<StyleCoefDegres> getStyleCoefDegres() {
		return styleCoefDegres;
	}

	public void setStyleCoefDegres(List<StyleCoefDegres> styleCoefDegres) {
		this.styleCoefDegres = styleCoefDegres;
	}
}
