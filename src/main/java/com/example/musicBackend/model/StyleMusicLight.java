package com.example.musicBackend.model;

public class StyleMusicLight {
	
	private long id;
	private String styleName;
	
	public StyleMusicLight(long id, String styleName) {
		this.id = id;
		this.styleName = styleName;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getStyleName() {
		return styleName;
	}
	public void setStyleName(String styleName) {
		this.styleName = styleName;
	} 

}
