package com.example.musicBackend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class StyleTempFort {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "serial")
	private long id;
	
	private long styleId;
	private long mesureId;
	private String tempsFort;
	
	public StyleTempFort(long mesureId, long styleId, String tempsFort) {
		this.tempsFort = tempsFort;
		this.mesureId = mesureId;
		this.styleId = styleId;
	}

	public String getTempsFort() {
		return tempsFort;
	}

	public void setTempsFort(String tempsFort) {
		this.tempsFort = tempsFort;
	}
	
	public long getId() {
		return this.id;
	}
	
	public void setId(long id) {
		this.id = id;
	}

	public long getMesureId() {
		return mesureId;
	}

	public void setMesureId(long mesureId) {
		this.mesureId = mesureId;
	}

	public long getStyleId() {
		return styleId;
	}

	public void setStyleId(long styleId) {
		this.styleId = styleId;
	}

}
