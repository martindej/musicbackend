package com.example.musicBackend.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Subscription implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "serial")
	private long id;
	private String name;
	private int musicsPerDay;
	private double pricePerMonth;
	private double pricePerYear;
	private byte[] logo;
	private boolean generalSettings;
	private boolean advancedSettings;
	private boolean melodySettings;
	private int durationInDays;
	
	public Subscription() {}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMusicsPerDay() {
		return musicsPerDay;
	}

	public void setMusicsPerDay(int musicsPerDay) {
		this.musicsPerDay = musicsPerDay;
	}

	public double getPricePerMonth() {
		return pricePerMonth;
	}

	public void setPricePerMonth(double pricePerMonth) {
		this.pricePerMonth = pricePerMonth;
	}

	public double getPricePerYear() {
		return pricePerYear;
	}

	public void setPricePerYear(double pricePerYear) {
		this.pricePerYear = pricePerYear;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public boolean isGeneralSettings() {
		return generalSettings;
	}

	public void setGeneralSettings(boolean generalSettings) {
		this.generalSettings = generalSettings;
	}

	public boolean isMelodySettings() {
		return melodySettings;
	}

	public void setMelodySettings(boolean melodySettings) {
		this.melodySettings = melodySettings;
	}

	public boolean isAdvancedSettings() {
		return advancedSettings;
	}

	public void setAdvancedSettings(boolean advancedSettings) {
		this.advancedSettings = advancedSettings;
	}

	public int getDurationInDays() {
		return durationInDays;
	}

	public void setDurationInDays(int durationInDays) {
		this.durationInDays = durationInDays;
	}

}
