package com.example.musicBackend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class SubscriptionUser {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "serial")
	private long id;
	
	private long userId;
	private long subscriptionId;
	private long start_date;
	private long end_date;
	private int duration_days;
	private int musicsPerDay;
	
	private boolean advancedSettings;
	private boolean melodySettings;
	
	public SubscriptionUser() {}
	
	public SubscriptionUser(long userId, long subscriptionId, long start_date, int duration_days, int musicsPerDay, boolean advancedSettings, boolean melodySettings) {
		this.userId = userId;
		this.subscriptionId = subscriptionId;
		this.start_date = start_date;
		this.end_date = start_date + (long)((long)duration_days*24*3600*1000); // add long type to avoid overflow
		this.duration_days = duration_days;
		this.musicsPerDay = musicsPerDay;
		this.advancedSettings = advancedSettings;
		this.melodySettings = melodySettings;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public long getStart_date() {
		return start_date;
	}

	public void setStart_date(long start_date) {
		this.start_date = start_date;
	}

	public long getEnd_date() {
		return end_date;
	}

	public void setEnd_date(long end_date) {
		this.end_date = end_date;
	}

	public int getDuration_days() {
		return duration_days;
	}

	public void setDuration_days(int duration_days) {
		this.duration_days = duration_days;
	}

	public int getMusicsPerDay() {
		return musicsPerDay;
	}

	public void setMusicsPerDay(int musicsPerDay) {
		this.musicsPerDay = musicsPerDay;
	}

	public boolean isAdvancedSettings() {
		return advancedSettings;
	}

	public void setAdvancedSettings(boolean advancedSettings) {
		this.advancedSettings = advancedSettings;
	}

	public boolean isMelodySettings() {
		return melodySettings;
	}

	public void setMelodySettings(boolean melodySettings) {
		this.melodySettings = melodySettings;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return this.id;
	}

}
