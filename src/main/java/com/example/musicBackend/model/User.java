package com.example.musicBackend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "utilisateurs")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "serial")
	private long id;
	private String username;
	
	private String password;
	private String email;
	private boolean emailChecked = false;
	private String identifier;
	private boolean player = false;
	private boolean composer = false;
	
	private boolean newsletter = true;
	
	private String instruments;
	
	private String nameAvatar;
	private byte[] avatar;
	
	private boolean superUser = false;
	private long subscriptionId = 0;
	private long boosterId = 0;
	
//	@Lob
//	@Type(type = "org.hibernate.type.TextType")
	@Column(columnDefinition = "text")
	private String description;
	
	public User() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public boolean getEmailChecked() {
		return emailChecked;
	}

	public void setEmailChecked(boolean emailChecked) {
		this.emailChecked = emailChecked;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}
//
//	public List<EnumInstruments> getMyInstruments() {
//		return myInstruments;
//	}
//
//	public void setMyInstruments(List<EnumInstruments> myInstruments) {
//		this.myInstruments = myInstruments;
//	}

	public String getNameAvatar() {
		return nameAvatar;
	}

	public void setNameAvatar(String nameAvatar) {
		this.nameAvatar = nameAvatar;
	}

	public byte[] getAvatar() {
		return avatar;
	}

	public void setAvatar(byte[] avatar) {
		this.avatar = avatar;
	}

	public boolean isPlayer() {
		return player;
	}

	public void setPlayer(boolean player) {
		this.player = player;
	}

	public boolean isComposer() {
		return composer;
	}

	public void setComposer(boolean composer) {
		this.composer = composer;
	}

	public String getInstruments() {
		return instruments;
	}

	public void setInstruments(String instruments) {
		this.instruments = instruments;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isSuperUser() {
		return superUser;
	}

	public void setSuperUser(boolean superUser) {
		this.superUser = superUser;
	}

	public long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public long getBoosterId() {
		return boosterId;
	}

	public void setBoosterId(long boosterId) {
		this.boosterId = boosterId;
	}

	public boolean isNewsletter() {
		return newsletter;
	}

	public void setNewsletter(boolean newsletter) {
		this.newsletter = newsletter;
	}
	
//	public byte[] getDescriptionAsBytes() throws UnsupportedEncodingException {
//		byte[] byteData = description.getBytes("UTF-8");//Better to specify encoding
//		return byteData;
////		Blob blobData = dbConnection.createBlob();
////		blobData.setBytes(1, byteData);
////		return description;
//	}
}
