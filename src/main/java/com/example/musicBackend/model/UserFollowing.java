package com.example.musicBackend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_following")
public class UserFollowing {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(columnDefinition = "serial")
	private long id;
	
	private long user_id;
	private long following_user_id;
	
	public UserFollowing() {}
	
	public long getUser_id() {
		return user_id;
	}
	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}
	public long getFollowing_user_id() {
		return following_user_id;
	}
	public void setFollowing_user_id(long following_user_id) {
		this.following_user_id = following_user_id;
	}

	public void setId(long id) {
		this.id = id;
		
	}

}
