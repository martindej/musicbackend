package com.example.musicBackend.service;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import com.example.musicBackend.model.Mail;

@Component
public class EmailService {
  
    @Autowired
    public JavaMailSender emailSender;
    
    @Autowired
    private SpringTemplateEngine templateEngine;
 
    public void sendSimpleMessage(String from, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage(); 
        message.setTo("murmure.codemure@gmail.com"); 
        message.setFrom(from); 
        message.setSubject(subject); 
        message.setText(text);
        emailSender.send(message);
    }
    
    public void sendMessageWithAttachment(String to, String subject, String text, String pathToAttachment) throws MessagingException {
         
        MimeMessage message = emailSender.createMimeMessage();
          
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
         
        helper.setTo(to);
        helper.setSubject(subject);
        helper.setText(text);
             
        FileSystemResource file = new FileSystemResource(new File(pathToAttachment));
        helper.addAttachment("Invoice", file);
    
        emailSender.send(message);
    }
    
    public void sendNewsletter(Mail mail, Context context) throws MessagingException, IOException {
        MimeMessage message = emailSender.createMimeMessage();
//        MimeMessageHelper helper = new MimeMessageHelper(message,
//                MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
//                StandardCharsets.UTF_8.name());
        MimeMessageHelper helper = new MimeMessageHelper(message, true /* multipart */, "UTF-8");

//        helper.addAttachment("play.png", new ClassPathResource("static/play_black.png"));
//        helper.addAttachment("logo.png", new ClassPathResource("static/logoMure.png"));
        
//        
        
//        context.setVariable("logo", "logo"); // so that we can reference it from HTML

        String html = templateEngine.process("newsletter", context);

        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());
        
     // Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
//        byte[] extractedBytes = extractBytes("static/logoMure.png");
//        final InputStreamSource imageSource = new ByteArrayResource(extractedBytes);
        
        helper.addInline("logo", new ClassPathResource("static/logoMure.png"), "image/png");

        emailSender.send(message);
    }
    
    public void sendMailConfirmation(Mail mail, Context context) throws MessagingException, IOException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true /* multipart */, "UTF-8");

        String html = templateEngine.process("emailCheck", context);

        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());
 
        helper.addInline("logo", new ClassPathResource("static/logoMure.png"), "image/png");

        emailSender.send(message);
    }
    
    public void sendMessage(Mail mail, Context context) throws MessagingException, IOException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true /* multipart */, "UTF-8");

        String html = templateEngine.process("frame", context);

        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(mail.getSubject());
        helper.setFrom(mail.getFrom());
 
        helper.addInline("logo", new ClassPathResource("static/logoMure.png"), "image/png");

        emailSender.send(message);
    }
    
    public byte[] extractBytes (String ImageName) throws IOException {
    	 // open image
    	ClassLoader classLoader = getClass().getClassLoader();
    	 File imgPath = new File(classLoader.getResource(ImageName).getFile());
    	 BufferedImage bufferedImage = ImageIO.read(imgPath);

    	 // get DataBufferBytes from Raster
    	 WritableRaster raster = bufferedImage .getRaster();
    	 DataBufferByte data   = (DataBufferByte) raster.getDataBuffer();

    	 return ( data.getData() );
    }
}
