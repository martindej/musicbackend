package com.example.musicBackend.service;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.Context;

import com.example.musicBackend.model.Mail;
import com.example.musicBackend.model.MusicFileLight;
import com.example.musicBackend.model.User;
 
@Component
public class ScheduleService {
      
    @Autowired
	ServiceMusic serviceMusic;
    
    @Autowired
	ServiceUser serviceUser;
    
    @Autowired
	EmailService emailService;
     
    // initialDelay = 3 second.
    // fixedDelay = 2 second.
    @Scheduled( cron="0 0 0 ? * MON")
//    @Scheduled(fixedDelay = 2000 * 1000, initialDelay = 5 * 1000)
    public void sendWeeklyNewsletter() {
         
    	List<User> listUsers = serviceUser.getListUsersNewsletter();
    	List<MusicFileLight> topMusics = serviceMusic.getTopMusicsMail();
    	
    	// new musics with your instruments
    	// new musics from people you follow
    	for(User user:listUsers) {
    		
    		Mail mail = new Mail();
            mail.setFrom("murmure.codemure@gmail.com");
            mail.setTo(user.getEmail());
            mail.setSubject("Murmûre letter");
            
            
            Context context = new Context();
            context.setVariable("name", user.getUsername());        
            context.setVariable("topMusics", topMusics);
            
            try {
    			emailService.sendNewsletter(mail, context);
    		} catch (MessagingException | IOException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    	}

         
    }
 
}
