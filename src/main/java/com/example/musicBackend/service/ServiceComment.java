package com.example.musicBackend.service;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.musicBackend.CommentRowMapper;
import com.example.musicBackend.model.Comment;

@Service("serviceComment")
@Transactional
public class ServiceComment {
	
	private JdbcTemplate jdbcTemplate;
	
	private CommentRowMapper commentRowMapper = new CommentRowMapper();
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public void addNew(Comment comment) {
		String sql = "INSERT INTO comment(user_id, user_name, music_id, date, message) VALUES(?,?,?,?,?)";
	    jdbcTemplate.update(sql, comment.getUser_id(), comment.getUser_name(), comment.getMusic_id(), comment.getDate(), comment.getMessage());
	}

	public List<Comment> getAllComments(long idMusic, int page) {
		
		int perPage = 20;
		int min = page*perPage + 1;
		int max = min+perPage;
		
		String sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER ( ORDER BY date DESC) AS RowNum, *"
				+ " FROM comment"
				+ " WHERE music_id = ? )";
		
		sql = sql + " AS RowConstrainedResult"
				+ " WHERE   RowNum >= ?"
				+ " AND RowNum < ?"; 
		
		sql = sql + " ORDER BY RowNum";
		
		return jdbcTemplate.query(sql, new Object[]{idMusic, min, max}, commentRowMapper);
	}
	
	public Comment getCommentById(long id) {
		String sql = "SELECT * FROM comment_like WHERE id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{id}, commentRowMapper);
	}
	
	public int likeCommentById(long idComment, long idUser, long idOwner) {
		this.removeEntryLikeCommentById(idComment, idUser);
		String sql = "INSERT INTO comment_like(user_id, comment_id, owner_id, like_value) VALUES(?,?,?,?)";
	    return jdbcTemplate.update(sql, idUser, idComment, idOwner, true);
	}
	
	public int dislikeCommentById(long idComment, long idUser, long idOwner) {
		this.removeEntryLikeCommentById(idComment, idUser);
		String sql = "INSERT INTO comment_like(user_id, comment_id, owner_id, like_value) VALUES(?,?,?,?)";
		return jdbcTemplate.update(sql, idUser, idComment, idOwner, false);
	}
	
	public int removeEntryLikeCommentById(long idComment, long idUser) {
		String sql = "DELETE FROM comment_like WHERE user_id = ? AND comment_id = ?";
	    return jdbcTemplate.update(sql, idUser, idComment);
	}
	
	public int numberLikes(long owner_id) {
		String sql = "SELECT COUNT(id) FROM comment_like WHERE owner_id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{owner_id}, Integer.class);
	}
	
	public int numberLikesByCommentId(long comment_id) {
		String sql = "SELECT COUNT(id) FROM comment_like WHERE comment_id = ? AND like_value = true";
		return jdbcTemplate.queryForObject(sql, new Object[]{comment_id}, Integer.class);
	}
	
	public int numberDislikesByCommentId(long comment_id) {
		String sql = "SELECT COUNT(id) FROM comment_like WHERE comment_id = ? AND like_value = false";
		return jdbcTemplate.queryForObject(sql, new Object[]{comment_id}, Integer.class);
	}
	
	public int getNumberCommentsByMusicId(long music_id) {
		String sql = "SELECT COUNT(id) FROM comment WHERE music_id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{music_id}, Integer.class);
	}

	public boolean hasLike(long idComment, long idUser) {
		
		String sql = "SELECT EXISTS ( SELECT 1 FROM comment_like WHERE user_id = ? AND comment_id = ? AND like_value = true)";

		try {
		    return jdbcTemplate.queryForObject(sql,Boolean.class, idUser, idComment);
		} catch(DataAccessException e){
		    return false;
		}
	}
	
	public boolean hasDislike(long idComment, long idUser) {
		
		String sql = "SELECT EXISTS ( SELECT 1 FROM comment_like WHERE user_id = ? AND comment_id = ? AND like_value = false)";

		try {
		    return jdbcTemplate.queryForObject(sql,Boolean.class, idUser, idComment);
		} catch(DataAccessException e){
		    return false;
		}
	}

	public int deleteCommentById(long idComment, long id) {
		String sql = "DELETE FROM comment WHERE id = ? and user_id = ?";
		return jdbcTemplate.update(sql, idComment, id);
	}
	
	public int signalComment(long idComment, long idUser, long idOwner) {
		String sql = "INSERT INTO comment_signaled (user_id, comment_id, owner_id) VALUES(?,?,?)";
	    return jdbcTemplate.update(sql, idUser, idComment, idOwner);
	}
	
	public List<Comment> getSignaledComments() {
		String sql = "SELECT *" + 
				"FROM comment" + 
				"WHERE EXISTS" + 
				"(SELECT *" + 
				"FROM comment_signaled" + 
				"WHERE comment.id = comment_signaled.comment_id)";
		
	    return jdbcTemplate.query(sql, commentRowMapper);
	}
	
	public int getNumberSignaledCommentById(long comment_id) {
		String sql = "SELECT COUNT(id) FROM comment_signaled WHERE comment_id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{comment_id}, Integer.class);
	}

}
