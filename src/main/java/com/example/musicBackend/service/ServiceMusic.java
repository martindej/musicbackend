package com.example.musicBackend.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.dao.DataAccessException;

import com.example.musicBackend.MusicFileLightRowMapper;
import com.example.musicBackend.MusicRowMapper;
import com.example.musicBackend.model.FilterMusic;
import com.example.musicBackend.model.MusicDetail;
import com.example.musicBackend.model.MusicFile;
import com.example.musicBackend.model.MusicFileLight;
import com.example.musicBackend.model.StyleGroupInstruments;

import midi.MidiToWav;
import styles.Style;
import styles.StyleGroupIns;

@Service("serviceMusic")
@Transactional
public class ServiceMusic {
	
	final static Logger logger = LoggerFactory.getLogger(ServiceUser.class);
	
	private JdbcTemplate jdbcTemplate;
	
	private MusicRowMapper musicRowMapper = new MusicRowMapper();
	private MusicFileLightRowMapper musicFileLightRowMapper = new MusicFileLightRowMapper();
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public void addNew(MusicFile musicFile) {
		String sql = "INSERT INTO music_file("
				+ "user_id, "
				+ "user_name, "
				+ "user_id_origin, "
				+ "user_id_modified, "
				+ "user_id_video, "
				+ "user_name_origin, "
				+ "music_xml_file, "
				+ "music_xml_file_modified, "
				+ "midi_file, "
				+ "name, "
				+ "description, "
				+ "video_id, "
				+ "instruments, "
				+ "tempo, "
				+ "style, "
				+ "mp3file, "
				+ "creation_date, "
				+ "tonality, "
				+ "picture_number, "
				+ "sound_font, "
				+ "visible_origin, "
				+ "visible_modified, "
				+ "visible_video, "
				+ "mother_id) "
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
	    jdbcTemplate.update(sql, musicFile.getUser_id(), musicFile.getUser_name(), musicFile.getUser_id_origin(), musicFile.getUser_id_modified(), musicFile.getUser_id_video(), musicFile.getUser_name_origin(), musicFile.getMusicXmlFile(), musicFile.getMusicXmlFileModified(), musicFile.getMidiFile(), musicFile.getName(), musicFile.getDescription(), musicFile.getVideoId(), musicFile.getInstruments(), musicFile.getTempo(), musicFile.getStyle(), musicFile.getMp3File(), musicFile.getCreationDate(),musicFile.getTonality(), musicFile.getPictureNumber(), musicFile.getSoundFont(), musicFile.isVisible_origin(), musicFile.isVisible_modified(), musicFile.isVisible_video(), musicFile.getMother_id());
	}

	public List<MusicFileLight> getAllMusics(long id, FilterMusic filterMusic, int page) {
		
		
		String instru = "";
		int perPage = 20;
		int min = page*perPage + 1;
		int max = min+perPage;
		for(String ins:filterMusic.getInstruments()) {
			if(instru != "") {
				instru = instru + " OR";
			}
			instru = instru + " LOWER(instruments) LIKE '%" + ins +"%'";
		}
		
		String styles = "";
		for(String sty:filterMusic.getStyle()) {
			if(styles != "") {
				styles = styles + ',';
			}
			styles = styles + "'" +sty+"'";
		}

		String sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER ( ORDER BY creation_date DESC) AS RowNum, *"
				+ " FROM music_file"
				+ " WHERE user_id = ?";
		
		if(styles != "") {
			sql = sql + " AND LOWER(style) IN (" + styles +")";
		}
		if(instru != "") {
			sql = sql + " AND (" + instru + " )";
		}
		if(filterMusic.getName() != "") {
			sql = sql + " AND LOWER(name) LIKE '%" + filterMusic.getName() + "%'";
		}
		
		sql = sql + " ) AS RowConstrainedResult"
				+ " WHERE   RowNum >= ?"
				+ " AND RowNum < ?"; 
		
		sql = sql + " ORDER BY RowNum";

		return jdbcTemplate.query(sql, new Object[]{id, min, max}, musicFileLightRowMapper);
	}
	
	public List<MusicFileLight> getTopMusics() {

		long DAY_IN_MS = 1000 * 60 * 60 * 24;
		long oneWeekAgo = System.currentTimeMillis() - (7 * DAY_IN_MS);
		
		String sql = "SELECT music_file.*"
				+ " FROM music_file"
				+ " JOIN music_like ON music_like.music_id = music_file.id"
				+ " WHERE music_file.visible_origin = true OR music_file.visible_modified = true OR music_file.visible_video = true"
				+ " AND creation_date > ?"
				+ " GROUP BY music_file.id"
				+ " ORDER BY COUNT(music_like.user_id) DESC LIMIT 8";
		return jdbcTemplate.query(sql, new Object[]{oneWeekAgo}, musicFileLightRowMapper);
	}
	
	public List<Long> getIdsTopMusics() {

		long DAY_IN_MS = 1000 * 60 * 60 * 24;
		long oneWeekAgo = System.currentTimeMillis() - (7 * DAY_IN_MS);
		
		String sql = "SELECT music_file.id"
				+ " FROM music_file"
				+ " JOIN music_like ON music_like.music_id = music_file.id"
				+ " WHERE music_file.visible_origin = true OR music_file.visible_modified = true OR music_file.visible_video = true"
				+ " AND creation_date > ?"
				+ " GROUP BY music_file.id"
				+ " ORDER BY COUNT(music_like.user_id) DESC LIMIT 8";
		return jdbcTemplate.queryForList(sql, new Object[]{oneWeekAgo}, Long.class);
	}
	
	public List<MusicFileLight> getTopMusicsMail() {

		long DAY_IN_MS = 1000 * 60 * 60 * 24;
		long oneWeekAgo = System.currentTimeMillis() - (7 * DAY_IN_MS);
		
		String sql = "SELECT music_file.*"
				+ " FROM music_file"
				+ " JOIN music_like ON music_like.music_id = music_file.id"
				+ " WHERE music_file.visible_origin = true OR music_file.visible_modified = true OR music_file.visible_video = true"
				+ " AND creation_date > ?"
				+ " GROUP BY music_file.id"
				+ " ORDER BY COUNT(music_like.user_id) DESC LIMIT 3";
		return jdbcTemplate.query(sql, new Object[]{oneWeekAgo}, musicFileLightRowMapper);
	}
	
	public List<MusicFileLight> getPublicMusics(FilterMusic filterMusic, int page) {
		
		String instru = "";
		int perPage = 20;
		int min = page*perPage + 1;
		int max = min+perPage;
		
		for(String ins:filterMusic.getInstruments()) {
			if(instru != "") {
				instru = instru + " OR";
			}
			instru = instru + " LOWER(instruments) LIKE '%" + ins +"%'";
		}
		
		String styles = "";
		for(String sty:filterMusic.getStyle()) {
			if(styles != "") {
				styles = styles + ',';
			}
			styles = styles + "'" +sty+"'";
		}

		String sql = "SELECT * FROM ( SELECT ROW_NUMBER() OVER ( ORDER BY creation_date DESC) AS RowNum, *"
				+ " FROM music_file"
				+ " WHERE (visible_origin = true OR visible_modified = true OR visible_video = true)";
		
		if(styles != "") {
			sql = sql + " AND LOWER(style) IN (" + styles +")";
		}
		if(instru != "") {
			sql = sql + " AND (" + instru + " )";
		}
		if(filterMusic.getName() != "") {
			sql = sql + " AND LOWER(name) LIKE '%" + filterMusic.getName() + "%'";
		}
		
		sql = sql + " ) AS RowConstrainedResult"
				+ " WHERE   RowNum >= ?"
				+ " AND RowNum < ?"; 
		
		sql = sql + " ORDER BY RowNum";

		return jdbcTemplate.query(sql, new Object[]{min, max}, musicFileLightRowMapper);
		
//		String sql = "SELECT * FROM music_file WHERE visible_origin = true OR visible_modified = true OR visible_video = true ORDER BY creation_date DESC";
//		return jdbcTemplate.query(sql, musicRowMapper);
	}

	public MusicFile getMusicById(long id) {
		String sql = "SELECT * FROM music_file WHERE id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{id}, musicRowMapper);
	}
	
	public File getWav(byte[] midi, String fileName) {
		
		Resource resource = new ClassPathResource("/soundfonts/" + fileName);
		
	    try {
	        File dbAsStream = resource.getFile();
	        MidiToWav wav = new MidiToWav(midi, dbAsStream);
	        System.out.println("successfully music written");
//			return wav.getWavBytes();
	        return wav.getWavFile();
			
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		
	    return null;
	}

	public MusicFile getByUserIdAndDate(long user_id, long creationDate) {
		String sql = "SELECT * FROM music_file WHERE user_id = ? AND creation_date = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{user_id, creationDate}, musicRowMapper);
	}
	
	public MusicFileLight getByUserIdAndDateLight(long user_id, long creationDate) {
		String sql = "SELECT * FROM music_file WHERE user_id = ? AND creation_date = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{user_id, creationDate}, musicFileLightRowMapper);
	}
	
//	public byte[] getImageOfStyleAsBytes(String style) throws IOException {
//		
//		Resource resource;
//		
//		switch(style){
//			case "All random":
//				resource = new ClassPathResource("/imageStyles/random.jpg");
//				break;
//			case "Tiercen":
//				resource = new ClassPathResource("/imageStyles/piano.jpg");
//				break;
//			default:
//				resource = new ClassPathResource("/imageStyles/pop.jpg");
//		}
//		
//		return IOUtils.toByteArray(resource.getInputStream());
//	}

	public int deleteMusicById(long idMusic) {
		
		String sql = "DELETE FROM music_like WHERE music_id = ?";
	    jdbcTemplate.update(sql, idMusic);
	    
		sql = "DELETE FROM music_file WHERE id = ?";
		return jdbcTemplate.update(sql, idMusic);
		
	}
	
	public int likeMusicById(long idMusic, long idUser, long idOwner) {
		String sql = "INSERT INTO music_like(user_id, music_id, owner_id) VALUES(?,?,?)";
	    return jdbcTemplate.update(sql, idUser, idMusic, idOwner);
	}
	
	public int dislikeMusicById(long idMusic, long idUser) {
		String sql = "DELETE FROM music_like WHERE user_id = ? AND music_id = ?";
	    return jdbcTemplate.update(sql, idUser, idMusic);
	}
	
	public int numberLikes(long owner_id) {
		String sql = "SELECT COUNT(id) FROM music_like WHERE owner_id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{owner_id}, Integer.class);
	}
	
	public int numberLikesByMusicId(long music_id) {
		String sql = "SELECT COUNT(id) FROM music_like WHERE music_id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{music_id}, Integer.class);
	}

	public boolean hasLike(long idMusic, long idUser) {
		
//		String idUserString = String.valueOf(idUser);
		String sql = "SELECT EXISTS ( SELECT 1 FROM music_like WHERE user_id = ? AND music_id = ?)";

		try {
		    return jdbcTemplate.queryForObject(sql,Boolean.class, idUser, idMusic);
		} catch(DataAccessException e){
		    return false;
		}
	}

	public int toggleVisibleMusicById(long idMusic, boolean visible) {
		String sql = "UPDATE music_file SET visible_origin = ?, visible_modified = ?, visible_video = ? WHERE id = ?";
		return jdbcTemplate.update(sql, visible, visible, visible, idMusic);
	}
	
	public int toggleVisibleOriginMusicById(long idMusic, boolean visible) {
		String sql = "UPDATE music_file SET visible_origin = ? WHERE id = ?";
		return jdbcTemplate.update(sql, visible, idMusic);
	}
	
	public int toggleVisibleModifiedMusicById(long idMusic, boolean visible) {
		String sql = "UPDATE music_file SET visible_modified = ? WHERE id = ?";
		return jdbcTemplate.update(sql, visible, idMusic);
	}
	
	public int toggleVisibleVideoMusicById(long idMusic, boolean visible) {
		String sql = "UPDATE music_file SET visible_video = ? WHERE id = ?";
		return jdbcTemplate.update(sql, visible, idMusic);
	}

	public List<MusicFileLight> getPublicMusicsByUserId(long id) {
		String sql = "SELECT * FROM music_file WHERE (visible_origin = true OR visible_modified = true OR visible_video = true) AND user_id = ? ORDER BY creation_date DESC";
		return jdbcTemplate.query(sql, new Object[]{id}, musicFileLightRowMapper);
	}
	
	public int numberCompositions(long user_id) {
		String sql = "SELECT count(*) FROM music_file WHERE user_id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{user_id}, Integer.class);
	}

	public boolean uploadModifiedScore(MultipartFile xmlMultipartFile, long id, long musicId) {
		
		String xml = "";
		
		File xmlFile = null;
		try {
			xmlFile = convert(xmlMultipartFile);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
				
		if(xmlFile != null) {
			try {
				xml = xmlFileToString(xmlFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String sql = "UPDATE music_file SET music_xml_file_modified = ?, user_id_modified = ? WHERE id = ?";
			jdbcTemplate.update(sql, xml, id, musicId);
			return true;
		}
		else{
			return false;
		}
				
	}
	
	private File convert(MultipartFile file) throws IOException {
	    File convFile = new File(file.getOriginalFilename());
	    convFile.createNewFile();
	    FileOutputStream fos = new FileOutputStream(convFile);
	    fos.write(file.getBytes());
	    fos.close();
	    return convFile;
	}
	
	private String xmlFileToString(File xml) throws IOException {
		
		BufferedReader br = new BufferedReader(new FileReader(xml));
		String line;
		StringBuilder sb = new StringBuilder();

		while((line=br.readLine())!= null){
		    sb.append(line.trim());
		}
		
		br.close();
		
		return sb.toString();
	}

	public MusicFile getCopyMusicById(long music_id, long user_id, String user_name, long creationDate) {
		MusicFile music, musicCopy;
		
		music = this.getMusicById(music_id);
		musicCopy = new MusicFile();
		
		musicCopy.setCreationDate(creationDate);
		musicCopy.setDescription("");
		musicCopy.setInstruments(music.getInstruments());
		musicCopy.setMidiFile(music.getMidiFile());
		musicCopy.setMother_id(music.getId());
		musicCopy.setMp3File(music.getMp3File());
		if(music.isVisible_modified()) {
			musicCopy.setMusicXmlFileModified(music.getMusicXmlFileModified());
		}
		musicCopy.setMusicXmlFile(music.getMusicXmlFile());
		musicCopy.setName(music.getName());
		musicCopy.setPictureNumber(music.getPictureNumber());
		musicCopy.setStyle(music.getStyle());
		musicCopy.setTempo(music.getTempo());
		musicCopy.setTonality(music.getTonality());
		musicCopy.setUser_id(user_id);
		musicCopy.setUser_id_modified(music.getUser_id_modified());
		musicCopy.setUser_id_origin(music.getUser_id_origin());
		musicCopy.setUser_id_video(music.getUser_id_video());
		musicCopy.setUser_name(user_name);
		musicCopy.setUser_name_modified(music.getUser_name_modified());
		musicCopy.setUser_name_origin(music.getUser_name_origin());
		musicCopy.setUser_name_video(music.getUser_name_video());		
		
		musicCopy.setVisible_modified(music.isVisible_modified());
		musicCopy.setVisible_origin(music.isVisible_origin());
		musicCopy.setVisible_video(music.isVisible_video());
		
		return musicCopy;
	}

	public int changeMusicDetails(MusicDetail musicDetail) {
		String sql = "UPDATE music_file SET name = ?, description = ? WHERE id = ? AND user_id = ?";
		return jdbcTemplate.update(sql, musicDetail.getTitle(), musicDetail.getDescription(), musicDetail.getMusic_id(), musicDetail.getUser_id());
	}
	
	public int setVideoIdMusicById(long idMusic, String video_id) {
		String sql = "UPDATE music_file SET video_id = ? WHERE id = ?";
		return jdbcTemplate.update(sql, video_id, idMusic);
	}
	
	public int averageSwing(Style style) {
		
		int swingLocal = 0;
		
		for(StyleGroupIns sgIns: style.getStyleGroupInstruments()) {
			swingLocal = swingLocal + sgIns.getPourcentageSil() + sgIns.getpSyncopes() + sgIns.getpSyncopettes()
							+ sgIns.getpSyncopesTempsForts() + sgIns.getpContreTemps() + sgIns.getpDissonance()
							+ sgIns.getpIrregularites() + sgIns.getpChanceSupSameRythm();
		}
		
		return Math.round(swingLocal/(style.getStyleGroupInstruments().size() * 8));
	}
	
	public List<String> findParamList(StyleGroupInstruments styleGroupInstruments) {
				
		Class<? extends StyleGroupInstruments> componentClass = styleGroupInstruments.getClass();
	    Field[] fields = componentClass.getDeclaredFields();
	    List<String> lines = new ArrayList<>();

	    Arrays.stream(fields).forEach(field -> {
	        field.setAccessible(true);
	        String result = field.getName();
	        result = result.replace("Max", "");
	        result = result.replace("Min", "");
	        if(!lines.contains(result)) {
	        	lines.add(result);
	        }
	    });

	    return lines;
	}

}
