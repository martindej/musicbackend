package com.example.musicBackend.service;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.musicBackend.MesureTempsFortRowMapper;
import com.example.musicBackend.StyleCoefDegresRowMapper;
import com.example.musicBackend.StyleGroupDrumsInsRowMapper;
import com.example.musicBackend.StyleGroupDrumsRowMapper;
import com.example.musicBackend.StyleGroupInstrumentsRowMapper;
import com.example.musicBackend.StyleMusicRowMapper;
import com.example.musicBackend.model.MesureTempsFort;
import com.example.musicBackend.model.MesureTempsFortFull;
import com.example.musicBackend.model.StyleCoefDegres;
import com.example.musicBackend.model.StyleGroupDrums;
import com.example.musicBackend.model.StyleGroupDrumsFull;
import com.example.musicBackend.model.StyleGroupDrumsIns;
import com.example.musicBackend.model.StyleGroupDrumsInsFull;
import com.example.musicBackend.model.StyleGroupInstruments;
import com.example.musicBackend.model.StyleGroupInstrumentsFull;
import com.example.musicBackend.model.StyleMusic;
import com.example.musicBackend.model.StyleMusicFull;

@Service("ServiceStyle")
@Transactional
public class ServiceStyle {
	
	private JdbcTemplate jdbcTemplate;

	private StyleMusicRowMapper styleMusicRowMapper = new StyleMusicRowMapper();
	private MesureTempsFortRowMapper mesureTempsFortRowMapper = new MesureTempsFortRowMapper();
	private StyleCoefDegresRowMapper styleCoefDegresRowMapper = new StyleCoefDegresRowMapper();
	private StyleGroupInstrumentsRowMapper styleGroupInstrumentsRowMapper = new StyleGroupInstrumentsRowMapper();
	private StyleGroupDrumsRowMapper styleGroupDrumsRowMapper = new StyleGroupDrumsRowMapper();
	private StyleGroupDrumsInsRowMapper styleGroupDrumsInsRowMapper = new StyleGroupDrumsInsRowMapper();
		
	@Autowired
	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public List<String> getListSoundFont() throws IOException {
		List<String> filenames = new ArrayList<>();
		
		ClassLoader cl = this.getClass().getClassLoader(); 
		ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(cl);
		org.springframework.core.io.Resource[] resources = resolver.getResources("classpath*:/soundfonts/*.sf2");
		
		for (org.springframework.core.io.Resource resource: resources){
			filenames.add(resource.getFilename());
		}

	    return filenames;
	}
	
	public List<StyleMusicFull> getAllStylesMusic(boolean published) {
		
		List<StyleMusic> stylesMusic = new ArrayList<StyleMusic>();
		List<StyleMusicFull> stylesMusicFull = new ArrayList<StyleMusicFull>();
		
		String sql = "SELECT * FROM style_music";
		
		if(!published) {
			sql = sql + " WHERE publish = true";
		}
		stylesMusic = jdbcTemplate.query(sql, styleMusicRowMapper);
		
		for(StyleMusic st:stylesMusic) {
			StyleMusicFull stFull = new StyleMusicFull(st);
			stFull.setStyleGroupIns(this.getAllStyleGroupInstrumentsByStyleId(st.getId()));
			stFull.setStyleGroupDrums(this.getAllStyleGroupDrumsByStyleId(st.getId()));
			stFull.setMesureTempsFort(this.getAllMesureTempsFortByStyleId(st.getId()));
			stFull.setStyleCoefDegres(this.getAllStyleCoefDegresByStyleId(st.getId()));
			stFull.setAvailableTon(this.getAllTonalitiesByStyleId(st.getId()));
			
			stylesMusicFull.add(stFull);
		}
		
		return stylesMusicFull;
	}
	
	public List<StyleGroupInstrumentsFull> getAllStyleGroupInstrumentsByStyleId(long style_id) {
		
		List<StyleGroupInstruments> styleGroupInstruments = new ArrayList<StyleGroupInstruments>();
		List<StyleGroupInstrumentsFull> styleGroupInstrumentsFull = new ArrayList<StyleGroupInstrumentsFull>();
		
		String sql = "SELECT * FROM style_group_instruments WHERE style_id = ?";
		
		styleGroupInstruments = jdbcTemplate.query(sql, new Object[]{style_id}, styleGroupInstrumentsRowMapper);
		
		for(StyleGroupInstruments st:styleGroupInstruments) {
			StyleGroupInstrumentsFull stFull = new StyleGroupInstrumentsFull(st);
			stFull.setInstruments(this.getAllInstrumentsByStyleIdAndGroupId(st.getStyleId(), st.getId()));
			
			styleGroupInstrumentsFull.add(stFull);
		}
		
		return styleGroupInstrumentsFull;
	}
	
	public List<StyleGroupDrumsFull> getAllStyleGroupDrumsByStyleId(long style_id) {
		
		List<StyleGroupDrums> styleGroupDrums = new ArrayList<StyleGroupDrums>();
		List<StyleGroupDrumsFull> styleGroupDrumsFull = new ArrayList<StyleGroupDrumsFull>();
		
		String sql = "SELECT * FROM style_group_drums WHERE style_id = ?";
		
		styleGroupDrums = jdbcTemplate.query(sql, new Object[]{style_id}, styleGroupDrumsRowMapper);
		
		for(StyleGroupDrums st:styleGroupDrums) {
			StyleGroupDrumsFull stFull = new StyleGroupDrumsFull(st);
			stFull.setDrumsIns(this.getAllStyleGroupDrumsInsByStyleIdAndGroupDrumsId(st.getStyleId(), st.getId()));
			
			styleGroupDrumsFull.add(stFull);
		}
		return styleGroupDrumsFull;
	}
	
	private List<StyleGroupDrumsInsFull> getAllStyleGroupDrumsInsByStyleIdAndGroupDrumsId(long styleId, long id) {
				
		List<StyleGroupDrumsIns> styleGroupDrumsIns = new ArrayList<StyleGroupDrumsIns>();
		List<StyleGroupDrumsInsFull> styleGroupDrumsInsFull = new ArrayList<StyleGroupDrumsInsFull>();
		
		String sql = "SELECT * FROM style_group_drums_ins WHERE style_id = ? AND group_drums_id = ?";
		
		styleGroupDrumsIns = jdbcTemplate.query(sql, new Object[]{styleId, id}, styleGroupDrumsInsRowMapper);
		
		for(StyleGroupDrumsIns st:styleGroupDrumsIns) {
			StyleGroupDrumsInsFull stFull = new StyleGroupDrumsInsFull(st);
			stFull.setInstruments(this.getAllDrumsByStyleIdAndGroupDrumsInsId(st.getStyleId(), st.getId()));
			
			styleGroupDrumsInsFull.add(stFull);
		}
		return styleGroupDrumsInsFull;
	}

	private List<Integer> getAllDrumsByStyleIdAndGroupDrumsInsId(long styleId, long id) {
		String sql = "SELECT instrument FROM style_drums WHERE group_drums_ins_id = ? AND style_id = ?";
		return jdbcTemplate.queryForList(sql, new Object[]{id, styleId}, Integer.class);
	}

	public List<MesureTempsFortFull> getAllMesureTempsFortByStyleId(long style_id) {
		
		List<MesureTempsFort> mesureTempsForts = new ArrayList<MesureTempsFort>();
		List<MesureTempsFortFull> mesureTempsFortsFull = new ArrayList<MesureTempsFortFull>();
		
		String sql = "SELECT * FROM mesure_temps_fort WHERE style_id = ?";
		
		mesureTempsForts = jdbcTemplate.query(sql, new Object[]{style_id}, mesureTempsFortRowMapper);
		
		for(MesureTempsFort m:mesureTempsForts) {
			MesureTempsFortFull mFull = new MesureTempsFortFull(m);
			mFull.setTempsFort(this.getAllTempsFortsByMesureId(m.getId()));
			
			mesureTempsFortsFull.add(mFull);
		}
		
		return mesureTempsFortsFull;
	}
	
	public List<StyleCoefDegres> getAllStyleCoefDegresByStyleId(long style_id) {
		
		List<StyleCoefDegres> styleCoefDegres = new ArrayList<StyleCoefDegres>();
		
		String sql = "SELECT * FROM style_coef_degres WHERE style_id = ?";
		styleCoefDegres = jdbcTemplate.query(sql, new Object[]{style_id}, styleCoefDegresRowMapper);
		
		return styleCoefDegres;
	}
	
	public List<Integer> getAllTonalitiesByStyleId(long styleId) {
		
		String sql = "SELECT tonality FROM style_tonality WHERE style_id = ?";
		return jdbcTemplate.queryForList(sql, new Object[]{styleId}, Integer.class);
	}
	
	public List<Integer> getAllChordDurationsByStyleId(long styleId) {
		
		String sql = "SELECT chord_duration FROM style_chord_duration WHERE style_id = ?";
		return jdbcTemplate.queryForList(sql, new Object[]{styleId}, Integer.class);
	}
	
	public List<Integer> getAllInstrumentsByStyleIdAndGroupId(long styleId, long groupId) {
		
		String sql = "SELECT instrument FROM style_instruments WHERE style_id = ? AND group_id = ?";
		return jdbcTemplate.queryForList(sql, new Object[]{styleId, groupId}, Integer.class);
	}
	
	public List<Integer> getAllTempsFortsByMesureId(long mesureId) {
		
		String sql = "SELECT temps_fort FROM style_temp_fort WHERE mesure_id = ?";
		return jdbcTemplate.queryForList(sql, new Object[]{mesureId}, Integer.class);
	}
	
	public int removeAllTonalitiesByStyleId(long newId) {
		String sql = "DELETE FROM style_tonality WHERE style_id = ?";
	    return jdbcTemplate.update(sql, newId);
	}
	
	public int setAllTonalitiesByStyleId(StyleMusicFull styleMusicFull, long newId) {
		
		String sql = "INSERT INTO style_tonality (style_id, tonality) VALUES ";
		int i = 0;
		
		for(Integer ton:styleMusicFull.getAvailableTon()) {
			i++;
			sql = sql+ "("+newId+","+ton+")";
			if(i<styleMusicFull.getAvailableTon().size()) {
				sql = sql+",";
			}
		}
				
		return jdbcTemplate.update(sql);
	}
	
	public int removeAllChordDurationsByStyleId(long newId) {
		String sql = "DELETE FROM style_chord_duration WHERE style_id = ?";
	    return jdbcTemplate.update(sql, newId);
	}
	
	public int removeAllInstrumentsByStyleId(long newId) {
		String sql = "DELETE FROM style_instruments WHERE style_id = ?";
	    return jdbcTemplate.update(sql, newId);
	}
	
	public int removeAllInstrumentsByStyleIdAndGroupId(long styleId,long groupId) {
		String sql = "DELETE FROM style_instruments WHERE style_id = ? AND group_id = ?";
	    return jdbcTemplate.update(sql, styleId, groupId);
	}
	
	public int removeAllDrumsByStyleId(long newId) {
		String sql = "DELETE FROM style_drums WHERE style_id = ?";
	    return jdbcTemplate.update(sql, newId);
	}
	
	public int removeAllDrumsByStyleIdAndGroupDrumsId(long styleId,long groupDrumsId) {
		String sql = "DELETE FROM style_drums WHERE style_id = ? AND group_drums_id = ?";
	    return jdbcTemplate.update(sql, styleId, groupDrumsId);
	}
	
	public int removeAllDrumsByStyleIdAndGroupDrumsInsId(long styleId,long groupDrumsInsId) {
		String sql = "DELETE FROM style_drums WHERE style_id = ? AND group_drums_ins_id = ?";
	    return jdbcTemplate.update(sql, styleId, groupDrumsInsId);
	}
	
	public int removeAllDrumsByStyleIdAndGroupDrumsIdAndGroupDrumsInsId(long styleId,long groupDrumsId, long groupDrumsInsId) {
		String sql = "DELETE FROM style_drums WHERE style_id = ? AND group_drums_id = ? AND group_drums_ins_id = ?";
	    return jdbcTemplate.update(sql, styleId, groupDrumsId, groupDrumsInsId);
	}
	
	public int setAllInstrumentsByStyleIdAndGroupId(StyleGroupInstrumentsFull styleGroupInstrumentsFull, long styleId,long groupId) {
		
		String sql = "INSERT INTO style_instruments (style_id, instrument, group_id) VALUES ";

		for(Integer ins:styleGroupInstrumentsFull.getInstruments()) {
			sql = sql+ "("+styleId+","+ins+","+groupId+"),";
		}
		sql = sql.substring(0, sql.length() - 1);
				
		return jdbcTemplate.update(sql);
	}
	
	public int setAllDrumsInstrumentsByStyleIdAndGroupDrumsIdAndGroupDrumsInsId(StyleGroupDrumsInsFull styleGroupDrumsInsFull, long styleId,long groupDrumsId,long groupDrumsInsId) {
		
		String sql = "INSERT INTO style_drums (style_id, instrument, group_drums_id, group_drums_ins_id) VALUES ";

		for(Integer ins:styleGroupDrumsInsFull.getInstruments()) {
			sql = sql+ "("+styleId+","+ins+","+groupDrumsId+","+groupDrumsInsId+"),";
		}
		sql = sql.substring(0, sql.length() - 1);
				
		return jdbcTemplate.update(sql);
	}
	
	public int removeAllTempsFortsByMesureId(long newId) {
		String sql = "DELETE FROM style_temp_fort WHERE mesure_id = ?";
	    return jdbcTemplate.update(sql, newId);
	}
	
	public int removeAllStyleCoefDegresByStyleId(long newId) {
		String sql = "DELETE FROM style_coef_degres WHERE style_id = ?";
	    return jdbcTemplate.update(sql, newId);
	}
	
	public int setAllTempsFortsByMesureIdAndStyleId(MesureTempsFortFull mesureTempsFortFull, long mesureId, long styleId) {
		
		String sql = "INSERT INTO style_temp_fort (mesure_id, temps_fort, style_id) VALUES ";
		int i = 0;
		
		for(Integer tf:mesureTempsFortFull.getTempsFort()) {
			i++;
			sql = sql+ "("+mesureId+","+tf+","+styleId+")";
			if(i<mesureTempsFortFull.getTempsFort().size()) {
				sql = sql+",";
			}
		}
				
		return jdbcTemplate.update(sql);
	}
	
	public boolean upload(byte[] image,long id,String musicName) throws IOException {
		
		String sql = "UPDATE style_music SET image = ? WHERE id = ? AND style_name LIKE ?";
		jdbcTemplate.update(sql, image, id, musicName);
		
		return true;
	}
	
	public int removeStyleById(long id) {
		String sql = "DELETE FROM style_music WHERE id = ?";
	    return jdbcTemplate.update(sql, id);
	}
	
	public int removeStyleGroupInstrumentsById(long id) {
		String sql = "DELETE FROM style_group_instruments WHERE id = ?";
	    return jdbcTemplate.update(sql, id);
	}
	
	public int removeStyleGroupDrumsById(long id) {
		String sql = "DELETE FROM style_group_drums WHERE id = ?";
	    return jdbcTemplate.update(sql, id);
	}
	
	public int removeStyleGroupDrumsInsById(long id) {
		String sql = "DELETE FROM style_group_drums_ins WHERE id = ?";
	    return jdbcTemplate.update(sql, id);
	}
	
	public int removeAllStyleGroupInstrumentsByStyleId(long id) {
		String sql = "DELETE FROM style_group_instruments WHERE style_id = ?";
	    return jdbcTemplate.update(sql, id);
	}
	
	public int removeAllStyleGroupDrumsByStyleId(long id) {
		String sql = "DELETE FROM style_group_drums WHERE style_id = ?";
	    return jdbcTemplate.update(sql, id);
	}
	
	public int removeAllStyleGroupDrumsInsByStyleId(long id) {
		String sql = "DELETE FROM style_group_drums_ins WHERE style_id = ?";
	    return jdbcTemplate.update(sql, id);
	}
	
	public int removeAllStyleGroupDrumsInsByGroupDrumsId(long id) {
		String sql = "DELETE FROM style_group_drums_ins WHERE group_drums_id = ?";
	    return jdbcTemplate.update(sql, id);
	}
	
	public int removeMesureTempsFortById(long id) {
		String sql = "DELETE FROM mesure_temps_fort WHERE id = ?";
	    return jdbcTemplate.update(sql, id);
	}
	
	public int removeStyleCoefDegresById(long id) {
		String sql = "DELETE FROM style_coef_degres WHERE id = ?";
	    return jdbcTemplate.update(sql, id);
	}
	
	public int removeAllMesureTempsFortByStyleId(long id) {
		String sql = "DELETE FROM mesure_temps_fort WHERE style_id = ?";
	    return jdbcTemplate.update(sql, id);
	}
	
	public long addNewStyleGroupInstruments(StyleGroupInstrumentsFull styleGroupInstrumentsFull) {
		
		KeyHolder holder = new GeneratedKeyHolder();
		
		String sql = "INSERT INTO style_group_instruments ("
				+ "style_id, "
				+ "pourcentage_playing_min, "
				+ "pourcentage_playing_max, "
				+ "balance_bass_melody_min, "
				+ "balance_bass_melody_max, "
				+ "balance_melody_melody_bis_min, "
				+ "balance_melody_melody_bis_max, "
				+ "g_between_notes_max, "
				+ "g_between_notes_min, "
				+ "dispersion_notes_min, "
				+ "dispersion_notes_max, "
				+ "pourcentage_sil_min, "
				+ "pourcentage_sil_max, "
				+ "speed_max, "
				+ "speed_min, "
				+ "p_syncopes_min, "
				+ "p_syncopes_max, "
				+ "p_syncopettes_min, "
				+ "p_syncopettes_max, "
				+ "p_syncopes_temps_forts_min, "
				+ "p_syncopes_temps_forts_max, "
				+ "p_contre_temps_min, "
				+ "p_contre_temps_max, "
				+ "p_dissonance_min, "
				+ "p_dissonance_max, "
				+ "p_irregularites_min, "
				+ "p_irregularites_max,"
				+ "b_binaire_ternaire_min,"
				+ "b_binaire_ternaire_max,"
				+ "p_chance_sup_same_rythm_min,"
				+ "p_chance_sup_same_rythm_max,"
				+ "number_notes_simultaneously_min,"
				+ "number_notes_simultaneously_max,"
				+ "probability_rythm_min," 
				+ "probability_rythm_max," 
				+ "number_rythm_min," 
				+ "number_rythm_max," 
				+ "weight_rythm_min," 
				+ "weight_rythm_max,"
				+ "p_chance_same_note_in_motif_min,"
				+ "p_chance_same_note_in_motif_max,"
				+ "p_chance_start_by_tonale_as_bass_min,"
				+ "p_chance_start_by_tonale_as_bass_max,"
				+ "p_chance_stay_around_note_ref_min,"
				+ "p_chance_stay_around_note_ref_max,"
				+ "p_chance_play_chord_min,"
				+ "p_chance_play_chord_max,"
				+ "name, "
				+ "mother_id"
				+ ") "
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		jdbcTemplate.update(connection -> {
	        PreparedStatement ps = connection.prepareStatement(sql, new String[] { "id" /* name of your id column */ });
	          ps.setLong(1, styleGroupInstrumentsFull.getStyleId());
	          ps.setInt(2, styleGroupInstrumentsFull.getPourcentagePlayingMin());
	          ps.setInt(3, styleGroupInstrumentsFull.getPourcentagePlayingMax());
	          ps.setInt(4, styleGroupInstrumentsFull.getBalanceBassMelodyMin());
	          ps.setInt(5, styleGroupInstrumentsFull.getBalanceBassMelodyMax());
	          ps.setInt(6, styleGroupInstrumentsFull.getBalanceMelodyMelodyBisMin());
	          ps.setInt(7, styleGroupInstrumentsFull.getBalanceMelodyMelodyBisMax());
	          ps.setInt(8, styleGroupInstrumentsFull.getgBetweenNotesMax());
	          ps.setInt(9, styleGroupInstrumentsFull.getgBetweenNotesMin());
	          ps.setInt(10, styleGroupInstrumentsFull.getDispersionNotesMin());
	          ps.setInt(11, styleGroupInstrumentsFull.getDispersionNotesMax());
	          ps.setInt(12, styleGroupInstrumentsFull.getPourcentageSilMin());
	          ps.setInt(13, styleGroupInstrumentsFull.getPourcentageSilMax());
	          ps.setInt(14, styleGroupInstrumentsFull.getSpeedMax());
	          ps.setInt(15, styleGroupInstrumentsFull.getSpeedMin());
	          ps.setInt(16, styleGroupInstrumentsFull.getpSyncopesMin());
	          ps.setInt(17, styleGroupInstrumentsFull.getpSyncopesMax());
	          ps.setInt(18, styleGroupInstrumentsFull.getpSyncopesMin());
	          ps.setInt(19, styleGroupInstrumentsFull.getpSyncopesMax());
	          ps.setInt(20, styleGroupInstrumentsFull.getpSyncopesTempsFortsMin());
	          ps.setInt(21, styleGroupInstrumentsFull.getpSyncopesTempsFortsMax());
	          ps.setInt(22, styleGroupInstrumentsFull.getpContreTempsMin());
	          ps.setInt(23, styleGroupInstrumentsFull.getpContreTempsMax());
	          ps.setInt(24, styleGroupInstrumentsFull.getpDissonanceMin());
	          ps.setInt(25, styleGroupInstrumentsFull.getpDissonanceMax());
	          ps.setInt(26, styleGroupInstrumentsFull.getpIrregularitesMin());
	          ps.setInt(27, styleGroupInstrumentsFull.getpIrregularitesMax());
	          ps.setInt(28, styleGroupInstrumentsFull.getbBinaireTernaireMin());
	          ps.setInt(29, styleGroupInstrumentsFull.getbBinaireTernaireMax());
	          ps.setInt(30, styleGroupInstrumentsFull.getpChanceSupSameRythmMin());
	          ps.setInt(31, styleGroupInstrumentsFull.getpChanceSupSameRythmMax());
	          ps.setInt(32, styleGroupInstrumentsFull.getNumberNotesSimultaneouslyMin());
	          ps.setInt(33, styleGroupInstrumentsFull.getNumberNotesSimultaneouslyMax());
	          ps.setInt(34, styleGroupInstrumentsFull.getProbabilityRythmMin());
	          ps.setInt(35, styleGroupInstrumentsFull.getProbabilityRythmMax());
	          ps.setInt(36, styleGroupInstrumentsFull.getNumberRythmMin());
	          ps.setInt(37, styleGroupInstrumentsFull.getNumberRythmMax());
	          ps.setInt(38, styleGroupInstrumentsFull.getWeightRythmMin());
	          ps.setInt(39, styleGroupInstrumentsFull.getWeightRythmMax());
	          ps.setInt(40, styleGroupInstrumentsFull.getpChanceSameNoteInMotifMin());
	          ps.setInt(41, styleGroupInstrumentsFull.getpChanceSameNoteInMotifMax());
	          ps.setInt(42, styleGroupInstrumentsFull.getpChanceStartByTonaleAsBassMin());
	          ps.setInt(43, styleGroupInstrumentsFull.getpChanceStartByTonaleAsBassMax());
	          ps.setInt(44, styleGroupInstrumentsFull.getpChanceStayAroundNoteRefMin());
	          ps.setInt(45, styleGroupInstrumentsFull.getpChanceStayAroundNoteRefMax());
	          ps.setInt(46, styleGroupInstrumentsFull.getpChancePlayChordMin());
	          ps.setInt(47, styleGroupInstrumentsFull.getpChancePlayChordMax());
	          ps.setString(48, styleGroupInstrumentsFull.getName());
	          ps.setLong(49, styleGroupInstrumentsFull.getMotherId());
	          
	          return ps;
	    }, holder);
		
		return holder.getKey().longValue();

	}
	
	public int updateStyleGroupInstruments(StyleGroupInstrumentsFull styleGroupInstrumentsFull) {
		
		String sql = "UPDATE style_group_instruments SET "
				+ "style_id = ?, "
				
				+ "pourcentage_playing_min = ?, "
				+ "pourcentage_playing_max = ?, "
				+ "balance_bass_melody_min = ?, "
				+ "balance_bass_melody_max = ?, "
				+ "balance_melody_melody_bis_min = ?, "
				+ "balance_melody_melody_bis_max = ?, "
				+ "g_between_notes_max = ?, "
				+ "g_between_notes_min = ?, "
				+ "dispersion_notes_min = ?, "
				+ "dispersion_notes_max = ?, "
				+ "pourcentage_sil_min = ?, "
				+ "pourcentage_sil_max = ?, "
				+ "speed_max = ?, "
				+ "speed_min = ?, "
				+ "p_syncopes_min = ?, "
				+ "p_syncopes_max = ?, "
				+ "p_syncopettes_min = ?, "
				+ "p_syncopettes_max = ?, "
				+ "p_syncopes_temps_forts_min = ?, "
				+ "p_syncopes_temps_forts_max = ?, "
				+ "p_contre_temps_min = ?, "
				+ "p_contre_temps_max = ?, "
				+ "p_dissonance_min = ?, "
				+ "p_dissonance_max = ?, "
				+ "p_irregularites_min = ?, "
				+ "p_irregularites_max = ?,"
				+ "b_binaire_ternaire_min = ?,"
				+ "b_binaire_ternaire_max = ?,"
				+ "p_chance_sup_same_rythm_min = ?,"
				+ "p_chance_sup_same_rythm_max = ?,"
				+ "number_notes_simultaneously_min = ?,"
				+ "number_notes_simultaneously_max = ?,"
				+ "probability_rythm_min = ?," 
				+ "probability_rythm_max = ?," 
				+ "number_rythm_min = ?," 
				+ "number_rythm_max = ?," 
				+ "weight_rythm_min = ?," 
				+ "weight_rythm_max = ?,"
				+ "p_chance_same_note_in_motif_min = ?,"
				+ "p_chance_same_note_in_motif_max = ?,"
				+ "p_chance_start_by_tonale_as_bass_min = ?,"
				+ "p_chance_start_by_tonale_as_bass_max = ?,"
				+ "p_chance_stay_around_note_ref_min = ?,"
				+ "p_chance_stay_around_note_ref_max = ?,"
				+ "p_chance_play_chord_min = ?,"
				+ "p_chance_play_chord_max = ?,"
				+ "name = ?,"
				+ "mother_id = ? "
				+ "WHERE id = ?";
		
	    return jdbcTemplate.update(sql, 
	    		styleGroupInstrumentsFull.getStyleId(), 
	    		styleGroupInstrumentsFull.getPourcentagePlayingMin(),
	    		styleGroupInstrumentsFull.getPourcentagePlayingMax(), 
	    		styleGroupInstrumentsFull.getBalanceBassMelodyMin(), 
	    		styleGroupInstrumentsFull.getBalanceBassMelodyMax(), 
	    		styleGroupInstrumentsFull.getBalanceMelodyMelodyBisMin(), 
	    		styleGroupInstrumentsFull.getBalanceMelodyMelodyBisMax(), 
	    		styleGroupInstrumentsFull.getgBetweenNotesMax(),
	    		styleGroupInstrumentsFull.getgBetweenNotesMin(),
	    		styleGroupInstrumentsFull.getDispersionNotesMin(),
	    		styleGroupInstrumentsFull.getDispersionNotesMax(),
	    		styleGroupInstrumentsFull.getPourcentageSilMin(),
	    		styleGroupInstrumentsFull.getPourcentageSilMax(),
	    		styleGroupInstrumentsFull.getSpeedMax(),
	    		styleGroupInstrumentsFull.getSpeedMin(),
	    		styleGroupInstrumentsFull.getpSyncopesMin(),
	    		styleGroupInstrumentsFull.getpSyncopesMax(),
	    		styleGroupInstrumentsFull.getpSyncopesMin(),
	    		styleGroupInstrumentsFull.getpSyncopesMax(),
	    		styleGroupInstrumentsFull.getpSyncopesTempsFortsMin(),
	    		styleGroupInstrumentsFull.getpSyncopesTempsFortsMax(),
	    		styleGroupInstrumentsFull.getpContreTempsMin(),
	    		styleGroupInstrumentsFull.getpContreTempsMax(),
	    		styleGroupInstrumentsFull.getpDissonanceMin(),
	    		styleGroupInstrumentsFull.getpDissonanceMax(),
	    		styleGroupInstrumentsFull.getpIrregularitesMin(),
	    		styleGroupInstrumentsFull.getpIrregularitesMax(),
	    		styleGroupInstrumentsFull.getbBinaireTernaireMin(),
	    		styleGroupInstrumentsFull.getbBinaireTernaireMax(),
	    		styleGroupInstrumentsFull.getpChanceSupSameRythmMin(),
	    		styleGroupInstrumentsFull.getpChanceSupSameRythmMax(),
	    		styleGroupInstrumentsFull.getNumberNotesSimultaneouslyMin(),
	    		styleGroupInstrumentsFull.getNumberNotesSimultaneouslyMax(),
	    		styleGroupInstrumentsFull.getProbabilityRythmMin(),
		        styleGroupInstrumentsFull.getProbabilityRythmMax(),
		        styleGroupInstrumentsFull.getNumberRythmMin(),
		        styleGroupInstrumentsFull.getNumberRythmMax(),
		        styleGroupInstrumentsFull.getWeightRythmMin(),
		        styleGroupInstrumentsFull.getWeightRythmMax(),
		        styleGroupInstrumentsFull.getpChanceSameNoteInMotifMin(),
		        styleGroupInstrumentsFull.getpChanceSameNoteInMotifMax(),
		        styleGroupInstrumentsFull.getpChanceStartByTonaleAsBassMin(),
		        styleGroupInstrumentsFull.getpChanceStartByTonaleAsBassMax(),
		        styleGroupInstrumentsFull.getpChanceStayAroundNoteRefMin(),
		        styleGroupInstrumentsFull.getpChanceStayAroundNoteRefMax(),
		        styleGroupInstrumentsFull.getpChancePlayChordMin(),
		        styleGroupInstrumentsFull.getpChancePlayChordMax(),
	    		styleGroupInstrumentsFull.getName(),
	    		styleGroupInstrumentsFull.getMotherId(),
	    		styleGroupInstrumentsFull.getId()
	    		);
	}
	
	public long addNewStyleGroupDrums(StyleGroupDrumsFull styleGroupDrumsFull) {
		
		KeyHolder holder = new GeneratedKeyHolder();
		
		String sql = "INSERT INTO style_group_drums ("
				+ "style_id, "
				+ "pourcentage_playing_temps_fort_min, "
				+ "pourcentage_playing_temps_fort_max, "
				+ "pourcentage_playing_temps_faible_min, "
				+ "pourcentage_playing_temps_faible_max, "
				+ "pourcentage_playing_temps1min, "
				+ "pourcentage_playing_temps1max, "
				+ "pourcentage_playing_temps2min, "
				+ "pourcentage_playing_temps2max, "
				+ "pourcentage_playing_temps3min, "
				+ "pourcentage_playing_temps3max, "
				+ "pourcentage_playing_temps4min, "
				+ "pourcentage_playing_temps4max, "
				+ "name, "
				+ "rhythm"
				+ ") "
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		jdbcTemplate.update(connection -> {
	        PreparedStatement ps = connection.prepareStatement(sql, new String[] { "id" /* name of your id column */ });
	          ps.setLong(1, styleGroupDrumsFull.getStyleId());
	          ps.setInt(2, styleGroupDrumsFull.getPourcentagePlayingTempsFortMin());
	          ps.setInt(3, styleGroupDrumsFull.getPourcentagePlayingTempsFortMax());
	          ps.setInt(4, styleGroupDrumsFull.getPourcentagePlayingTempsFaibleMin());
	          ps.setInt(5, styleGroupDrumsFull.getPourcentagePlayingTempsFaibleMax());
	          ps.setInt(6, styleGroupDrumsFull.getPourcentagePlayingTemps1Min());
	          ps.setInt(7, styleGroupDrumsFull.getPourcentagePlayingTemps1Max());
	          ps.setInt(8, styleGroupDrumsFull.getPourcentagePlayingTemps2Min());
	          ps.setInt(9, styleGroupDrumsFull.getPourcentagePlayingTemps2Max());
	          ps.setInt(10, styleGroupDrumsFull.getPourcentagePlayingTemps3Min());
	          ps.setInt(11, styleGroupDrumsFull.getPourcentagePlayingTemps3Max());
	          ps.setInt(12, styleGroupDrumsFull.getPourcentagePlayingTemps4Min());
	          ps.setInt(13, styleGroupDrumsFull.getPourcentagePlayingTemps4Max());
	          ps.setString(14, styleGroupDrumsFull.getName());
	          ps.setInt(15, styleGroupDrumsFull.getRhythm());
	          
	          return ps;
	    }, holder);
		
		return holder.getKey().longValue();

	}
	
	public int updateStyleGroupDrums(StyleGroupDrumsFull styleGroupDrumsFull) {
		
		String sql = "UPDATE style_group_drums SET "
				+ "style_id = ?, "
				+ "pourcentage_playing_temps_fort_min = ?, "
				+ "pourcentage_playing_temps_fort_max = ?, "
				+ "pourcentage_playing_temps_faible_min = ?, "
				+ "pourcentage_playing_temps_faible_max = ?, "
				+ "pourcentage_playing_temps1min = ?, "
				+ "pourcentage_playing_temps1max = ?, "
				+ "pourcentage_playing_temps2min = ?, "
				+ "pourcentage_playing_temps2max = ?, "
				+ "pourcentage_playing_temps3min = ?, "
				+ "pourcentage_playing_temps3max = ?, "
				+ "pourcentage_playing_temps4min = ?, "
				+ "pourcentage_playing_temps4max = ?, "
				+ "name = ?, "
				+ "rhythm = ? "
				+ "WHERE id = ?";
		
	    return jdbcTemplate.update(sql, 
	    		styleGroupDrumsFull.getStyleId(),
        		styleGroupDrumsFull.getPourcentagePlayingTempsFortMin(),
		        styleGroupDrumsFull.getPourcentagePlayingTempsFortMax(),
		        styleGroupDrumsFull.getPourcentagePlayingTempsFaibleMin(),
		        styleGroupDrumsFull.getPourcentagePlayingTempsFaibleMax(),
		        styleGroupDrumsFull.getPourcentagePlayingTemps1Min(),
		        styleGroupDrumsFull.getPourcentagePlayingTemps1Max(),
		        styleGroupDrumsFull.getPourcentagePlayingTemps2Min(),
		        styleGroupDrumsFull.getPourcentagePlayingTemps2Max(),
		        styleGroupDrumsFull.getPourcentagePlayingTemps3Min(),
		        styleGroupDrumsFull.getPourcentagePlayingTemps3Max(),
		        styleGroupDrumsFull.getPourcentagePlayingTemps4Min(),
		        styleGroupDrumsFull.getPourcentagePlayingTemps4Max(),
		        styleGroupDrumsFull.getName(),
		        styleGroupDrumsFull.getRhythm(),
		        styleGroupDrumsFull.getId()
	    		);
	}
	
	public long addNewStyleGroupDrumsIns(StyleGroupDrumsInsFull styleGroupDrumsInsFull) {
		
		KeyHolder holder = new GeneratedKeyHolder();
		
		String sql = "INSERT INTO style_group_drums_ins ("
				+ "style_id, "
				+ "group_drums_id, "
				+ "pourcentage_playing_temps_fort_min, "
				+ "pourcentage_playing_temps_fort_max, "
				+ "pourcentage_playing_temps_faible_min, "
				+ "pourcentage_playing_temps_faible_max, "
				+ "pourcentage_playing_temps1min, "
				+ "pourcentage_playing_temps1max, "
				+ "pourcentage_playing_temps2min, "
				+ "pourcentage_playing_temps2max, "
				+ "pourcentage_playing_temps3min, "
				+ "pourcentage_playing_temps3max, "
				+ "pourcentage_playing_temps4min, "
				+ "pourcentage_playing_temps4max, "
				+ "name, "
				+ "rhythm "
				+ ") "
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		jdbcTemplate.update(connection -> {
	        PreparedStatement ps = connection.prepareStatement(sql, new String[] { "id" /* name of your id column */ });
	          ps.setLong(1, styleGroupDrumsInsFull.getStyleId());
	          ps.setLong(2, styleGroupDrumsInsFull.getGroupDrumsId());
	          ps.setInt(3, styleGroupDrumsInsFull.getPourcentagePlayingTempsFortMin());
	          ps.setInt(4, styleGroupDrumsInsFull.getPourcentagePlayingTempsFortMax());
	          ps.setInt(5, styleGroupDrumsInsFull.getPourcentagePlayingTempsFaibleMin());
	          ps.setInt(6, styleGroupDrumsInsFull.getPourcentagePlayingTempsFaibleMax());
	          ps.setInt(7, styleGroupDrumsInsFull.getPourcentagePlayingTemps1Min());
	          ps.setInt(8, styleGroupDrumsInsFull.getPourcentagePlayingTemps1Max());
	          ps.setInt(9, styleGroupDrumsInsFull.getPourcentagePlayingTemps2Min());
	          ps.setInt(10, styleGroupDrumsInsFull.getPourcentagePlayingTemps2Max());
	          ps.setInt(11, styleGroupDrumsInsFull.getPourcentagePlayingTemps3Min());
	          ps.setInt(12, styleGroupDrumsInsFull.getPourcentagePlayingTemps3Max());
	          ps.setInt(13, styleGroupDrumsInsFull.getPourcentagePlayingTemps4Min());
	          ps.setInt(14, styleGroupDrumsInsFull.getPourcentagePlayingTemps4Max());
	          ps.setString(15, styleGroupDrumsInsFull.getName());
	          ps.setInt(16, styleGroupDrumsInsFull.getRhythm());
	          
	          return ps;
	    }, holder);
		
		return holder.getKey().longValue();

	}
	
	public int updateStyleGroupDrumsIns(StyleGroupDrumsInsFull styleGroupDrumsInsFull) {
		
		String sql = "UPDATE style_group_drums_ins SET "
				+ "style_id = ?, "
				+ "group_drums_id = ?, "
				+ "pourcentage_playing_temps_fort_min = ?, "
				+ "pourcentage_playing_temps_fort_max = ?, "
				+ "pourcentage_playing_temps_faible_min = ?, "
				+ "pourcentage_playing_temps_faible_max = ?, "
				+ "pourcentage_playing_temps1min = ?, "
				+ "pourcentage_playing_temps1max = ?, "
				+ "pourcentage_playing_temps2min = ?, "
				+ "pourcentage_playing_temps2max = ?, "
				+ "pourcentage_playing_temps3min = ?, "
				+ "pourcentage_playing_temps3max = ?, "
				+ "pourcentage_playing_temps4min = ?, "
				+ "pourcentage_playing_temps4max = ?, "
				+ "name = ?, "
				+ "rhythm = ? "
				+ "WHERE id = ?";
		
	    return jdbcTemplate.update(sql, 
	    		styleGroupDrumsInsFull.getStyleId(),
	    		styleGroupDrumsInsFull.getGroupDrumsId(),
	    		styleGroupDrumsInsFull.getPourcentagePlayingTempsFortMin(),
	    		styleGroupDrumsInsFull.getPourcentagePlayingTempsFortMax(),
	    		styleGroupDrumsInsFull.getPourcentagePlayingTempsFaibleMin(),
	    		styleGroupDrumsInsFull.getPourcentagePlayingTempsFaibleMax(),
	    		styleGroupDrumsInsFull.getPourcentagePlayingTemps1Min(),
	    		styleGroupDrumsInsFull.getPourcentagePlayingTemps1Max(),
	    		styleGroupDrumsInsFull.getPourcentagePlayingTemps2Min(),
	    		styleGroupDrumsInsFull.getPourcentagePlayingTemps2Max(),
	    		styleGroupDrumsInsFull.getPourcentagePlayingTemps3Min(),
	    		styleGroupDrumsInsFull.getPourcentagePlayingTemps3Max(),
	    		styleGroupDrumsInsFull.getPourcentagePlayingTemps4Min(),
	    		styleGroupDrumsInsFull.getPourcentagePlayingTemps4Max(),
	    		styleGroupDrumsInsFull.getName(),
	    		styleGroupDrumsInsFull.getRhythm(),
	    		styleGroupDrumsInsFull.getId()
	    		);
	}
	
	public long addNew(StyleMusicFull styleMusicFull) {
		
		KeyHolder holder = new GeneratedKeyHolder();
		
		String sql = "INSERT INTO style_music ("
				+ "style_name, "
				+ "rythme_max, "
				+ "rythme_min, "
				+ "n_instrument_min, "
				+ "n_instrument_max, "
				+ "n_percussion_min, "
				+ "n_percussion_max, "
				+ "complexity_chord_min, "
				+ "complexity_chord_max, "
				+ "number_chord_min, "
				+ "number_chord_max, "
				+ "sound_font, "
				+ "publish"
				+ ") "
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		jdbcTemplate.update(connection -> {
	        PreparedStatement ps = connection.prepareStatement(sql, new String[] { "id" /* name of your id column */ });
	          ps.setString(1, styleMusicFull.getStyleName());
	          ps.setInt(2, styleMusicFull.getRythmeMax());
	          ps.setInt(3, styleMusicFull.getRythmeMin());
	          ps.setInt(4, styleMusicFull.getnInstrumentMin());
	          ps.setInt(5, styleMusicFull.getnInstrumentMax());
	          ps.setInt(6, styleMusicFull.getnPercussionMin());
	          ps.setInt(7, styleMusicFull.getnPercussionMax());
	          
	          ps.setInt(8, styleMusicFull.getComplexityChordMin());
	          ps.setInt(9, styleMusicFull.getComplexityChordMax());
	          ps.setInt(10, styleMusicFull.getNumberChordMin());
	          ps.setInt(11, styleMusicFull.getNumberChordMax());
	          
	          ps.setString(12, styleMusicFull.getSoundFont());
	          ps.setBoolean(13, styleMusicFull.isPublish());
	          return ps;
	    }, holder);
		
		return holder.getKey().longValue();

	}
	
	public int updateStyleMusic(StyleMusicFull styleMusic) {
		
		String sql = "UPDATE style_music SET "
				+ "style_name = ?, "
				+ "rythme_max = ?, "
				+ "rythme_min = ?, "
				+ "n_instrument_min = ?, "
				+ "n_instrument_max = ?, "
				+ "n_percussion_min = ?, "
				+ "n_percussion_max = ?, "
				+ "complexity_chord_min = ?, "
				+ "complexity_chord_max = ?, "
				+ "number_chord_min = ?, "
				+ "number_chord_max = ?, "
				+ "sound_font = ?, "
				+ "publish = ? "
				+ "WHERE id = ?";
		
	    return jdbcTemplate.update(sql, 
	    		styleMusic.getStyleName(), 
	    		styleMusic.getRythmeMax(), 
	    		styleMusic.getRythmeMin(), 
	    		styleMusic.getnInstrumentMin(), 
	    		styleMusic.getnInstrumentMax(),
	    		styleMusic.getnPercussionMin(), 
	    		styleMusic.getnPercussionMax(),
	    		styleMusic.getComplexityChordMin(),
	    		styleMusic.getComplexityChordMax(),
	    		styleMusic.getNumberChordMin(),
	    		styleMusic.getNumberChordMax(),
	    		styleMusic.getSoundFont(),
	    		styleMusic.isPublish(),
	    		styleMusic.getId()
	    		);
	}
	
	public long addNewStyleCoefDegres(StyleCoefDegres styleCoefDegres) {
		
		KeyHolder holder = new GeneratedKeyHolder();
		
		String sql = "INSERT INTO style_coef_degres ("
				+ "c11, "
				+ "c12, "
				+ "c13, "
				+ "c14, "
				+ "c15, "
				+ "c16, "
				+ "c17, "
				
				+ "c21, "
				+ "c22, "
				+ "c23, "
				+ "c24, "
				+ "c25, "
				+ "c26, "
				+ "c27, "
				
				+ "c31, "
				+ "c32, "
				+ "c33, "
				+ "c34, "
				+ "c35, "
				+ "c36, "
				+ "c37, "
				
				+ "c41, "
				+ "c42, "
				+ "c43, "
				+ "c44, "
				+ "c45, "
				+ "c46, "
				+ "c47, "
				
				+ "c51, "
				+ "c52, "
				+ "c53, "
				+ "c54, "
				+ "c55, "
				+ "c56, "
				+ "c57, "
				
				+ "c61, "
				+ "c62, "
				+ "c63, "
				+ "c64, "
				+ "c65, "
				+ "c66, "
				+ "c67, "
				
				+ "c71, "
				+ "c72, "
				+ "c73, "
				+ "c74, "
				+ "c75, "
				+ "c76, "
				+ "c77, "
		
				+ "style_id, "
				+ "coef_degre"
				+ ") "
				+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		jdbcTemplate.update(connection -> {
	        PreparedStatement ps = connection.prepareStatement(sql, new String[] { "id" /* name of your id column */ });
		        ps.setInt(1, styleCoefDegres.getC11());
		    	ps.setInt(2, styleCoefDegres.getC12());
		    	ps.setInt(3, styleCoefDegres.getC13());
		    	ps.setInt(4, styleCoefDegres.getC14());
		    	ps.setInt(5, styleCoefDegres.getC15());
		    	ps.setInt(6, styleCoefDegres.getC16());
		    	ps.setInt(7, styleCoefDegres.getC17());
		    	
		    	ps.setInt(8, styleCoefDegres.getC21());
		    	ps.setInt(9, styleCoefDegres.getC22());
		    	ps.setInt(10, styleCoefDegres.getC23());
		    	ps.setInt(11, styleCoefDegres.getC24());
		    	ps.setInt(12, styleCoefDegres.getC25());
		    	ps.setInt(13, styleCoefDegres.getC26());
		    	ps.setInt(14, styleCoefDegres.getC27());
		    	
		    	ps.setInt(15, styleCoefDegres.getC31());
		    	ps.setInt(16, styleCoefDegres.getC32());
		    	ps.setInt(17, styleCoefDegres.getC33());
		    	ps.setInt(18, styleCoefDegres.getC34());
		    	ps.setInt(19, styleCoefDegres.getC35());
		    	ps.setInt(20, styleCoefDegres.getC36());
		    	ps.setInt(21, styleCoefDegres.getC37());
		    	
		    	ps.setInt(22, styleCoefDegres.getC41());
		    	ps.setInt(23, styleCoefDegres.getC42());
		    	ps.setInt(24, styleCoefDegres.getC43());
		    	ps.setInt(25, styleCoefDegres.getC44());
		    	ps.setInt(26, styleCoefDegres.getC45());
		    	ps.setInt(27, styleCoefDegres.getC46());
		    	ps.setInt(28, styleCoefDegres.getC47());
		    	
		    	ps.setInt(29, styleCoefDegres.getC51());
		    	ps.setInt(30, styleCoefDegres.getC52());
		    	ps.setInt(31, styleCoefDegres.getC53());
		    	ps.setInt(32, styleCoefDegres.getC54());
		    	ps.setInt(33, styleCoefDegres.getC55());
		    	ps.setInt(34, styleCoefDegres.getC56());
		    	ps.setInt(35, styleCoefDegres.getC57());
		    	
		    	ps.setInt(36, styleCoefDegres.getC61());
		    	ps.setInt(37, styleCoefDegres.getC62());
		    	ps.setInt(38, styleCoefDegres.getC63());
		    	ps.setInt(39, styleCoefDegres.getC64());
		    	ps.setInt(40, styleCoefDegres.getC65());
		    	ps.setInt(41, styleCoefDegres.getC66());
		    	ps.setInt(42, styleCoefDegres.getC67());
		    	
		    	ps.setInt(43, styleCoefDegres.getC71());
		    	ps.setInt(44, styleCoefDegres.getC72());
		    	ps.setInt(45, styleCoefDegres.getC73());
		    	ps.setInt(46, styleCoefDegres.getC74());
		    	ps.setInt(47, styleCoefDegres.getC75());
		    	ps.setInt(48, styleCoefDegres.getC76());
		    	ps.setInt(49, styleCoefDegres.getC77());
		        ps.setLong(50, styleCoefDegres.getStyleId());
		        ps.setString(51, styleCoefDegres.getCoefDegre());
	          return ps;
	    }, holder);
		
		return holder.getKey().longValue();

	}
	
	public long updateStyleCoefDegres(StyleCoefDegres styleCoefDegres) {
		
		KeyHolder holder = new GeneratedKeyHolder();
		
		String sql = "UPDATE style_coef_degres SET "
				+ "c11 = ?, "
				+ "c12 = ?, "
				+ "c13 = ?, "
				+ "c14 = ?, "
				+ "c15 = ?, "
				+ "c16 = ?, "
				+ "c17 = ?, "
				
				+ "c21 = ?, "
				+ "c22 = ?, "
				+ "c23 = ?, "
				+ "c24 = ?, "
				+ "c25 = ?, "
				+ "c26 = ?, "
				+ "c27 = ?, "
				
				+ "c31 = ?, "
				+ "c32 = ?, "
				+ "c33 = ?, "
				+ "c34 = ?, "
				+ "c35 = ?, "
				+ "c36 = ?, "
				+ "c37 = ?, "
				
				+ "c41 = ?, "
				+ "c42 = ?, "
				+ "c43 = ?, "
				+ "c44 = ?, "
				+ "c45 = ?, "
				+ "c46 = ?, "
				+ "c47 = ?, "
				
				+ "c51 = ?, "
				+ "c52 = ?, "
				+ "c53 = ?, "
				+ "c54 = ?, "
				+ "c55 = ?, "
				+ "c56 = ?, "
				+ "c57 = ?, "
				
				+ "c61 = ?, "
				+ "c62 = ?, "
				+ "c63 = ?, "
				+ "c64 = ?, "
				+ "c65 = ?, "
				+ "c66 = ?, "
				+ "c67 = ?, "
				
				+ "c71 = ?, "
				+ "c72 = ?, "
				+ "c73 = ?, "
				+ "c74 = ?, "
				+ "c75 = ?, "
				+ "c76 = ?, "
				+ "c77 = ?, "
		
				+ "style_id = ?, "
				+ "coef_degre = ? "
				+ "WHERE id = ?";
		
		return jdbcTemplate.update(sql, 
				styleCoefDegres.getC11(),
				styleCoefDegres.getC12(),
				styleCoefDegres.getC13(),
				styleCoefDegres.getC14(),
				styleCoefDegres.getC15(),
				styleCoefDegres.getC16(),
				styleCoefDegres.getC17(),
				
				styleCoefDegres.getC21(),
				styleCoefDegres.getC22(),
				styleCoefDegres.getC23(),
				styleCoefDegres.getC24(),
				styleCoefDegres.getC25(),
				styleCoefDegres.getC26(),
				styleCoefDegres.getC27(),
				
				styleCoefDegres.getC31(),
				styleCoefDegres.getC32(),
				styleCoefDegres.getC33(),
				styleCoefDegres.getC34(),
				styleCoefDegres.getC35(),
				styleCoefDegres.getC36(),
				styleCoefDegres.getC37(),
				
				styleCoefDegres.getC41(),
				styleCoefDegres.getC42(),
				styleCoefDegres.getC43(),
				styleCoefDegres.getC44(),
				styleCoefDegres.getC45(),
				styleCoefDegres.getC46(),
				styleCoefDegres.getC47(),
				
				styleCoefDegres.getC51(),
				styleCoefDegres.getC52(),
				styleCoefDegres.getC53(),
				styleCoefDegres.getC54(),
				styleCoefDegres.getC55(),
				styleCoefDegres.getC56(),
				styleCoefDegres.getC57(),
				
				styleCoefDegres.getC61(),
				styleCoefDegres.getC62(),
				styleCoefDegres.getC63(),
				styleCoefDegres.getC64(),
				styleCoefDegres.getC65(),
				styleCoefDegres.getC66(),
				styleCoefDegres.getC67(),
				
				styleCoefDegres.getC71(),
				styleCoefDegres.getC72(),
				styleCoefDegres.getC73(),
				styleCoefDegres.getC74(),
				styleCoefDegres.getC75(),
				styleCoefDegres.getC76(),
				styleCoefDegres.getC77(),
				styleCoefDegres.getStyleId(),
				styleCoefDegres.getCoefDegre(),
				styleCoefDegres.getId()
	    		);

	}
	
	public long addNewMesureTempsFort(MesureTempsFortFull mesureTempsFort) {
		
		KeyHolder holder = new GeneratedKeyHolder();
		
		String sql = "INSERT INTO mesure_temps_fort ("
				+ "style_id, "
				+ "mesure, "
				+ "et_ou "
				+ ") "
				+ "VALUES(?,?,?)";
		
		jdbcTemplate.update(connection -> {
	        PreparedStatement ps = connection.prepareStatement(sql, new String[] { "id" /* name of your id column */ });
	          ps.setLong(1, mesureTempsFort.getStyleId());
	          ps.setInt(2, mesureTempsFort.getMesure());
	          ps.setBoolean(3, mesureTempsFort.isEtOu());
	          return ps;
	    }, holder);
		
		return holder.getKey().longValue();

	}
	
	public int updateMesureTempsFort(MesureTempsFortFull mesureTempsFort) {
		
		String sql = "UPDATE mesure_temps_fort SET "
				+ "style_id = ?, "
				+ "mesure = ?, "
				+ "et_ou = ? "
				+ "WHERE id = ?";
		
	    return jdbcTemplate.update(sql, 
	    		mesureTempsFort.getStyleId(), 
	    		mesureTempsFort.getMesure(), 
	    		mesureTempsFort.isEtOu(), 
	    		mesureTempsFort.getId()
	    		);
	}

	public boolean validateName(String name) {
		
		String sql = "SELECT EXISTS ( SELECT 1 FROM style_music WHERE style_name LIKE ?)";

		try {
		    return jdbcTemplate.queryForObject(sql,Boolean.class, name);
		} catch(DataAccessException e){
		    return false;
		}
	}
	
	public StyleMusic getStyleIdByName(String name) {
		String sql = "SELECT * FROM style_music WHERE style_name LIKE ?";
		return jdbcTemplate.queryForObject(sql,new Object[]{name}, styleMusicRowMapper);
	}

	public StyleMusic getById(long style_id) {
		String sql = "SELECT * FROM style_music WHERE id = ?";
		return jdbcTemplate.queryForObject(sql,new Object[]{style_id}, styleMusicRowMapper);
	}

	public StyleMusicFull getByIdFull(long style_id) {
		
		StyleMusic st = this.getById(style_id);
		StyleMusicFull stFull = new StyleMusicFull(st);
		stFull.setStyleGroupIns(this.getAllStyleGroupInstrumentsByStyleId(st.getId()));
		stFull.setStyleCoefDegres(this.getAllStyleCoefDegresByStyleId(st.getId()));
		stFull.setStyleGroupDrums(this.getAllStyleGroupDrumsByStyleId(st.getId()));
		stFull.setMesureTempsFort(this.getAllMesureTempsFortByStyleId(st.getId()));
		stFull.setAvailableTon(this.getAllTonalitiesByStyleId(st.getId()));
		
		return stFull;
	}

	public StyleMusic downloadAvatar(long style_id) {
		
		String sql = "SELECT * FROM style_music WHERE id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{style_id}, styleMusicRowMapper);
	}
	
	public boolean upload(byte[] avatar,long id) throws IOException {
		String sql = "UPDATE style_music SET image = ? WHERE id = ?";
		jdbcTemplate.update(sql, avatar, id);

		return true;
	}

	
}
