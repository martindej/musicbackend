package com.example.musicBackend.service;

import java.sql.PreparedStatement;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.musicBackend.BoosterRowMapper;
import com.example.musicBackend.SubscriptionRowMapper;
import com.example.musicBackend.SubscriptionUserRowMapper;
import com.example.musicBackend.model.Booster;
import com.example.musicBackend.model.Subscription;
import com.example.musicBackend.model.SubscriptionUser;

@Service("serviceSubscription")
@Transactional
public class ServiceSubscription {
	
	final static Logger logger = LoggerFactory.getLogger(ServiceSubscription.class);
	
	private JdbcTemplate jdbcTemplate;
	
	private SubscriptionRowMapper subscriptionRowMapper = new SubscriptionRowMapper();
	private BoosterRowMapper boosterRowMapper = new BoosterRowMapper();
	private SubscriptionUserRowMapper subscriptionUserRowMapper = new SubscriptionUserRowMapper();
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	public List<Subscription> getAllSubscriptions(){
	    return jdbcTemplate.query("SELECT * FROM subscription", subscriptionRowMapper);
	}
	
	public Subscription getSubscriptionById(long id){
	    String sql = "SELECT * FROM subscription WHERE id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{id}, subscriptionRowMapper);
	}
	
	public SubscriptionUser getSubscriptionUserById(long id){
	    String sql = "SELECT * FROM subscription_user WHERE id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{id}, subscriptionUserRowMapper);
	}
	
	public SubscriptionUser getSubscriptionUserByUserId(long user_id){
		long now = new Date().getTime();
		
		String sql = "SELECT * FROM subscription_user " +
				"WHERE (end_date > ? OR end_date = 0) AND user_id = ? " + 
				"ORDER BY end_date DESC LIMIT 1";
		return jdbcTemplate.queryForObject(sql, new Object[]{now, user_id}, subscriptionUserRowMapper);
	}
	
	public Long getSubscriptionOptionIdByUserId(long user_id) {
		long now = new Date().getTime();
		String sql = "SELECT subscription_id FROM subscription_user " +
				"WHERE (end_date > ? OR end_date = 0) AND user_id = ? " + 
				"ORDER BY end_date DESC LIMIT 1";
		return jdbcTemplate.queryForObject(sql, new Object[]{now, user_id}, Long.class);
	}
	
	public Subscription getFreeSubscription() {
		String sql = "SELECT * FROM subscription WHERE price_per_month = 0";
		return jdbcTemplate.queryForObject(sql, subscriptionRowMapper);
	}

	public void addNewSubscription(Subscription sub) {
		String sql = "INSERT INTO subscription(name, musics_per_day, price_per_month, price_per_year) VALUES(?,?,?,?)";
	    jdbcTemplate.update(sql, sub.getName(), sub.getMusicsPerDay(), sub.getPricePerMonth(), sub.getPricePerYear());
	}
	
	public List<Booster> getAllBoosters(){
	    return jdbcTemplate.query("SELECT * FROM booster", boosterRowMapper);
	}
	
	public Booster getBoosterById(long id){
	    String sql = "SELECT * FROM booster WHERE id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{id}, boosterRowMapper);
	}
	
	public Booster getFreeBooster() {
		String sql = "SELECT * FROM booster WHERE price = 0";
		return jdbcTemplate.queryForObject(sql, boosterRowMapper);
	}

	public void addNewBooster(Booster boo) {
		String sql = "INSERT INTO booster(name, number_musics, price) VALUES(?,?,?)";
	    jdbcTemplate.update(sql, boo.getName(), boo.getNumberMusics(), boo.getPrice());
	}
	
	public int getNumberMusicsBoosterByUserId(long user_id) {
		String sql = "SELECT number_musics FROM music_booster WHERE user_id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{user_id}, Integer.class);
	}
	
	public int getNumberMusicsSubscriptionByUserId(long user_id) {
		
		int numberMusicsAlreadyComposed = getNumberMusicsSubscriptionToday(user_id);
		int musicsPerDays = getNumberMusicsSubscriptionByDayByUserId(user_id);
		
		return musicsPerDays - numberMusicsAlreadyComposed;
	}
	
	public int getNumberMusicsSubscriptionByDayByUserId(long user_id) {
		
		long now = new Date().getTime();
		String sql = "SELECT musics_per_day FROM subscription_user " +
				"WHERE (end_date > ? OR end_date = 0) AND user_id = ? " + 
				"ORDER BY end_date DESC LIMIT 1";
//		String sql = "SELECT musics_per_day FROM subscription_user WHERE user_id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{now, user_id}, Integer.class);
	}
	
	public int getNumberMusicsSubscriptionToday(long user_id) {
		
		Calendar date = new GregorianCalendar();
		// reset hour, minutes, seconds and millis
		date.set(Calendar.HOUR_OF_DAY, 0);
		date.set(Calendar.MINUTE, 0);
		date.set(Calendar.SECOND, 0);
		date.set(Calendar.MILLISECOND, 0);
		
		long midnight = date.getTimeInMillis();

		String sql = "SELECT count(*) FROM music_subscription WHERE user_id = ? AND origin = 'subscription' AND date > ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{user_id, midnight}, Integer.class);
	}

	public void addNewMusicSubscription(long user_id, long creationDate, String origin) {
		String sql = "INSERT into music_subscription(date, user_id, origin) VALUES (?,?,?)";
		jdbcTemplate.update(sql, creationDate, user_id, origin);
	}

	public void decrementBoosterByUserId(long user_id) {
		String sql = "UPDATE music_booster SET number_musics = number_musics - 1 WHERE user_id = ?";
		jdbcTemplate.update(sql, user_id);
	}

	public void setMusicBooster(Long id, int numberMusics) {
		String sql = "INSERT INTO music_booster (user_id, number_musics) VALUES(?,?)";
		jdbcTemplate.update(sql, id, numberMusics);
	}
	
	public long addSubscriptionUser(SubscriptionUser subscriptionUser) {
		
		KeyHolder holder = new GeneratedKeyHolder();
		
		String sql = "INSERT INTO subscription_user ("
				+ "user_id, "
				+ "subscription_id, "
				+ "start_date, "
				+ "end_date, "
				+ "duration_days, "
				+ "musics_per_day, "
				+ "advanced_settings, "
				+ "melody_settings) "
				+ "VALUES(?,?,?,?,?,?,?,?)";
		
		jdbcTemplate.update(connection -> {
	        PreparedStatement ps = connection.prepareStatement(sql, new String[] { "id" /* name of your id column */ });
	          ps.setLong(1, subscriptionUser.getUserId());
	          ps.setLong(2, subscriptionUser.getSubscriptionId());
	          ps.setLong(3, subscriptionUser.getStart_date());
	          ps.setLong(4, subscriptionUser.getEnd_date());
	          ps.setInt(5, subscriptionUser.getDuration_days());
	          ps.setInt(6, subscriptionUser.getMusicsPerDay());
	          ps.setBoolean(7, subscriptionUser.isAdvancedSettings());
	          ps.setBoolean(8, subscriptionUser.isMelodySettings());
	          return ps;
	    }, holder);
	 
        return holder.getKey().longValue();
	}

	public long getOrderId() {
		String sql = "SELECT order_id FROM order_id WHERE ref = 'PROUT'";
		return jdbcTemplate.queryForObject(sql, new Object[]{}, Long.class);
	}
	
	public int incrementOrderId() {
		String sql = "UPDATE order_id SET order_id = order_id+1 WHERE ref = 'PROUT'";
		return jdbcTemplate.update(sql);
	}

	public void deleteSubscriptionUserByUserId(Long id) {
		String sql = "DELETE FROM subscription_user WHERE user_id = ?";
		jdbcTemplate.update(sql, id);
	}

}
