package com.example.musicBackend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.*;
import org.springframework.transaction.annotation.Transactional;
import com.example.musicBackend.UserFollowingRowMapper;
import com.example.musicBackend.UserRowMapper;
import com.example.musicBackend.model.User;
import com.example.musicBackend.model.UserFollowing;

@Service("serviceUser")
@Transactional
public class ServiceUser implements UserDetailsService{
	
	final static Logger logger = LoggerFactory.getLogger(ServiceUser.class);
	
	private JdbcTemplate jdbcTemplate;
	
	private UserRowMapper userRowMapper = new UserRowMapper();
	
	@Autowired
	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	public boolean userExistById(long id) {
		
		String sql = "SELECT EXISTS ( SELECT 1 FROM utilisateurs WHERE id = ?)";

		try {
		    return jdbcTemplate.queryForObject(sql,Boolean.class,id);
		} catch(DataAccessException e){
		    return false;
		}
	}

	public long addNew(User user) {
		if(!validateEmail(user.getEmail()) && !validateUsername(user.getUsername())) {
			KeyHolder holder = new GeneratedKeyHolder();
			
			user.setPassword(bcryptEncoder.encode(user.getPassword()));
			String sql = "INSERT INTO utilisateurs(username, password, email, email_checked, identifier, player, composer, description, subscription_id, super_user, newsletter, booster_id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
//		    jdbcTemplate.update(sql, user.getUsername(), user.getPassword(), user.getEmail(), user.getEmailChecked(), user.getIdentifier(), user.isPlayer(), user.isComposer(),user.getDescription(), user.getSubscriptionId(), user.isSuperUser(), user.isNewsletter(), user.getBoosterId());
			jdbcTemplate.update(connection -> {
		        PreparedStatement ps = connection.prepareStatement(sql, new String[] { "id" /* name of your id column */ });
		          ps.setString(1, user.getUsername());
		          ps.setString(2, user.getPassword());
		          ps.setString(3, user.getEmail());
		          ps.setBoolean(4, user.getEmailChecked());
		          ps.setString(5, user.getIdentifier());
		          ps.setBoolean(6, user.isPlayer());
		          ps.setBoolean(7, user.isComposer());
		          ps.setString(8, user.getDescription());
		          ps.setLong(9, user.getSubscriptionId());
		          ps.setBoolean(10, user.isSuperUser());
		          ps.setBoolean(11, user.isNewsletter());
		          ps.setLong(12, user.getBoosterId());
		          return ps;
		    }, holder);
		 
	        return holder.getKey().longValue();
	    }
		else {
			return 0;
		}	
	}
	
	public User getUserByEmail(String email) {
		String sql = "SELECT * FROM utilisateurs WHERE email LIKE ?";
        return jdbcTemplate.queryForObject(sql, new String[]{email}, new UserRowMapper());
    }
	
	public User getUserById(long id) {
		String sql = "SELECT * FROM utilisateurs WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{id}, new UserRowMapper());
    }
	
	public long getSubscriptionIdByUserId(long user_id) {
		String sql = "SELECT subscription_id FROM utilisateurs WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{user_id}, Long.class);
	}
	
	public long getBoosterIdByUserId(long user_id) {
		String sql = "SELECT booster_id FROM utilisateurs WHERE id = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{user_id}, Long.class);
	}
	
	public User getByUsername(String username) {
		String sql = "SELECT * FROM utilisateurs WHERE username = ?";
        return jdbcTemplate.queryForObject(sql, new Object[]{username}, new UserRowMapper());
	}
	
	public List<User> getAllUsers(){
	    return jdbcTemplate.query("SELECT * FROM utilisateurs", userRowMapper);
	}

	public User login(String username, String password) {
		
		String sql = "SELECT * FROM utilisateurs WHERE username LIKE ? AND password LIKE ?";
		return jdbcTemplate.queryForObject(sql, new String[] {username, password}, new UserRowMapper());
	}
	
	public User validateIdentifierMail(String username, String identifier) {
		
		String sql = "SELECT * FROM utilisateurs WHERE username LIKE ? AND identifier LIKE ?";
		return jdbcTemplate.queryForObject(sql, new String[] {username, identifier}, new UserRowMapper());
	}
	
	public List<User> getListUsersNewsletter() {
		String sql = "SELECT * FROM utilisateurs WHERE newsletter = true";
		return jdbcTemplate.query(sql, userRowMapper);
	}

	public boolean validateUsername(String username) {
		
		String sql = "SELECT EXISTS ( SELECT 1 FROM utilisateurs WHERE username LIKE ?)";

		try {
		    return jdbcTemplate.queryForObject(sql,Boolean.class, username);
		} catch(DataAccessException e){
		    return false;
		}
	}

	public boolean validateEmail(String email) {
		
		String sql = "SELECT EXISTS ( SELECT 1 FROM utilisateurs WHERE email LIKE ?)";

		try {
		    return jdbcTemplate.queryForObject(sql,Boolean.class, email);
		} catch(DataAccessException e){
		    return false;
		}
	}

	public int setEmailChecked(User user) {
		String sql = "UPDATE utilisateurs SET email_checked = ? WHERE username LIKE ? AND password = ?";
		return jdbcTemplate.update(sql, true, user.getUsername(), user.getPassword());
	}
		
	public boolean following(long user_id, long user_following_id) {
		String sql = "SELECT EXISTS ( SELECT 1 FROM user_following WHERE user_id = ? AND following_user_id = ?)";

		try {
		    return jdbcTemplate.queryForObject(sql,Boolean.class, user_id, user_following_id);
		} catch(DataAccessException e){
		    return false;
		}
	}
	
	public List<UserFollowing> listUsersFollowed(long user_id) {
		String sql = "SELECT * FROM user_following WHERE user_id = ?";
		return jdbcTemplate.query(sql, new Object[]{user_id}, new UserFollowingRowMapper() );
	}
	
	public int numberFollowers(long user_id) {
		String sql = "SELECT count(*) FROM user_following WHERE following_user_id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{user_id}, Integer.class);
	}

	public boolean toggleFollow(long user_id, long user_following_id) {
		if(following(user_id, user_following_id)) {
			String sql = "DELETE FROM user_following WHERE user_id = ? AND following_user_id = ?";
			jdbcTemplate.update(sql, user_id, user_following_id);
		}
		else {
			String sql = "INSERT INTO user_following(user_id, following_user_id) VALUES(?,?)";
		    jdbcTemplate.update(sql, user_id, user_following_id);
		}
		return true;
	}
	
	public User downloadAvatar(long user_id) {
		
		String sql = "SELECT * FROM utilisateurs WHERE id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[]{user_id}, new UserRowMapper());
//		return jdbcTemplate.queryForObject(sql, ProfileImage.class, user_id);
	}
	
	public boolean upload(String avatarName, byte[] avatar,long id,String username) throws IOException {

		String sql = "UPDATE utilisateurs SET name_avatar = ?, avatar = ? WHERE id = ? AND username LIKE ?";
		jdbcTemplate.update(sql, avatarName, avatar, id, username);
		return true;
	}

	public void updateUser(User user) {

		String sql = "UPDATE utilisateurs SET username = ?, email = ?, player = ?, composer = ?, instruments = ?, description = ?, newsletter = ? WHERE id = ?"; 
		jdbcTemplate.update(sql, user.getUsername(), user.getEmail(), user.isPlayer(), user.isComposer(),user.getInstruments(), user.getDescription(),user.isNewsletter(), user.getId());
	}

	public void changePassword(long id, String newPassword) {
		newPassword = bcryptEncoder.encode(newPassword);
		String sql = "UPDATE utilisateurs SET password = ? WHERE id = ?";
		jdbcTemplate.update(sql, newPassword, id);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = this.getByUsername(username);
		if(user == null){
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority());
	}
	
	private List<SimpleGrantedAuthority> getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}

	public boolean isAdmin(long user_id) {
		String sql = "SELECT EXISTS ( SELECT 1 FROM utilisateurs WHERE id = ? AND super_user = ?)";

		try {
		    return jdbcTemplate.queryForObject(sql,Boolean.class, user_id, true);
		} catch(DataAccessException e){
		    return false;
		}
	}

	public void setNewPassword(User user, String newPassword) {
		user.setPassword(bcryptEncoder.encode(newPassword));
		String sql = "UPDATE utilisateurs SET password = ? WHERE id = ?";
		jdbcTemplate.update(sql, user.getPassword(), user.getId());
	}

	public void setSubscriptionId(long userId, long subscriptionUserId) {
		String sql = "UPDATE utilisateurs SET subscription_id = ? WHERE id = ?";
		jdbcTemplate.update(sql, subscriptionUserId, userId);
	}

	public int deleteUserById(Long id) {
		String sql = "DELETE FROM utilisateurs WHERE id = ?";
		return jdbcTemplate.update(sql, id);
		
	}

	public void deleteUserFollowingByUserId(Long id) {
		String sql = "DELETE FROM user_following WHERE following_user_id = ? OR user_id = ?";
		jdbcTemplate.update(sql, id, id);
	}

}
