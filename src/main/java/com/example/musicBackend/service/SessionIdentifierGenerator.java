package com.example.musicBackend.service;

import java.security.SecureRandom;

import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service("SessionIdentifierGenerator")
public final class SessionIdentifierGenerator {
  private SecureRandom random = new SecureRandom();

  public String nextSessionId() {
    return new BigInteger(130, random).toString(32);
  }
}
