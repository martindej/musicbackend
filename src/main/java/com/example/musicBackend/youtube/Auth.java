package com.example.musicBackend.youtube;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.StoredCredential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.DataStore;
import com.google.api.client.util.store.FileDataStoreFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.GeneralSecurityException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Shared class used by every sample. Contains methods for authorizing a user and caching credentials.
 */
public class Auth {

    /**
     * Define a global instance of the HTTP transport.
     */
    public static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

    /**
     * Define a global instance of the JSON factory.
     */
    public static final JsonFactory JSON_FACTORY = new JacksonFactory();

    /**
     * This is the directory that will be used under the user's home directory where OAuth tokens will be stored.
     */
    private static final String CREDENTIALS_DIRECTORY = ".oauth-credentials";

    /**
     * Authorizes the installed application to access user's protected data.
     *
     * @param scopes              list of scopes needed to run youtube upload.
     * @param credentialDatastore name of the credential datastore to cache OAuth tokens
     */
    public static Credential authorize(List<String> scopes, String credentialDatastore) throws IOException {

        // Load client secrets.
        Reader clientSecretReader = new InputStreamReader(Auth.class.getResourceAsStream("/client_secrets.json"));
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, clientSecretReader);

        // Checks that the defaults have been replaced (Default = "Enter X here").
        if (clientSecrets.getDetails().getClientId().startsWith("Enter")
                || clientSecrets.getDetails().getClientSecret().startsWith("Enter ")) {
            System.out.println(
                    "Enter Client ID and Secret from https://console.developers.google.com/project/_/apiui/credential "
                            + "into src/main/resources/client_secrets.json");
            System.exit(1);
        }

        // This creates the credentials datastore at ~/.oauth-credentials/${credentialDatastore}
        FileDataStoreFactory fileDataStoreFactory = new FileDataStoreFactory(new File(System.getProperty("user.home") + "/" + CREDENTIALS_DIRECTORY));
        DataStore<StoredCredential> datastore = fileDataStoreFactory.getDataStore(credentialDatastore);

        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, scopes).setCredentialDataStore(datastore)
                .build();

        // Build the local server and bind it to port 8080
        LocalServerReceiver localReceiver = new LocalServerReceiver.Builder().setPort(8070).build();

        // Authorize.
        return new AuthorizationCodeInstalledApp(flow, localReceiver).authorize("user");
    }
    
    public static String getAccessToken() {
    	
    	String refresh_token = null, client_id, client_secret;	
    	JSONParser parser = new JSONParser();

        try
        {
        	ClassLoader classloader = Thread.currentThread().getContextClassLoader();
    		InputStream is = classloader.getResourceAsStream("refresh_token.json");
            Object obj = parser.parse(new InputStreamReader(is));
            JSONObject jsonObject = (JSONObject) obj;
            refresh_token = (String) jsonObject.get("refresh_token");
            
            InputStream isBis = classloader.getResourceAsStream("client_secrets.json");
            Object objBis = parser.parse(new InputStreamReader(isBis));
            JSONObject jsonObjectBis = (JSONObject) objBis;
            JSONObject installed = (JSONObject) jsonObjectBis.get("installed");
            client_id = (String) installed.get("client_id");
            client_secret = (String) installed.get("client_secret");
            
            Map<String,Object> params = new LinkedHashMap<>();
            params.put("grant_type","refresh_token");
            params.put("client_id",client_id);
            params.put("client_secret",client_secret);
            params.put("refresh_token", refresh_token);

            StringBuilder postData = new StringBuilder();
            for(Map.Entry<String,Object> param : params.entrySet())
            {
                if(postData.length() != 0)
                {
                    postData.append('&');
                }
                postData.append(URLEncoder.encode(param.getKey(),"UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(String.valueOf(param.getValue()),"UTF-8"));
            }
            byte[] postDataBytes = postData.toString().getBytes("UTF-8");

            URL url = new URL("https://www.googleapis.com/oauth2/v4/token");
            HttpURLConnection con = (HttpURLConnection)url.openConnection();
            con.setDoOutput(true);
            con.setUseCaches(false);
            con.setRequestMethod("POST");
            con.getOutputStream().write(postDataBytes);

            BufferedReader  reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuffer buffer = new StringBuffer();
            for (String line = reader.readLine(); line != null; line = reader.readLine())
            {
                buffer.append(line);
            }
            
//            JSONObject json = new JSONObject();
//            json.
//
//            JSONObject json = new JSONObject(buffer.toString());
//            String accessToken = json.getString("access_token");
            return buffer.toString();
        }
        catch (Exception ex)
        {
            ex.printStackTrace(); 
        }
        return null;
    }
    
    public static Credential generateCredentialWithUserApprovedToken() throws IOException, GeneralSecurityException {
    	
    	HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
    	Reader clientSecretReader = new InputStreamReader(Auth.class.getResourceAsStream("/client_secrets.json"));
    	GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, clientSecretReader);

    	String refresh_token = null;	
    	JSONParser parser = new JSONParser();
    	try {
    		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
    		InputStream is = classloader.getResourceAsStream("refresh_token.json");
    		
            Object obj = parser.parse(new InputStreamReader(is));
 
            JSONObject jsonObject = (JSONObject) obj;
 
            refresh_token = (String) jsonObject.get("refresh_token");
 
        } catch (Exception e) {
            e.printStackTrace();
        }
    	
    	return new GoogleCredential.Builder().setTransport(httpTransport).setJsonFactory(JSON_FACTORY)
	      .setClientSecrets(clientSecrets).build().setRefreshToken(refresh_token);
	}
}
